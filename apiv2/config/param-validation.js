import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required()
    }
  },

  // POST /api/follower
  createFollower: {
    body: {
      followed_id: Joi.string().required()
    }
  },

    // POST /api/Question
    createQuestion: {
      body: {
        question: Joi.string().required(),
        difficulty: Joi.string().required(),
        type: Joi.string().required(),
      }
    },

    
    // UPDATE /api/users/:userId
    updateQuestion: {
    body: {
      question: Joi.string().required(),
      difficulty: Joi.string().required(),
      type: Joi.string().required(),
    },
    params: {
      questionId: Joi.string().hex().required()
    }
  },

  createOption: {
    body: {
      answer: Joi.string().required(),
      question_id: Joi.string().required(),
     
    }
  },

  
  // UPDATE /api/users/:userId
  updateOption: {
  body: {
    answer: Joi.string().required(),
    question_id: Joi.string().required(),
  
  },
  params: {
    optionId: Joi.string().hex().required()
  }
},

createMedia: {
  body: {
    name: Joi.string().required(),
    file: Joi.string().required(),
    type: Joi.string().required(),
   
  }
},


// UPDATE /api/users/:userId
updateMedia: {
body: {
  name: Joi.string().required(),
  file: Joi.string().required(),
  type: Joi.string().required(),

},
params: {
  mediaId: Joi.string().hex().required(),
}
},


createAnswer: {
  body: {
   
    time_spent: Joi.number().required(),
    attempts: Joi.number().required(),
    //game_id: Joi.number().required(),
    user_id: Joi.string().hex().required(),
    question_id: Joi.string().hex().required(),
    option_id: Joi.string().hex().required(),
   
  }
},


// UPDATE /api/users/:userId
updateAnswer: {
body: {
  time_spent: Joi.number().required(),
    attempts: Joi.number().required(),
    //game_id: Joi.number().required(),
    user_id: Joi.string().hex().required(),
    question_id: Joi.string().hex().required(),
    option_id: Joi.string().hex().required(),
},
params: {
  answerId: Joi.string().hex().required()
}
},
  

createQuestionMedia: {
  body: {
   
    question_id: Joi.string().hex().required(),
    media_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/users/:userId
updateQuestionMedia: {
body: {
  question_id: Joi.string().hex().required(),
  media_id: Joi.string().hex().required(),
},
params: {
  questionMediaId: Joi.string().hex().required()
}
},


createGameType: {
  body: {
    name: Joi.string().required(),
    min_players: Joi.number().required(),
    max_players: Joi.number().required(),
    max_duration: Joi.number().required(),
    time_per_question: Joi.number().required(),
  }
},

// UPDATE /api/gametype/:gameTypeId
updateGameType: {
body: {
  name: Joi.string().required(),
  min_players: Joi.number().required(),
  max_players: Joi.number().required(),
  max_duration: Joi.number().required(),
  time_per_question: Joi.number().required(),
},
params: {
  questionMediaId: Joi.string().hex().required()
}
},

createGame: {
  body: {
    gametype_id: Joi.string().required(),
    state: Joi.string().required(),
    start_time: Joi.string().required(),
    end_time: Joi.string().required(),
    status: Joi.string().required(),
  }
},

// UPDATE /api/gametype/:gameTypeId
updateGame: {
body: {
  gametype_id: Joi.string().required(),
  state: Joi.string().required(),
  start_time: Joi.string().required(),
  end_time: Joi.string().required(),
  status: Joi.string().required(),
},
params: {
  gameId: Joi.string().hex().required(),
}

},


createCategory: {
  body: {
    name: Joi.string().required(),
    description: Joi.string().required(),
    
  }
},

// UPDATE /api/gametype/:gameTypeId
updateCategory: {
body: {
  name: Joi.string().required(),
  description: Joi.string().required(),
},
params: {
  categoryId: Joi.string().hex().required(),
}

},

createGameQuestion: {
  body: {
    game_id: Joi.string().hex().required(),
    question_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:gameTypeId
updateGameQuestion: {
body: {
    game_id: Joi.string().hex().required(),
    question_id: Joi.string().hex().required(),
},
params: {
  gameQuestionId: Joi.string().hex().required(),
}

},



createQuestionCategory: {
  body: {
    category_id: Joi.string().hex().required(),
    question_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:questionCategoryId
updateQuestionCategory: {
body: {
   category_id: Joi.string().hex().required(),
    question_id: Joi.string().hex().required(),
},
params: {
  gameQuestionId: Joi.string().hex().required(),
}

},

createOptionMedias: {
  body: {
    option_id: Joi.string().hex().required(),
   media_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:optionMediasId
updateOptionMedias: {
body: {
   option_id: Joi.string().hex().required(),
   media_id: Joi.string().hex().required(),
},
params: {
  gameQuestionId: Joi.string().hex().required(),
}

},


createMail: {
  body: {
    subject:  Joi.string().required(),
    body:  Joi.string().required(),
    sender_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:mailId
updateMail: {
body: {
    subject:  Joi.string().required(),
    body:  Joi.string().required(),
    sender_id: Joi.string().hex().required(),
},
params: {
  mailId: Joi.string().hex().required(),
}

},


createChat: {
  body: {
    name:  Joi.string().required(),
    messages:  Joi.array().required(),
  }
},

// UPDATE /api/gametype/:chatId
updateChat: {
body: {
  name:  Joi.string().required(),
  messages:  Joi.array().required(),
},
params: {
  chatId: Joi.string().hex().required(),
}

},



createMessage: {
  body: {
    body:  Joi.string().required(),
    sender_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:messageId
updateMessage: {
body: {
    body:  Joi.string().required(),
    sender_id: Joi.string().hex().required(),
},
params: {
  messageId: Joi.string().hex().required(),
}

},



createReaction: {
  body: {
    type:  Joi.string().required(),
    user_id: Joi.string().hex().required(),
    observable_type:  Joi.string().required(),
    observable_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:reactionId
updateReaction: {
body: {
    type:  Joi.string().required(),
    user_id: Joi.string().hex().required(),
    observable_type:  Joi.string().required(),
    observable_id: Joi.string().hex().required(),
},
params: {
  reactionId: Joi.string().hex().required(),
}

},



createRecipient: {
  body: {
   
    mail_id: Joi.string().hex().required(),
    user_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:recipientId
updateRecipient: {
body: {
    mail_id: Joi.string().hex().required(),
    user_id: Joi.string().hex().required(),
},
params: {
  recipientId: Joi.string().hex().required(),
}

},


createParticipant: {
  body: {
   
    chat_id: Joi.string().hex().required(),
    user_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:mailId
updateParticipant: {
body: {
    chat_id: Joi.string().hex().required(),
    user_id: Joi.string().hex().required(),
},
params: {
  participantId: Joi.string().hex().required(),
}

},


createPost: {
  body: {
   
    title: Joi.string().required(),
    summary: Joi.string().required(),
    content: Joi.string().required(),
    category_id: Joi.string().hex().required(),
    publisher_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:postId
updatePost: {
body: {
    title: Joi.string().required(),
    summary: Joi.string().required(),
    content: Joi.string().required(),
    category_id: Joi.string().hex().required(),
    publisher_id: Joi.string().hex().required(),
},
params: {
  postId: Joi.string().hex().required(),
}

},


createComment: {
  body: {
   
    post: Joi.string().required(),
    content: Joi.string().required(),
    sender_id: Joi.string().hex().required(),
  }
},

// UPDATE /api/gametype/:commentId
updateComment: {
body: {
    post: Joi.string().required(),
    content: Joi.string().required(),
    sender_id: Joi.string().hex().required(),
},
params: {
  commentId: Joi.string().hex().required(),
}

},

createReward: {
  body: {
    name: Joi.string().required(),
    description: Joi.string().required(),
    category: Joi.string().hex().required(),
    completion_points: Joi.number().required(),
    image: Joi.string().hex().required(),
  }
},

// UPDATE /api/:levels
updateReward: {
  body: {
    name: Joi.string().required(),
    description: Joi.string().required(),
    category: Joi.string().hex().required(),
    completion_points: Joi.number().required(),
    image: Joi.string().hex().required(),
  },
  params: {
    rewardId: Joi.string().hex().required(),
    }
  },


  createLevel: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      xp: Joi.number().required(),
      image: Joi.string().hex().required(),
    }
  },
  
  // UPDATE /api/:level
  updateLevel: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      xp: Joi.number().required(),
      image: Joi.string().hex().required(),
    },
    params: {
      levelId: Joi.string().hex().required(),
    }
  },

  createLeaderboard: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      image: Joi.string().hex().required(),
    }
  },
  
  // UPDATE /api/:leaderboard
  updateLeaderboard: {
    body: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      image: Joi.string().hex().required(),
    },
    params: {
      leaderboardlId: Joi.string().hex().required(),
    }
  },

  createScore: {
    body: {
      score: Joi.number().required(),
  
    }
  },
  
  // UPDATE /api/:score
  updateScore: {
    body: {
      score: Joi.number().required(),
    },
    params: {
      scoreId: Joi.string().hex().required(),
    }
  },

  createUserLevel: {
    body: {
      state: Joi.string().required(),
      xp: Joi.number().required(),
      user_id: Joi.string().hex().required(),
      level_id: Joi.string().hex().required(),
    }
  },
  
   // UPDATE /api/:user-level
   updateUserLevel: {
    body: {
      state: Joi.string().required(),
      xp: Joi.number().required(),
      user_id: Joi.string().hex().required(),
      level_id: Joi.string().hex().required(),
    },
    params: {
      userLevelId: Joi.string().hex().required(),
    }
  },

  createUserReward: {
    body: {
      
      points: Joi.number().required(),
      user_id: Joi.string().hex().required(),
      reward_id: Joi.string().hex().required(),
    }
  },
  
   // UPDATE /api/:user-level
   updateUserReward: {
    body: {
      
      points: Joi.number().required(),
      user_id: Joi.string().hex().required(),
      reward_id: Joi.string().hex().required(),
    },
    params: {
      userRewardId: Joi.string().hex().required(),
    }
  },


  createLevelReward: {
    body: {
      
     
      level_id: Joi.string().hex().required(),
      reward_id: Joi.string().hex().required(),
    }
  },
  
   // UPDATE /api/:level-reward
   updateLevelReward: {
    body: {
      
      level_id: Joi.string().hex().required(),
      reward_id: Joi.string().hex().required(),
    },
    params: {
      levelRewardId: Joi.string().hex().required(),
    }
  },

  createLeaderboardReward: {
    body: {
      
     
      leaderboard_id: Joi.string().hex().required(),
      reward_id: Joi.string().hex().required(),
    }
  },
  
   // UPDATE /api/:leaderboar-reward
   updateLeaderboardReward: {
    body: {
      
      leaderboard_id: Joi.string().hex().required(),
      reward_id: Joi.string().hex().required(),
    },
    params: {
      leaderboardRewardId: Joi.string().hex().required(),
    }
  },

  
};
