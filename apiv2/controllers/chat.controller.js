
import httpStatus from 'http-status';
import chat from '../models/chat.model';
import APIError from '../helpers/APIError';

/**
 * Get chat
 * @returns {chat}
 */
function get(req, res) {
  return res.json(req.chat);
}

/**
 * Create new chat
 * @property {string} req.body.name - The name of chat.
   * @returns {chat}
 */
function create(req, res, next) {
  
  console.log(typeof( req.body.messages))
  const chat = new chat({
    name: req.body.name, 
    messages: req.body.messages,
     });
     
  chat.save() 
  .then(savedchat => res.json(savedchat))
  .catch(e => next(e));
  }

/**
 * Update existing chat
 * @property {string} req.body.subject - The subject of chat.
 * @property {string} req.body.messages - The messages of chat.
 * @returns {chat}
 */
function update(req, res, next) {

  chat.findOne({ _id: req.params.chatId }).then(chat => {
    chat.name = req.body.name,
    chat.name = req.body.messages,  
    
    chat.save()
      .then(savedchat => res.json(savedchat))
      .catch(e => next(e));
  });
}


/**
 * Get chat list.
 * @property {number} req.query.skip - Number of chats to be skipped.
 * @property {number} req.query.limit - Limit number of chats to be returned.
 * @returns {chat[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  chat.list({ limit, skip })
    .then(chats => res.json(chats))
    .catch(e => next(e));
}

/**
 * Delete chat.
 * @returns {chat}
 */
function remove(req, res, next) {  
  chat.findOneAndRemove({ _id: req.params.chatId })
    .then(chat => {
      if (!chat) {
        let error = new APIError('chat not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(chat);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
