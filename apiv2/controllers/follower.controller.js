
import httpStatus from 'http-status';
import Follower from '../models/follower.model';
import APIError from '../helpers/APIError';

/**
 * Get follower
 * @returns {Follower}
 */
function get(req, res) {
  return res.json(req.follower);
}

/**
 * Create new follower
 * @property {string} req.body.followed_id - The followed_id of follower.
 * @returns {Follower}
 */
function create(req, res, next) {
  const follower = new Follower({
  user_id: req.user._id,
  followed_id: req.body.followed_id, 
  });
  
  follower.save() 
  .then(savedFollower => res.json(savedFollower))
  .catch(e => next(e));
  }

/**
 * Get follower list.
 * @property {number} req.query.skip - Number of followers to be skipped.
 * @property {number} req.query.limit - Limit number of followers to be returned.
 * @returns {Follower[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Follower.list({ limit, skip })
    .then(followers => res.json(followers))
    .catch(e => next(e));
}

/**
 * Delete follower.
 * @returns {Follower}
 */
function remove(req, res, next) {  
  Follower.findOneAndRemove({ _id: req.params.followerId })
    .then(follower => {
      if (!follower) {
        let error = new APIError('Follower not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(follower);
    })
    .catch(e => next(e));
}

export default {  get, create, list, remove };
