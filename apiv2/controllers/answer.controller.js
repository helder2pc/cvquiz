
import httpStatus from 'http-status';
import Answer from '../models/answer.model';
import APIError from '../helpers/APIError';

/**
 * Get answer
 * @returns {Answer}
 */
function get(req, res) {
  return res.json(req.answer);
}

/**
 * Create new answer
 * @property {string} req.body.time_spent - The time_spent of Answer.
 * @property {string} req.body.attempts - The attempts of Answer.
 * @property {string} req.body.game_id - The game_id of Answer.
 * @property {string} req.body.user_id - The user_id of Answer.
 * @property {string} req.body.question_id - The question_id of Answer.
 * @property {string} req.body.option_id - The option_id of Answer.
 * @returns {Answer}
 */
function create(req, res, next) {
  const answer = new Answer({
    time_spent: req.body.time_spent, 
    attempts: req.body.attempts, 
    game_id: req.body.game_id, 
    user_id: req.body.user_id, 
    question_id: req.body.question_id, 
    option_id: req.body.option_id, 
     
  });
  
  answer.save() 
  .then(savedAnswer => res.json(savedAnswer))
  .catch(e => next(e));
  }

/**
 * Update existing answer
 * @property {string} req.body.answer - The answer of Answer.
 * @property {string} req.body.question_id - The question_id of Answer.
 * @property {string} req.body.feedback - The feedback of Answer.
 * @property {string} req.body.order - The order of Answer.
 * @property {string} req.body.correct - The correct of Answer.
 * @returns {Answer}
 */
function update(req, res, next) {

  User.findOne({ _id: req.params.answerId }).then(answer => {
    answer.time_spent = req.body.time_spent, 
    answer.attempts = req.body.attempts, 
    answer.game_id = req.body.game_id, 
    answer.user_id = req.body.user_id, 
    answer.question_id = req.body.question_id, 
    answer.option_id = req.body.option_id, 
    answer.save()
      .then(savedAnswer => res.json(savedAnswer))
      .catch(e => next(e));
  });
}


/**
 * Get answer list.
 * @property {number} req.query.skip - Number of answers to be skipped.
 * @property {number} req.query.limit - Limit number of answers to be returned.
 * @returns {Answer[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Answer.list({ limit, skip })
    .then(answers => res.json(answers))
    .catch(e => next(e));
}

/**
 * Delete answer.
 * @returns {Answer}
 */
function remove(req, res, next) {  
  Answer.findOneAndRemove({ _id: req.params.answerId })
    .then(answer => {
      if (!answer) {
        let error = new APIError('Answer not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(answer);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
