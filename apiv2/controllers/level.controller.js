
import httpStatus from 'http-status';
import Level from '../models/level.model';
import APIError from '../helpers/APIError';

/**
 * Get level
 * @returns {Level}
 */
function get(req, res) {
  return res.json(req.level);
}

/**
 * Create new level
 * @property {string} req.body.name - The name of Level.
 * @property {string} req.body.description - The description of Level.
 * @property {string} req.body.xp - The xp of Level.
 * @property {string} req.body.image - The image of Level.
 * @returns {Level}
 */
function create(req, res, next) {
  const level = new Level({
    name: req.body.name, 
    description: req.body.description, 
    xp: req.body.xp, 
    image: req.body.image, 
    
  });
  
  level.save() 
  .then(savedLevel => res.json(savedLevel))
  .catch(e => next(e));
  }

/**
 * Update existing level
 * @property {string} req.body.name - The name of Level.
 * @property {string} req.body.description - The description of Level.
 * @property {string} req.body.xp - The xp of Level.
 * @property {string} req.body.image - The image of Level.
 * @returns {Level}
 */
function update(req, res, next) {

  Level.findOne({ _id: req.params.levelId }).then(level => {
    level.name = req.body.name, 
    level.description = req.body.description, 
    level.feedback = req.body.feedback, 
    level.xp = req.body.xp, 
    level.image = req.body.image, 
  
    level.save()
      .then(savedLevel => res.json(savedLevel))
      .catch(e => next(e));
  });
}

/**
 * Get level list.
 * @property {number} req.query.skip - Number of levels to be skipped.
 * @property {number} req.query.limit - Limit number of levels to be returned.
 * @returns {Level[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Level.list({ limit, skip })
    .then(levels => res.json(levels))
    .catch(e => next(e));
}

/**
 * Delete level.
 * @returns {Level}
 */
function remove(req, res, next) {  
  Level.findOneAndRemove({ _id: req.params.levelId })
    .then(level => {
      if (!level) {
        let error = new APIError('Level not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(level);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
