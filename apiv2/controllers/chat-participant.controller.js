
import httpStatus from 'http-status';
import Participant from '../models/chat-participant.model';
import APIError from '../helpers/APIError';

/**
 * Get participant
 * @returns {Participant}
 */
function get(req, res) {
  return res.json(req.participant);
}

/**
 * Create new participant
 * @property {string} req.body.chat_id - The chat_id of Participant.
 * @property {string} req.body.user_id - The user_id of Participant.
 * @returns {Participant}
 */
function create(req, res, next) {
  const participant = new Participant({
    chat_id: req.body.chat_id, 
    user_id: req.body.user_id, 
  });
  
  participant.save() 
  .then(savedParticipant => res.json(savedParticipant))
  .catch(e => next(e));
  }

/**
 * Update existing participant
 * @property {string} req.body.chat_id - The chat_id of Participant.
 * @property {string} req.body.user_id - The user_id of Participant.
 * @returns {Participant}
 */
function update(req, res, next) {

  Participant.findOne({ _id: req.params.participantId }).then(participant => {
    participant.chat_id = req.body.chat_id, 
    participant.user_id = req.body.user_id, 
  
    participant.save()
      .then(savedParticipant => res.json(savedParticipant))
      .catch(e => next(e));
  });
}


/**
 * Get participant list.
 * @property {number} req.query.skip - Number of participants to be skipped.
 * @property {number} req.query.limit - Limit number of participants to be returned.
 * @returns {Participant[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Participant.list({ limit, skip })
    .then(participants => res.json(participants))
    .catch(e => next(e));
}

/**
 * Delete participant.
 * @returns {Participant}
 */
function remove(req, res, next) {  
  Participant.findOneAndRemove({ _id: req.params.participantId })
    .then(participant => {
      if (!participant) {
        let error = new APIError('Participant not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(participant);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
