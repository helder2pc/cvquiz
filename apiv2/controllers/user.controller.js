
import httpStatus from 'http-status';

import APIError from '../helpers/APIError';
import Media from '../models/media.model';
import User from '../models/user.model';

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id)
    .then((user) => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get user
 * @property {string[]} req.query.relations - Relations to populate.
 * @returns {User}
 */
function get(req, res) {
  return User.get(req.user._id, req.query.relations ? req.query.relations.split(',') : [])
    .then(user => res.json(user))
    .catch(e => next(e));
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.email - The email of user.
 * @returns {User}
 */
function create(req, res, next) {
  const user = new User({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    username: req.body.username,
    email: req.body.email,
    role: [ 'user' ]
  });

  user.setPassword(req.body.password);

  user.save()
    .then(savedUser => res.json(savedUser))
    .catch(e => next(e));
}

/**
 * Update existing user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.email - The email of user.
 * @returns {User}
 */
function update(req, res, next) {

  User.findOne({ _id: req.params.userId }).then(async user => {
    user.first_name = req.body.first_name;
    user.last_name = req.body.last_name;
    user.username = req.body.username;
    user.email = req.body.email;
    
    if (req.body.photo !== undefined) {
      if (typeof req.body.photo === 'object') {

        // cascade upsert
        let photo;
        if (req.body.photo.id) {
          photo = await Media.update(req.body.photo);
        } else {
          photo = await Media.create(req.body.photo);
        }

        // delete old photo
        if (user.photo) {
          await Media.deleteOne(user.photo);
        }

        user.photo = photo._id;
      } else {
        user.photo = req.body.photo;
      }
    }
    
    user.save()
      .then(savedUser => res.json(savedUser))
      .catch(e => next(e));
  });
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @param {string[]} relations - Relations to populate.
 * @returns {User[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  User.list({
    limit,
    skip,
    relations: req.query.relations ? req.query.relations.split(',') : []
  })
    .then(users => res.json(users))
    .catch(e => next(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {  
  User.findOneAndRemove({ _id: req.params.userId })
    .then(user => {
      if (!user) {
        let error = new APIError('User not found', httpStatus.NOT_FOUND);
        return next(error);
      }  

      Media.remove({ _id: user.photo });

      res.json(user);
    })
    .catch(e => next(e));
}

export default { load, get, create, update, list, remove };
