
import httpStatus from 'http-status';
import Mail from '../models/mail.model';
import APIError from '../helpers/APIError';

/**
 * Get mail
 * @returns {Mail}
 */
function get(req, res) {
  return res.json(req.mail);
}

/**
 * Create new mail
 * @property {string} req.body.subject - The subject of Mail.
 * @property {string} req.body.body - The body of Mail.
 * @property {string} req.body.sender_id - The sender_id of Mail.
  * @returns {Mail}
 */
function create(req, res, next) {
  const mail = new Mail({
    subject: req.body.subject, 
    body: req.body.body, 
    sender_id:  req.body.sender_id,
     });
  
  mail.save() 
  .then(savedMail => res.json(savedMail))
  .catch(e => next(e));
  }

/**
 * Update existing mail
 * @property {string} req.body.subject - The subject of Mail.
 * @property {string} req.body.body - The body of Mail.
 * @property {string} req.body.sender_id - The sender_id of Mail.
 * @returns {Mail}
 */
function update(req, res, next) {

  Mail.findOne({ _id: req.params.mailId }).then(mail => {
    mail.subject = req.body.subject, 
    mail.body = req.body.body,
    mail.sender_id.body = req.body.sender_id,

    mail.save()
      .then(savedMail => res.json(savedMail))
      .catch(e => next(e));
  });
}


/**
 * Get mail list.
 * @property {number} req.query.skip - Number of mails to be skipped.
 * @property {number} req.query.limit - Limit number of mails to be returned.
 * @returns {Mail[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Mail.list({ limit, skip })
    .then(mails => res.json(mails))
    .catch(e => next(e));
}

/**
 * Delete mail.
 * @returns {Mail}
 */
function remove(req, res, next) {  
  Mail.findOneAndRemove({ _id: req.params.mailId })
    .then(mail => {
      if (!mail) {
        let error = new APIError('Mail not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(mail);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
