
import httpStatus from 'http-status';
import Score from '../models/score.model';
import APIError from '../helpers/APIError';

/**
 * Get score
 * @returns {Score}
 */
function get(req, res) {
  return res.json(req.score);
}

/**
 * Create new score
 * @property {string} req.body.score - The answer of Score.
 * @returns {Score}
 */
function create(req, res, next) {
  const score = new Score({
    score: req.body.score, 
  
  });
  
  score.save() 
  .then(savedScore => res.json(savedScore))
  .catch(e => next(e));
  }

/**
 * Update existing score
 * @property {string} req.body.score - The score of Score.
 * @returns {Score}
 */
function update(req, res, next) {

  Score.findOne({ _id: req.params.scoreId }).then(score => {
    score.score = req.body.score, 
   
  
    score.save()
      .then(savedScore => res.json(savedScore))
      .catch(e => next(e));
  });
}


/**
 * Get score list.
 * @property {number} req.query.skip - Number of scores to be skipped.
 * @property {number} req.query.limit - Limit number of scores to be returned.
 * @returns {Score[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Score.list({ limit, skip })
    .then(scores => res.json(scores))
    .catch(e => next(e));
}

/**
 * Delete score.
 * @returns {Score}
 */
function remove(req, res, next) {  
  Score.findOneAndRemove({ _id: req.params.scoreId })
    .then(score => {
      if (!score) {
        let error = new APIError('Score not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(score);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
