
import httpStatus from 'http-status';
import LevelReward from '../models/level-reward.model';
import APIError from '../helpers/APIError';

/**
 * Get levelReward
 * @returns {LevelReward}
 */
function get(req, res) {
  return res.json(req.levelReward);
}

/**
 * Create new levelReward
 * @property {string} req.body.level_id - The level_id of LevelReward.
 * @property {string} req.body.reward_id - The reward_id of LevelReward.
 * @returns {LevelReward}
 */
function create(req, res, next) {
  const levelReward = new LevelReward({
    reward_id: req.body.reward_id, 
    level_id: req.body.level_id, 
   
  });
  
  levelReward.save() 
  .then(savedLevelReward => res.json(savedLevelReward))
  .catch(e => next(e));
  }

/**
 * Update existing levelReward
 * @property {string} req.body.level_id - The level_id of LevelReward.
 * @property {string} req.body.reward_id - The reward_id of LevelReward.
 * @returns {LevelReward}
 */
function update(req, res, next) {

  LevelReward.findOne({ _id: req.params.levelRewardId }).then(levelReward => {
    levelReward.level_id = req.body.level_id, 
    levelReward.reward_id = req.body.reward_id, 
    
  
    levelReward.save()
      .then(savedLevelReward => res.json(savedLevelReward))
      .catch(e => next(e));
  });
}


/**
 * Get levelReward list.
 * @property {number} req.query.skip - Number of levelRewards to be skipped.
 * @property {number} req.query.limit - Limit number of levelRewards to be returned.
 * @returns {LevelReward[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  LevelReward.list({ limit, skip })
    .then(levelRewards => res.json(levelRewards))
    .catch(e => next(e));
}

/**
 * Delete levelReward.
 * @returns {LevelReward}
 */
function remove(req, res, next) {  
  LevelReward.findOneAndRemove({ _id: req.params.levelRewardId })
    .then(levelReward => {
      if (!levelReward) {
        let error = new APIError('LevelReward not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(levelReward);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
