
import httpStatus from 'http-status';
import Game from '../models/game.model';
import APIError from '../helpers/APIError';

/**
 * Get game
 * @returns {Game}
 */
function get(req, res) {
  return res.json(req.game);
}

/**
 * Create new game
* @property {string} req.body.gametype_id - The gametype_id of Game.
 * @property {string} req.body.state - The state of Game.
 * @property {string} req.body.start_time - The start_time of Game.
 * @property {string} req.body.end_time - The end_time of Game.
 * @property {string} req.body.status - The status of Game.
 * @returns {Game}
 */
function create(req, res, next) {
  const game = new Game({
    gametype_id: req.body.gametype_id, 
    state: req.body.state, 
    start_time: req.body.start_time, 
    end_time: req.body.end_time, 
    status: req.body.status, 
  });
  
  game.save() 
  .then(savedGame => res.json(savedGame))
  .catch(e => next(e));
  }

/**
 * Update existing game
 * @property {string} req.body.gametype_id - The gametype_id of Game.
 * @property {string} req.body.state - The state of Game.
 * @property {string} req.body.start_time - The start_time of Game.
 * @property {string} req.body.end_time - The end_time of Game.
 * @property {string} req.body.status - The status of Game.
 * @returns {Game}
 */
function update(req, res, next) {

  Game.findOne({ _id: req.params.gameId }).then(game => {
    game.gametype_id = req.body.gametype_id, 
    game.state = req.body.state, 
    game.start_time = req.body.start_time, 
    game.end_time = req.body.end_time, 
    game.status = req.body.status, 
  
    game.save()
      .then(savedGame => res.json(savedGame))
      .catch(e => next(e));
  });
}


/**
 * Get game list.
 * @property {number} req.query.skip - Number of games to be skipped.
 * @property {number} req.query.limit - Limit number of games to be returned.
 * @returns {Game[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Game.list({ limit, skip })
    .then(games => res.json(games))
    .catch(e => next(e));
}

/**
 * Delete game.
 * @returns {Game}
 */
function remove(req, res, next) {  
  Game.findOneAndRemove({ _id: req.params.gameId })
    .then(game => {
      if (!game) {
        let error = new APIError('Game not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(game);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
