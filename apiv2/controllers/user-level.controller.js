
import httpStatus from 'http-status';
import UserLevel from '../models/user-level.model';
import APIError from '../helpers/APIError';

/**
 * Get userLevel
 * @returns {UserLevel}
 */
function get(req, res) {
  return res.json(req.userLevel);
}

/**
 * Create new userLevel
 * @property {string} req.body.user_id - The user_id of UserLevel.
 * @property {string} req.body.level_id - The level_id of UserLevel.
 * @property {string} req.body.xp - The xp of UserLevel.
 * @property {string} req.body.state - The state of UserLevel.
 * @returns {UserLevel}
 */
function create(req, res, next) {
  const userLevel = new UserLevel({
    user_id: req.body.user_id, 
    level_id: req.body.level_id, 
    xp: req.body.xp, 
    state: req.body.state, 
  
  });
  
  userLevel.save() 
  .then(savedUserLevel => res.json(savedUserLevel))
  .catch(e => next(e));
  }

/**
 * Update existing userLevel
 * @property {string} req.body.user_id - The user_id of UserLevel.
 * @property {string} req.body.level_id - The level_id of UserLevel.
 * @property {string} req.body.xp - The xp of UserLevel.
 * @property {string} req.body.state - The state of UserLevel.
 * @returns {UserLevel}
 */
function update(req, res, next) {

  UserLevel.findOne({ _id: req.params.userLevelId }).then(userLevel => {
    userLevel.user_id = req.body.user_id, 
    userLevel.level_id = req.body.level_id, 
    userLevel.state = req.body.state, 
    userLevel.xp = req.body.xp, 
    
  
    userLevel.save()
      .then(savedUserLevel => res.json(savedUserLevel))
      .catch(e => next(e));
  });
}


/**
 * Get userLevel list.
 * @property {number} req.query.skip - Number of userLevels to be skipped.
 * @property {number} req.query.limit - Limit number of userLevels to be returned.
 * @returns {UserLevel[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  UserLevel.list({ limit, skip })
    .then(userLevels => res.json(userLevels))
    .catch(e => next(e));
}

/**
 * Delete userLevel.
 * @returns {UserLevel}
 */
function remove(req, res, next) {  
  UserLevel.findOneAndRemove({ _id: req.params.userLevelId })
    .then(userLevel => {
      if (!userLevel) {
        let error = new APIError('UserLevel not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(userLevel);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
