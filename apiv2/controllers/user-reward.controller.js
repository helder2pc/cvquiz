
import httpStatus from 'http-status';
import UserReward from '../models/user-reward.model';
import APIError from '../helpers/APIError';

/**
 * Get userReward
 * @returns {UserReward}
 */
function get(req, res) {
  return res.json(req.userReward);
}

/**
 * Create new userReward
 * @property {string} req.body.user_id - The user_id of UserReward.
 * @property {string} req.body.reward_id - The question_id of UserReward.
 * @property {string} req.body.points - The feedback of UserReward.
 
 * @returns {UserReward}
 */
function create(req, res, next) {
  const userReward = new UserReward({
    user_id: req.body.user_id, 
    reward_id: req.body.reward_id, 
    points: req.body.points, 
    
  });
  
  userReward.save() 
  .then(savedUserReward => res.json(savedUserReward))
  .catch(e => next(e));
  }
  

/**
 * Update existing userReward
 * @property {string} req.body.user_id - The user_id of UserReward.
 * @property {string} req.body.reward_id - The question_id of UserReward.
 * @property {string} req.body.points - The feedback of UserReward.
 */
function update(req, res, next) {

  UserReward.findOne({ _id: req.params.userRewardId }).then(userReward => {
    userReward.user_id = req.body.user_id, 
    userReward.reward_id = req.body.reward_id, 
    userReward.points = req.body.points, 
    
  
    userReward.save()
      .then(savedUserReward => res.json(savedUserReward))
      .catch(e => next(e));
  });
}


/**
 * Get userReward list.
 * @property {number} req.query.skip - Number of userRewards to be skipped.
 * @property {number} req.query.limit - Limit number of userRewards to be returned.
 * @returns {UserReward[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  UserReward.list({ limit, skip })
    .then(userRewards => res.json(userRewards))
    .catch(e => next(e));
}

/**
 * Delete userReward.
 * @returns {UserReward}
 */
function remove(req, res, next) {  
  UserReward.findOneAndRemove({ _id: req.params.userRewardId })
    .then(userReward => {
      if (!userReward) {
        let error = new APIError('UserReward not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(userReward);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
