
import httpStatus from 'http-status';
import LeaderBoardReward from '../models/leaderboard-reward.model';
import APIError from '../helpers/APIError';

/**
 * Get leaderboardReward
 * @returns {LeaderBoardReward}
 */
function get(req, res) {
  return res.json(req.leaderboardReward);
}

/**d
 * Create new leaderboardReward
 * @property {string} req.body.reward_id - The reward_id of LeaderBoardReward.
 * @property {string} req.body.leaderboard_id - The leaderboard_id of LeaderBoardReward.
 
 * @returns {LeaderBoardReward}
 */
function create(req, res, next) {
  const leaderboardReward = new LeaderBoardReward({
   
    reward_id: req.body.reward_id, 
    leaderboard_id: req.body.leaderboard_id, 
   
  });
  
  leaderboardReward.save() 
  .then(savedLeaderBoardReward => res.json(savedLeaderBoardReward))
  .catch(e => next(e));
  }

/**
 * Update existing leaderboardReward
* @property {string} req.body.reward_id - The reward_id of LeaderBoardReward.
 * @property {string} req.body.leaderboard_id - The leaderboard_id of LeaderBoardReward.
 * @returns {LeaderBoardReward}
 */
function update(req, res, next) {

  LeaderBoardReward.findOne({ _id: req.params.leaderboardRewardId }).then(leaderboardReward => {
    leaderboardReward.reward_id = req.body.reward_id, 
    leaderboardReward.leaderboard_id = req.body.leaderboard_id, 
    leaderboardReward.feedback = req.body.feedback, 
    leaderboardReward.order = req.body.order, 
    leaderboardReward.correct = req.body.correct, 
  
    leaderboardReward.save()
      .then(savedLeaderBoardReward => res.json(savedLeaderBoardReward))
      .catch(e => next(e));
  });
}


/**
 * Get leaderboardReward list.
 * @property {number} req.query.skip - Number of leaderboardRewards to be skipped.
 * @property {number} req.query.limit - Limit number of leaderboardRewards to be returned.
 * @returns {LeaderBoardReward[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  LeaderBoardReward.list({ limit, skip })
    .then(leaderboardRewards => res.json(leaderboardRewards))
    .catch(e => next(e));
}

/**
 * Delete leaderboardReward.
 * @returns {LeaderBoardReward}
 */
function remove(req, res, next) {  
  LeaderBoardReward.findOneAndRemove({ _id: req.params.leaderboardRewardId })
    .then(leaderboardReward => {
      if (!leaderboardReward) {
        let error = new APIError('LeaderBoardReward not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(leaderboardReward);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
