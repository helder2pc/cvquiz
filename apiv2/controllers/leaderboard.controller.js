
import httpStatus from 'http-status';
import Leaderboard from '../models/leaderboard.model';
import APIError from '../helpers/APIError';

/**
 * Get leaderboard
 * @returns {Leaderboard}
 */
function get(req, res) {
  return res.json(req.leaderboard);
}

/**
 * Create new leaderboard
 * @property {string} req.body.name - The answer of name.
 * @property {string} req.body.description - The question_id of description.
 * @property {string} req.body.image - The image of Leaderboard.
 
 * @returns {Leaderboard}
 */
function create(req, res, next) {
  const leaderboard = new Leaderboard({
    name: req.body.name, 
    description: req.body.description, 
    image: req.body.image, 
   
  });
  
  leaderboard.save() 
  .then(savedLeaderboard => res.json(savedLeaderboard))
  .catch(e => next(e));
  }

/**
 * Update existing leaderboard
 * @property {string} req.body.name - The answer of name.
 * @property {string} req.body.description - The question_id of description.
 * @property {string} req.body.image - The image of Leaderboard.
 * @returns {Leaderboard}
 */
function update(req, res, next) {

  Leaderboard.findOne({ _id: req.params.leaderboardId }).then(leaderboard => {
    leaderboard.name = req.body.name, 
    leaderboard.description = req.body.description, 
    leaderboard.image = req.body.image, 

    leaderboard.save()
      .then(savedLeaderboard => res.json(savedLeaderboard))
      .catch(e => next(e));
  });
}


/**
 * Get leaderboard list.
 * @property {number} req.query.skip - Number of leaderboards to be skipped.
 * @property {number} req.query.limit - Limit number of leaderboards to be returned.
 * @returns {Leaderboard[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Leaderboard.list({ limit, skip })
    .then(leaderboards => res.json(leaderboards))
    .catch(e => next(e));
}

/**
 * Delete leaderboard.
 * @returns {Leaderboard}
 */
function remove(req, res, next) {  
  Leaderboard.findOneAndRemove({ _id: req.params.leaderboardId })
    .then(leaderboard => {
      if (!leaderboard) {
        let error = new APIError('Leaderboard not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(leaderboard);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
