
import httpStatus from 'http-status';
import Post from '../models/post.model';
import APIError from '../helpers/APIError';

/**
 * Get post
 * @returns {Post}
 */
function get(req, res) {
  return res.json(req.post);
}

/**
 * Create new post
 * @property {string} req.body.title - The title of Post.
 * @property {string} req.body.summary - The summary of Post.
 * @property {string} req.body.content - The content of Post.
 * @property {string} req.body.category_id - The category_id of Post.
 * @property {string} req.body.publisher_id - The publisher_id of Post.
 * @returns {Post}
 */
function create(req, res, next) {
  const post = new Post({
    title: req.body.title, 
    summary: req.body.summary, 
    content: req.body.content, 
    category_id: req.body.category_id, 
    publisher_id: req.body.publisher_id, 
  });
  
  post.save() 
  .then(savedPost => res.json(savedPost))
  .catch(e => next(e));
  }

/**
 * Update existing post
 * @property {string} req.body.title - The title of Post.
 * @property {string} req.body.summary - The summary of Post.
 * @property {string} req.body.content - The content of Post.
 * @property {string} req.body.category_id - The category_id of Post.
 * @property {string} req.body.publisher_id - The publisher_id of Post.
 * @returns {Post}
 */
function update(req, res, next) {

  Post.findOne({ _id: req.params.postId }).then(post => {
    post.title = req.body.title, 
    post.summary = req.body.summary, 
    post.content = req.body.content, 
    post.category_id = req.body.category_id, 
    post.publisher_id = req.body.publisher_id,  
  
    post.save()
      .then(savedPost => res.json(savedPost))
      .catch(e => next(e));
  });
}


/**
 * Get post list.
 * @property {number} req.query.skip - Number of posts to be skipped.
 * @property {number} req.query.limit - Limit number of posts to be returned.
 * @returns {Post[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Post.list({ limit, skip })
    .then(posts => res.json(posts))
    .catch(e => next(e));
}

/**
 * Delete post.
 * @returns {Post}
 */
function remove(req, res, next) {  
  Post.findOneAndRemove({ _id: req.params.postId })
    .then(post => {
      if (!post) {
        let error = new APIError('Post not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(post);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
