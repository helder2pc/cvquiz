
import httpStatus from 'http-status';
import GameQuestion from '../models/game-questions.model';
import APIError from '../helpers/APIError';

/**
 * Get gameQuestion
 * @returns {GameQuestion}
 */
function get(req, res) {
  return res.json(req.gameQuestion);
}

/**
 * Create new gameQuestion
 * @property {string} req.body.game_id - The game_id of GameQuestion.
 * @property {string} req.body.question_id - The question_id of GameQuestion.
 * @returns {GameQuestion}
 */
function create(req, res, next) {
  const gameQuestion = new GameQuestion({
    game_id: req.body.game_id, 
    question_id: req.body.question_id, 
     
  });
  
  gameQuestion.save() 
  .then(savedGameQuestion => res.json(savedGameQuestion))
  .catch(e => next(e));
  }

/**
 * Update existing gameQuestion
 * @property {string} req.body.game_id - The game_id of GameQuestion.
 * @property {string} req.body.question_id - The question_id of GameQuestion.
 * @returns {GameQuestion}
 */
function update(req, res, next) {

  GameQuestion.findOne({ _id: req.params.gameQuestionId }).then(gameQuestion => {
    gameQuestion.game_id = req.body.game_id, 
    gameQuestion.question_id = req.body.question_id, 
  
    gameQuestion.save()
      .then(savedGameQuestion => res.json(savedGameQuestion))
      .catch(e => next(e));
  });
}


/**
 * Get gameQuestion list.
 * @property {number} req.query.skip - Number of gameQuestions to be skipped.
 * @property {number} req.query.limit - Limit number of gameQuestions to be returned.
 * @returns {GameQuestion[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  GameQuestion.list({ limit, skip })
    .then(gameQuestions => res.json(gameQuestions))
    .catch(e => next(e));
}

/**
 * Delete gameQuestion.
 * @returns {GameQuestion}
 */
function remove(req, res, next) {  
  GameQuestion.findOneAndRemove({ _id: req.params.gameQuestionId })
    .then(gameQuestion => {
      if (!gameQuestion) {
        let error = new APIError('GameQuestion not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(gameQuestion);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
