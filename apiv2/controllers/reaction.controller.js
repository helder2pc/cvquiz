
import httpStatus from 'http-status';
import Reaction from '../models/reaction.model';
import APIError from '../helpers/APIError';

/**
 * Get reaction
 * @returns {Reaction}
 */
function get(req, res) {
  return res.json(req.reaction);
}

/**
 * Create new reaction
 * @property {string} req.body.type - The type of Reaction.
 * @property {string} req.body.user_id - The body of user_id.
 * @property {string} req.body.observable_id - The observable_id of Reaction.
 * @property {string} req.body.observable_type - The observable_type of Reaction.
  * @returns {Reaction}
 */
function create(req, res, next) {
  const reaction = new Reaction({
    type: req.body.type, 
    user_id: req.body.user_id, 
    observable_id:  req.body.observable_id,
    observable_type:  req.body.observable_type,
     });
  
  reaction.save() 
  .then(savedReaction => res.json(savedReaction))
  .catch(e => next(e));
  }

/**
 * Update existing reaction
 * @property {string} req.body.type - The type of Reaction.
 * @property {string} req.body.user_id - The body of user_id.
 * @property {string} req.body.observable_id - The observable_id of Reaction.
 * @property {string} req.body.observable_type - The observable_type of Reaction.
 * @returns {Reaction}
 */
function update(req, res, next) {

  Reaction.findOne({ _id: req.params.reactionId }).then(reaction => {
    reaction.type = req.body.type, 
    reaction.user_id = req.body.user_id, 
    reaction.observable_id =  req.body.observable_id,
    reaction.observable_type =  req.body.observable_type,

    reaction.save()
      .then(savedReaction => res.json(savedReaction))
      .catch(e => next(e));
  });
}


/**
 * Get reaction list.
 * @property {number} req.query.skip - Number of reactions to be skipped.
 * @property {number} req.query.limit - Limit number of reactions to be returned.
 * @returns {Reaction[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Reaction.list({ limit, skip })
    .then(reactions => res.json(reactions))
    .catch(e => next(e));
}

/**
 * Delete reaction.
 * @returns {Reaction}
 */
function remove(req, res, next) {  
  Reaction.findOneAndRemove({ _id: req.params.reactionId })
    .then(reaction => {
      if (!reaction) {
        let error = new APIError('Reaction not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(reaction);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
