
import httpStatus from 'http-status';
import Media from '../models/media.model';
import APIError from '../helpers/APIError';

/**
 * Get media
 * @returns {Media}
 */
function get(req, res) {
  return res.json(req.media);
}

/**
 * Create new media
 * @property {string} req.body.name - The name of Media.
 * @property {string} req.body.file - The file of Media.
 * @property {string} req.body.type - The type of Media.
 * @returns {Media}
 */
function create(req, res, next) {
  const media = new Media({
    name: req.body.name, 
    file: req.body.file, 
    type: req.body.type, 
    
  });
  
  media.save() 
  .then(savedMedia => res.json(savedMedia))
  .catch(e => next(e));
  }

/**
 * Update existing media
* @property {string} req.body.name - The name of Media.
 * @property {string} req.body.file - The file of Media.
 * @property {string} req.body.type - The type of Media.
 * @returns {Media}
 */
function update(req, res, next) {

  Media.findOne({ _id: req.params.mediaId }).then(media => {
    media.name = req.body.name, 
    media.file = req.body.file, 
    media.type = req.body.type, 
  
    media.save()
      .then(savedMedia => res.json(savedMedia))
      .catch(e => next(e));
  });
}


/**
 * Get media list.
 * @property {number} req.query.skip - Number of medias to be skipped.
 * @property {number} req.query.limit - Limit number of medias to be returned.
 * @returns {Media[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Media.list({ limit, skip })
    .then(medias => res.json(medias))
    .catch(e => next(e));
}

/**
 * Delete media.
 * @returns {Media}
 */
function remove(req, res, next) {  
  Media.findOneAndRemove({ _id: req.params.mediaId })
    .then(media => {
      if (!media) {
        let error = new APIError('Media not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(media);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
