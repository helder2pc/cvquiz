
import httpStatus from 'http-status';
import Recipient from '../models/mail-recipients.model';
import APIError from '../helpers/APIError';

/**
 * Get recipient
 * @returns {Recipient}
 */
function get(req, res) {
  return res.json(req.recipient);
}

/**
 * Create new recipient
 * @property {string} req.body.mail_id - The mail_id of Recipient.
 * @property {string} req.body.user_id - The user_id of Recipient.
  * @returns {Recipient}
 */
function create(req, res, next) {
  const recipient = new Recipient({
    mail_id: req.body.mail_id, 
    user_id: req.body.user_id, 
     });
  
  recipient.save() 
  .then(savedRecipient => res.json(savedRecipient))
  .catch(e => next(e));
  }

/**
 * Update existing recipient
 * @property {string} req.body.mail_id - The mail_id of Recipient.
 * @property {string} req.body.user_id - The user_id of Recipient.
 * @returns {Recipient}
 */
function update(req, res, next) {

  Recipient.findOne({ _id: req.params.recipientId }).then(recipient => { 
    recipient.mail_id = req.body.mail_id,
    recipient.user_id = req.body.user_id,

    recipient.save()
      .then(savedRecipient => res.json(savedRecipient))
      .catch(e => next(e));
  });
}


/**
 * Get recipient list.
 * @property {number} req.query.skip - Number of recipients to be skipped.
 * @property {number} req.query.limit - Limit number of recipients to be returned.
 * @returns {Recipient[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Recipient.list({ limit, skip })
    .then(recipients => res.json(recipients))
    .catch(e => next(e));
}

/**
 * Delete recipient.
 * @returns {Recipient}
 */
function remove(req, res, next) {  
  Recipient.findOneAndRemove({ _id: req.params.recipientId })
    .then(recipient => {
      if (!recipient) {
        let error = new APIError('Recipient not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(recipient);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
