
import httpStatus from 'http-status';
import Category from '../models/category.model';
import APIError from '../helpers/APIError';

/**
 * Get category
 * @returns {Category}
 */
function get(req, res) {
  return res.json(req.category);
}

/**
 * Create new category
 * @property {string} req.body.name - The name of Category.
 * @property {string} req.body.descripton - The descripton of Category.
 * @returns {Category}
 */
function create(req, res, next) {
  const category = new Category({
    name: req.body.name, 
    descripton: req.body.descripton, 
   
  });
  
  category.save() 
  .then(savedCategory => res.json(savedCategory))
  .catch(e => next(e));
  }

/**
 * Update existing category
 * @property {string} req.body.answer - The answer of Category.
 * @property {string} req.body.question_id - The question_id of Category.
 * @property {string} req.body.feedback - The feedback of Category.
 * @property {string} req.body.order - The order of Category.
 * @property {string} req.body.correct - The correct of Category.
 * @returns {Category}
 */
function update(req, res, next) {

  Category.findOne({ _id: req.params.categoryId }).then(category => {
    category.name = req.body.name, 
    category.descripton = req.body.descripton, 
  
    category.save()
      .then(savedCategory => res.json(savedCategory))
      .catch(e => next(e));
  });
}


/**
 * Get category list.
 * @property {number} req.query.skip - Number of categorys to be skipped.
 * @property {number} req.query.limit - Limit number of categorys to be returned.
 * @returns {Category[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Category.list({ limit, skip })
    .then(categorys => res.json(categorys))
    .catch(e => next(e));
}

/**
 * Delete category.
 * @returns {Category}
 */
function remove(req, res, next) {  
  Category.findOneAndRemove({ _id: req.params.categoryId })
    .then(category => {
      if (!category) {
        let error = new APIError('Category not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(category);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
