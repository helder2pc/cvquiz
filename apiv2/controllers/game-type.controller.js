
import httpStatus from 'http-status';
import GameType from '../models/game-type.model';
import APIError from '../helpers/APIError';

/**
 * Get gameType
 * @returns {GameType}
 */
function get(req, res) {
  return res.json(req.gameType);
}

/**
 * Create new gameType
 * @property {string} req.body.name - The answer of GameType.
 * @property {string} req.body.min_players - The min_players of GameType.
 * @property {string} req.body.max_players - The max_players of GameType.
 * @property {string} req.body.max_duration - The max_duration of GameType.
 * @property {string} req.body.time_per_question - The time_per_question of GameType.
 * @returns {GameType}
 */
function create(req, res, next) {
  const gameType = new GameType({
    name: req.body.name, 
    min_players: req.body.min_players, 
    max_players: req.body.max_players, 
    max_duration: req.body.max_duration, 
    time_per_question: req.body.time_per_question, 
  });
  
  gameType.save() 
  .then(savedGameType => res.json(savedGameType))
  .catch(e => next(e));
  }

/**
 * Update existing gameType
 * @property {string} req.body.name - The answer of GameType.
 * @property {string} req.body.min_players - The min_players of GameType.
 * @property {string} req.body.max_players - The max_players of GameType.
 * @property {string} req.body.max_duration - The max_duration of GameType.
 * @property {string} req.body.time_per_question - The time_per_question of GameType.
 * @returns {GameType}
 */
function update(req, res, next) {

  GameType.findOne({ _id: req.params.gameTypeId }).then(gameType => {
    gameType.name = req.body.name, 
    gameType.min_players = req.body.min_players, 
    gameType.max_players = req.body.max_players, 
    gameType.max_duration = req.body.max_duration, 
    gameType.time_per_question = req.body.time_per_question, 
  
    gameType.save()
      .then(savedGameType => res.json(savedGameType))
      .catch(e => next(e));
  });
}


/**
 * Get gameType list.
 * @property {number} req.query.skip - Number of gameTypes to be skipped.
 * @property {number} req.query.limit - Limit number of gameTypes to be returned.
 * @returns {GameType[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  GameType.list({ limit, skip })
    .then(gameTypes => res.json(gameTypes))
    .catch(e => next(e));
}

/**
 * Delete gameType.
 * @returns {GameType}
 */
function remove(req, res, next) {  
  GameType.findOneAndRemove({ _id: req.params.gameTypeId })
    .then(gameType => {
      if (!gameType) {
        let error = new APIError('GameType not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(gameType);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
