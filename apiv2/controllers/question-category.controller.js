
import httpStatus from 'http-status';
import QuestionCategory from '../models/game-questions.model';
import APIError from '../helpers/APIError';

/**
 * Get questionCategory
 * @returns {QuestionCategory}
 */
function get(req, res) {
  return res.json(req.questionCategory);
}

/**
 * Create new questionCategory
 * @property {string} req.body.category_id - The category_id of QuestionCategory.
 * @property {string} req.body.question_id - The question_id of QuestionCategory.
 * @returns {QuestionCategory}
 */
function create(req, res, next) {
  const questionCategory = new QuestionCategory({
    category_id: req.body.category_id, 
    question_id: req.body.question_id, 
     
  });
  
  questionCategory.save() 
  .then(savedQuestionCategory => res.json(savedQuestionCategory))
  .catch(e => next(e));
  }

/**
 * Update existing questionCategory
 * @property {string} req.body.game_id - The game_id of QuestionCategory.
 * @property {string} req.body.question_id - The question_id of QuestionCategory.
 * @returns {QuestionCategory}
 */
function update(req, res, next) {

  QuestionCategory.findOne({ _id: req.params.questionCategoryId }).then(questionCategory => {
    questionCategory.category_id = req.body.category_id, 
    questionCategory.question_id = req.body.question_id, 
  
    questionCategory.save()
      .then(savedQuestionCategory => res.json(savedQuestionCategory))
      .catch(e => next(e));
  });
}

/**
 * Get questionCategory list.
 * @property {number} req.query.skip - Number of questionCategorys to be skipped.
 * @property {number} req.query.limit - Limit number of questionCategorys to be returned.
 * @returns {QuestionCategory[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  QuestionCategory.list({ limit, skip })
    .then(questionCategorys => res.json(questionCategorys))
    .catch(e => next(e));
}

/**
 * Delete questionCategory.
 * @returns {QuestionCategory}
 */
function remove(req, res, next) {  
  QuestionCategory.findOneAndRemove({ _id: req.params.questionCategoryId })
    .then(questionCategory => {
      if (!questionCategory) {
        let error = new APIError('QuestionCategory not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(questionCategory);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
