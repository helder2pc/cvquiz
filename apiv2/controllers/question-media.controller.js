import httpStatus from 'http-status';
import QuestionMedia from '../models/question-medias.model';
import APIError from '../helpers/APIError';

/**
 * Get questionMedia
 * @returns {QuestionMedia}
 */
function get(req, res) {
  return res.json(req.questionMedia);
}

/**
 * Create new questionMedia
 * @property {string} req.body.question_id - The question_id of QuestionMedia.
 * @property {string} req.body.media_id - The media_id of QuestionMedia.
 * @returns {QuestionMedia}
 */
function create(req, res, next) {
  const questionMedia = new QuestionMedia({
    question_id: req.body.question_id,
    media_id: req.body.media_id,
  });

  questionMedia.save()
    .then(savedQuestionMedia => res.json(savedQuestionMedia))
    .catch(e => next(e));
}

/**
 * Update existing questionMedia
 * @property {string} req.body.question_id - The question_id of QuestionMedia.
 * @property {string} req.body.media_id - The media_id of QuestionMedia.
 * @returns {QuestionMedia}
 */
function update(req, res, next) {

  QuestionMedia.findOne({
    _id: req.params.questionMediaId
  }).then(questionMedia => {
    questionMedia.question_id = req.body.question_id,
      questionMedia.media_id = req.body.media_id,

      questionMedia.save()
      .then(savedQuestionMedia => res.json(savedQuestionMedia))
      .catch(e => next(e));
  });
}


/**
 * Get questionMedia list.
 * @property {number} req.query.skip - Number of questionMedias to be skipped.
 * @property {number} req.query.limit - Limit number of questionMedias to be returned.
 * @returns {QuestionMedia[]} 
 */
function list(req, res, next) {
  const {
    limit = 50, skip = 0
  } = req.query;
  QuestionMedia.list({
      limit,
      skip
    })
    .then(questionMedias => res.json(questionMedias))
    .catch(e => next(e));
}

/**
 * Delete questionMedia.
 * @returns {QuestionMedia}
 */
function remove(req, res, next) {

  QuestionMedia.findOneAndRemove({
      _id: req.params.questionmediaId
    })
    .then(questionMedia => {
      if (!questionMedia) {
        let error = new APIError('QuestionMedia not found', httpStatus.NOT_FOUND);
        next(error);
      }

      res.json(questionMedia);
    })
    .catch(e => next(e));
}

/**
 * HELPERS
 */

async function associate(questionId, mediaIds, removeOld = false) {

  if (removeOld) {
    await QuestionMedia.remove({
      question_id: req.params.questionId
    });
  }

  mediaIds.forEach(mediaId => {

    const questionMedia = new QuestionMedia({
      question_id: questionId,
      media_id: mediaId,
    });

    questionMedia.save()
      .catch(e => next(e));
  });
}


export default {
  get,
  update,
  create,
  list,
  remove,

  associate
};
