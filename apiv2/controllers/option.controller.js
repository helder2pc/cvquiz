
import httpStatus from 'http-status';
import Option from '../models/option.model';
import APIError from '../helpers/APIError';

/**
 * Get option
 * @returns {Option}
 */
function get(req, res) {
  return res.json(req.option);
}

/**
 * Create new option
 * @property {string} req.body.answer - The answer of Option.
 * @property {string} req.body.question_id - The question_id of Option.
 * @property {string} req.body.feedback - The feedback of Option.
 * @property {string} req.body.order - The order of Option.
 * @property {string} req.body.correct - The correct of Option.
 * @returns {Option}
 */
function create(req, res, next) {
  const option = new Option({
    answer: req.body.answer, 
    question_id: req.body.question_id, 
    feedback: req.body.feedback, 
    order: req.body.order, 
    correct: req.body.correct, 
  });
  
  option.save() 
  .then(savedOption => res.json(savedOption))
  .catch(e => next(e));
  }

/**
 * Update existing option
 * @property {string} req.body.answer - The answer of Option.
 * @property {string} req.body.question_id - The question_id of Option.
 * @property {string} req.body.feedback - The feedback of Option.
 * @property {string} req.body.order - The order of Option.
 * @property {string} req.body.correct - The correct of Option.
 * @returns {Option}
 */
function update(req, res, next) {

  Option.findOne({ _id: req.params.optionId }).then(option => {
    option.answer = req.body.answer, 
    option.question_id = req.body.question_id, 
    option.feedback = req.body.feedback, 
    option.order = req.body.order, 
    option.correct = req.body.correct, 
  
    option.save()
      .then(savedOption => res.json(savedOption))
      .catch(e => next(e));
  });
}


/**
 * Get option list.
 * @property {number} req.query.skip - Number of options to be skipped.
 * @property {number} req.query.limit - Limit number of options to be returned.
 * @returns {Option[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Option.list({ limit, skip })
    .then(options => res.json(options))
    .catch(e => next(e));
}

/**
 * Delete option.
 * @returns {Option}
 */
function remove(req, res, next) {  
  Option.findOneAndRemove({ _id: req.params.optionId })
    .then(option => {
      if (!option) {
        let error = new APIError('Option not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(option);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
