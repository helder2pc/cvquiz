
import httpStatus from 'http-status';
import Reward from '../models/reward.model';
import APIError from '../helpers/APIError';

/**
 * Get reward
 * @returns {Reward}
 */
function get(req, res) {
  return res.json(req.reward);
}

/**
 * Create new reward
 * @property {string} req.body.name - The name of Reward.
 * @property {string} req.body.description - The description of Reward.
 * @property {string} req.body.category - The category of Reward.
 * @property {string} req.body.completion_points - The completion_points of Reward.
 * @property {string} req.body.image - The image of Reward.
 * @returns {Reward}
 */
function create(req, res, next) {
  const reward = new Reward({
    name: req.body.name, 
    description: req.body.description, 
    category: req.body.category, 
    completion_points: req.body.completion_points, 
    image: req.body.image, 
  });
  
  reward.save() 
  .then(savedReward => res.json(savedReward))
  .catch(e => next(e));
  }

/**
 * Update existing reward
 * @property {string} req.body.name - The name of Reward.
 * @property {string} req.body.description - The description of Reward.
 * @property {string} req.body.category - The category of Reward.
 * @property {string} req.body.completion_points - The completion_points of Reward.
 * @property {string} req.body.image - The image of Reward.
 * @returns {Reward}
 */
function update(req, res, next) {

  Reward.findOne({ _id: req.params.rewardId }).then(reward => {
    reward.name = req.body.name, 
    reward.description = req.body.description, 
    reward.category = req.body.category, 
    reward.completion_points = req.body.completion_points, 
    reward.image = req.body.image, 
  
    reward.save()
      .then(savedReward => res.json(savedReward))
      .catch(e => next(e));
  });
}


/**
 * Get reward list.
 * @property {number} req.query.skip - Number of rewards to be skipped.
 * @property {number} req.query.limit - Limit number of rewards to be returned.
 * @returns {Reward[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Reward.list({ limit, skip })
    .then(rewards => res.json(rewards))
    .catch(e => next(e));
}

/**
 * Delete reward.
 * @returns {Reward}
 */
function remove(req, res, next) {  
  Reward.findOneAndRemove({ _id: req.params.rewardId })
    .then(reward => {
      if (!reward) {
        let error = new APIError('Reward not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(reward);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
