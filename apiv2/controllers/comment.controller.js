
import httpStatus from 'http-status';
import Comment from '../models/comment.model';
import APIError from '../helpers/APIError';

/**
 * Get comment
 * @returns {Comment}
 */
function get(req, res) {
  return res.json(req.comment);
}

/**
 * Create new comment
 * @property {string} req.body.content - The content of Comment.
 * @property {string} req.body.post - The post of Comment.
 * @property {string} req.body.reply_to - The reply_to of Comment.
 * @property {string} req.body.sender_id - The sender_id of Comment.
 * @returns {Comment}
 */
function create(req, res, next) {
  const comment = new Comment({
    content: req.body.content, 
    post: req.body.post, 
    reply_to: req.body.reply_to, 
    sender_id: req.body.sender_id, 
  });
  
  comment.save() 
  .then(savedComment => res.json(savedComment))
  .catch(e => next(e));
  }

/**
 * Update existing comment
 * @property {string} req.body.content - The content of Comment.
 * @property {string} req.body.post - The post of Comment.
 * @property {string} req.body.reply_to - The reply_to of Comment.
 * @property {string} req.body.sender_id - The sender_id of Comment.
 * @returns {Comment}
 */
function update(req, res, next) {

  Comment.findOne({ _id: req.params.commentId }).then(comment => {
    comment.content = req.body.content, 
    comment.post = req.body.post, 
    comment.reply_to = req.body.reply_to, 
    comment.sender_id = req.body.sender_id,  
  
    comment.save()
      .then(savedComment => res.json(savedComment))
      .catch(e => next(e));
  });
}


/**
 * Get comment list.
 * @property {number} req.query.skip - Number of comments to be skipped.
 * @property {number} req.query.limit - Limit number of comments to be returned.
 * @returns {Comment[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Comment.list({ limit, skip })
    .then(comments => res.json(comments))
    .catch(e => next(e));
}

/**
 * Delete comment.
 * @returns {Comment}
 */
function remove(req, res, next) {  
  Comment.findOneAndRemove({ _id: req.params.commentId })
    .then(comment => {
      if (!comment) {
        let error = new APIError('Comment not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(comment);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
