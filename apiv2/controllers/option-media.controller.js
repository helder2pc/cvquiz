
import httpStatus from 'http-status';
import OptionMedias from '../models/option-medias.model';
import APIError from '../helpers/APIError';

/**
 * Get optionMedias
 * @returns {OptionMedias}
 */
function get(req, res) {
  return res.json(req.optionMedias);
}

/**
 * Create new optionMedias
* @property {string} req.body.option_id - The option_id of OptionMedias.
 * @property {string} req.body.media_id - The media_id of OptionMedias.
 * @returns {OptionMedias}
 */
function create(req, res, next) {
  const optionMedias = new OptionMedias({
    option_id: req.body.option_id, 
    media_id: req.body.question_id, 
     
  });
  
  optionMedias.save() 
  .then(savedOptionMedias => res.json(savedOptionMedias))
  .catch(e => next(e));
  }

/**
 * Update existing optionMedias
 * @property {string} req.body.option_id - The option_id of OptionMedias.
 * @property {string} req.body.media_id - The media_id of OptionMedias.
 * @returns {OptionMedias}
 */
function update(req, res, next) {

  OptionMedias.findOne({ _id: req.params.optionMediasId }).then(optionMedias => {
    optionMedias.option_id= req.body.option_id, 
    optionMedias.media_id= req.body.question_id,  
  
    optionMedias.save()
      .then(savedOptionMedias => res.json(savedOptionMedias))
      .catch(e => next(e));
  });
}


/**
 * Get optionMedias list.
 * @property {number} req.query.skip - Number of optionMediass to be skipped.
 * @property {number} req.query.limit - Limit number of optionMediass to be returned.
 * @returns {OptionMedias[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  OptionMedias.list({ limit, skip })
    .then(optionMediass => res.json(optionMediass))
    .catch(e => next(e));
}

/**
 * Delete optionMedias.
 * @returns {OptionMedias}
 */
function remove(req, res, next) {  
  OptionMedias.findOneAndRemove({ _id: req.params.optionMediasId })
    .then(optionMedias => {
      if (!optionMedias) {
        let error = new APIError('OptionMedias not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(optionMedias);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
