
import httpStatus from 'http-status';
import Message from '../models/message.model';
import APIError from '../helpers/APIError';

/**
 * Get message
 * @returns {Message}
 */
function get(req, res) {
  return res.json(req.message);
}

/**
 * Create new message
 * @property {string} req.body.body - The body of Message.
 * @property {string} req.body.sender_id - The sender_id of Message.
  * @returns {Message}
 */
function create(req, res, next) {
  const message = new Message({
    body: req.body.body, 
    sender_id:  req.body.sender_id,
     });
  
  message.save() 
  .then(savedMessage => res.json(savedMessage))
  .catch(e => next(e));
  }

/**
 * Update existing message
 * @property {string} req.body.body - The body of Message.
 * @property {string} req.body.sender_id - The sender_id of Message.
 * @returns {Message}
 */
function update(req, res, next) {

  Message.findOne({ _id: req.params.messageId }).then(message => {
    message.body = req.body.body,
    sender_id.body = req.body.sender_id,

    message.save()
      .then(savedMessage => res.json(savedMessage))
      .catch(e => next(e));
  });
}


/**
 * Get message list.
 * @property {number} req.query.skip - Number of messages to be skipped.
 * @property {number} req.query.limit - Limit number of messages to be returned.
 * @returns {Message[]} 
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Message.list({ limit, skip })
    .then(messages => res.json(messages))
    .catch(e => next(e));
}

/**
 * Delete message.
 * @returns {Message}
 */
function remove(req, res, next) {  
  Message.findOneAndRemove({ _id: req.params.messageId })
    .then(message => {
      if (!message) {
        let error = new APIError('Message not found', httpStatus.NOT_FOUND);
        next(error);
      }  

      res.json(message);
    })
    .catch(e => next(e));
}

export default {  get, update,  create, list, remove };
