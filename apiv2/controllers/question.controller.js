import httpStatus from 'http-status';
import Question from '../models/question.model';
import APIError from '../helpers/APIError';
import QuestionMediaCtrl from '../controllers/question-media.controller';

/**
 * Get question
 * @returns {Question}
 */
function get(req, res) {
  return res.json(req.question);
}

/**
 * Create new question
 * @property {string} req.body.question - The question of Question.
 * @property {string} req.body.description - The description of Question.
 * @property {string} req.body.difficulty - The difficulty of Question.
 * @property {string} req.body.type - The type of Question.
 * @returns {Question}
 */
function create(req, res, next) {
  const question = new Question({
    question: req.body.question,
    description: req.body.description,
    difficulty: req.body.difficulty,
    type: req.body.type,
  });

  question.save()
    .then(savedQuestion => { req.responseEntity = savedQuestion; res.json(savedQuestion); next(); })
    .catch(e => next(e));
}

/**
 * Update existing question
 * @property {string} req.body.question - The question of Question.
 * @property {string} req.body.description - The description of Question.
 * @property {string} req.body.difficulty - The difficulty of Question.
 * @property {string} req.body.type - The type of Question.
 * @returns {Question}
 */
function update(req, res, next) {

  Question.findOne({
    _id: req.params.questionId
  }).then(question => {
    question.question = req.body.question,
      question.description = req.body.description,
      question.difficulty = req.body.difficulty,
      question.type = req.body.type,

      question.save()
      .then(savedQuestion => res.json(savedQuestion))
      .catch(e => next(e));
  });
}


/**
 * Get question list.
 * @property {number} req.query.skip - Number of questions to be skipped.
 * @property {number} req.query.limit - Limit number of questions to be returned.
 * @returns {Question[]} 
 */
function list(req, res, next) {
  const {
    limit = 50, skip = 0
  } = req.query;
  Question.list({
      limit,
      skip
    })
    .then(questions => res.json(questions))
    .catch(e => next(e));
}

/**
 * Delete question.
 * @returns {Question}
 */
function remove(req, res, next) {
  Question.findOneAndRemove({
      _id: req.params.questionId
    })
    .then(question => {
      if (!question) {
        let error = new APIError('Question not found', httpStatus.NOT_FOUND);
        next(error);
      }

      res.json(question);
    })
    .catch(e => next(e));
}

/** After Hooks */

async function associateMedias(req, res, next) {
  console.log(req.responseEntity);
  
  let mediaIds = req.body.media_ids;
  if (mediaIds && Array.isArray(mediaIds)) {
    await QuestionMediaCtrl.associate(req.responseEntity._id, mediaIds);
  }
  
  res.end();
}


export default {
  get,
  update,
  create,
  list,
  remove,

  associateMedias
};
