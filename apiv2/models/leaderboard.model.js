'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LeaderboardSchema = Schema({

    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    media: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    
    
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
LeaderboardSchema.statics = {
    /**
     * Get leaderboard
     * @param {ObjectId} id - The objectId of leaderboard.
     * @returns {Promise<leaderboard, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((leaderboard) => {
          if (leaderboard) {
            return leaderboard;
          }
          const err = new APIError('No such leaderboard exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List leaderboards in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of leaderboards to be skipped.
     * @param {number} limit - Limit number of leaderboards to be returned.
     * @returns {Promise<Leaderboard[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Leaderboard
    */
module.exports = mongoose.model('Leaderboard', LeaderboardSchema);