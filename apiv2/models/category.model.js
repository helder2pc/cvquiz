'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = Schema({

    name: {
        type: String,
        trim: true
    },
    description: {
        type: String,
    },
    sub_categories: [{
        type: Schema.ObjectId,
        ref: 'Category'
    }],
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
CategorySchema.statics = {
    /**
     * Get category
     * @param {ObjectId} id - The objectId of category.
     * @returns {Promise<category, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((category) => {
          if (category) {
            return category;
          }
          const err = new APIError('No such category exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List categorys in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of categorys to be skipped.
     * @param {number} limit - Limit number of categorys to be returned.
     * @returns {Promise<Category[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Category
    */

module.exports = mongoose.model('Category', CategorySchema);