'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserLevelSchema = Schema({

    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    
    level_id: {
        type: Schema.ObjectId,
        ref: 'Level'
    },
    xp: {
        type: Number
    },
    state: {
        type: String,
        enum: ['progressing', 'achieved' ],
        required: true,
    },
    
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
UserLevelSchema.statics = {
    /**
     * Get option
     * @param {ObjectId} id - The objectId of option.
     * @returns {Promise<option, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((option) => {
          if (option) {
            return option;
          }
          const err = new APIError('No such option exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List options in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of options to be skipped.
     * @param {number} limit - Limit number of options to be returned.
     * @returns {Promise<UserLevel[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef UserLevel
    */
module.exports = mongoose.model('UserLevel', UserLevelSchema);