'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ScoreSchema = Schema({

    score: {
        type: Number,
        required: true
    },
    
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
ScoreSchema.statics = {
    /**
     * Get score
     * @param {ObjectId} id - The objectId of score.
     * @returns {Promise<score, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((score) => {
          if (score) {
            return score;
          }
          const err = new APIError('No such score exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List scores in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of scores to be skipped.
     * @param {number} limit - Limit number of scores to be returned.
     * @returns {Promise<Score[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Score
    */
module.exports = mongoose.model('Score', ScoreSchema);