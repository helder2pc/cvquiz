'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OptionSchema = Schema({

    answer: {
        type: String,
        required: true
    },
    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    feedback: {
        type: String
    },
    order: {
        type: Number,
        default: 0
    },
    correct: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
OptionSchema.statics = {
    /**
     * Get option
     * @param {ObjectId} id - The objectId of option.
     * @returns {Promise<option, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((option) => {
          if (option) {
            return option;
          }
          const err = new APIError('No such option exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List options in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of options to be skipped.
     * @param {number} limit - Limit number of options to be returned.
     * @returns {Promise<Option[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Option
    */
module.exports = mongoose.model('Option', OptionSchema);