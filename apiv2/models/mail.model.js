'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MailSchema = Schema({

    subject: {
        type: String,
        require: true,
    },
    body: {
        type: String,
        require: true,
    },
   
    sender_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
    }, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});


/**
 * Statics
 */
MailSchema.statics = {
    /**
     * Get mail
     * @param {ObjectId} id - The objectId of mail.
     * @returns {Promise<mail, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((mail) => {
          if (mail) {
            return mail;
          }
          const err = new APIError('No such mail exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List mails in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of mails to be skipped.
     * @param {number} limit - Limit number of mails to be returned.
     * @returns {Promise<Mail[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Mail
    */
module.exports = mongoose.model('Mail', MailSchema);