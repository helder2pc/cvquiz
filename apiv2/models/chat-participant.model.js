'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChatParticipantsSchema = Schema({

    chat_id: {
        type: Schema.ObjectId,
        ref: 'Chat'
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
ChatParticipantsSchema.statics = {
    /**
     * Get participant
     * @param {ObjectId} id - The objectId of participant.
     * @returns {Promise<participant, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((participant) => {
          if (participant) {
            return participant;
          }
          const err = new APIError('No such participant exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List participants in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of participants to be skipped.
     * @param {number} limit - Limit number of participants to be returned.
     * @returns {Promise<Participant[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Participant
    */

module.exports = mongoose.model('ChatParticipants', ChatParticipantsSchema);