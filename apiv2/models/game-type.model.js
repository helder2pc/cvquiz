'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameTypeSchema = Schema({

    name: {
        type: String,
        required: true,
        trim: true
    },
    min_players: {
        type: Number,
        min: 1
    },
    max_players: {
        type: Number,
        min: 1
    },
    max_duration: { // time in seconds
        type: Number,
    },
    time_per_question: { // time in seconds
        type: Number,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
GameTypeSchema.statics = {
    /**
     * Get gameType
     * @param {ObjectId} id - The objectId of gameType.
     * @returns {Promise<gameType, APIError>}
     */
    get(id) { 
      return this.findById(id)
        .exec()
        .then((gameType) => {
          if (gameType) {
            return gameType;
          }
          const err = new APIError('No such gameType exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List gameTypes in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of gameTypes to be skipped.
     * @param {number} limit - Limit number of gameTypes to be returned.
     * @returns {Promise<GameType[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
    * @typedef GameType
    */

module.exports = mongoose.model('GameType', GameTypeSchema);