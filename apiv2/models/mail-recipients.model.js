'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MailRecipientsSchema = Schema({

    mail_id: {
        type: Schema.ObjectId,
        ref: 'Mail'
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
MailRecipientsSchema.statics = {
    /**
     * Get recipient
     * @param {ObjectId} id - The objectId of recipient.
     * @returns {Promise<recipient, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((recipient) => {
          if (recipient) {
            return recipient;
          }
          const err = new APIError('No such recipient exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List recipients in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of recipients to be skipped.
     * @param {number} limit - Limit number of recipients to be returned.
     * @returns {Promise<Recipient[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Recipient
    */
module.exports = mongoose.model('MailRecipientsSchema', MailRecipientsSchema);