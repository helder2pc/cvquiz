'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameQuestionsSchema = Schema({

    game_id: {
        type: Schema.ObjectId,
        ref: 'Game'
    },
    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
GameQuestionsSchema.statics = {
    /**
     * Get gameQuestion
     * @param {ObjectId} id - The objectId of gameQuestion.
     * @returns {Promise<gameQuestion, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((gameQuestion) => {
          if (gameQuestion) {
            return gameQuestion;
          }
          const err = new APIError('No such gameQuestion exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List gameQuestions in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of gameQuestions to be skipped.
     * @param {number} limit - Limit number of gameQuestions to be returned.
     * @returns {Promise<GameQuestion[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
    * @typedef GameQuestion
    */

module.exports = mongoose.model('GameQuestions', GameQuestionsSchema);