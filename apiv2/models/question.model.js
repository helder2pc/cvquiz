'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionSchema = Schema({

    question: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    difficulty: {
        type: String,
        enum: ['very_easy', 'easy', 'medium', 'difficult', 'very_difficult'],
        required: true,
    },
    type: {
        type: String,
        enum: ['mcq', 'mcq_ma', 'true_false', 'fill_in_blank', 'match', 'order', 'short_answer', 'hotspot'],
        required: true,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});


/**
 * Statics
 */
QuestionSchema.statics = {
    /**
     * Get question
     * @param {ObjectId} id - The objectId of question.
     * @returns {Promise<Question, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((question) => {
          if (question) {
            return question;
          }
          const err = new APIError('No such question exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List questions in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of questions to be skipped.
     * @param {number} limit - Limit number of questions to be returned.
     * @returns {Promise<User[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
 * @typedef Question
 */
module.exports = mongoose.model('Question', QuestionSchema);