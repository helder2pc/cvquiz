'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReactionSchema = Schema({
    type: {
        type: String,
        enum: ['like', 'dislike'],
        required: true
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    observable_id: {
        type: Schema.ObjectId,
        required: true
    },
    observable_type: {
        type: String,
        enum: ['Post', 'Comment', 'Message'],
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
ReactionSchema.statics = {
    /**
     * Get reaction
     * @param {ObjectId} id - The objectId of reaction.
     * @returns {Promise<reaction, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((reaction) => {
          if (reaction) {
            return reaction;
          }
          const err = new APIError('No such reaction exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List reactions in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of reactions to be skipped.
     * @param {number} limit - Limit number of reactions to be returned.
     * @returns {Promise<Reaction[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Reaction
    */

module.exports = mongoose.model('Reaction', ReactionSchema);