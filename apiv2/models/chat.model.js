'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChatSchema = Schema({

    name: {
        type: String,
        require: true,
    },
    messages: [{
        type: Schema.ObjectId,
        ref: 'Message'
    }],

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
ChatSchema.statics = {
    /**
     * Get chat
     * @param {ObjectId} id - The objectId of chat.
     * @returns {Promise<chat, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((chat) => {
          if (chat) {
            return chat;
          }
          const err = new APIError('No such chat exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List chats in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of chats to be skipped.
     * @param {number} limit - Limit number of chats to be returned.
     * @returns {Promise<Chat[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Chat
    */

module.exports = mongoose.model('Chat', ChatSchema);