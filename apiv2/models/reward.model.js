'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RewardSchema = Schema({

    name: {
        type: String,
        required: true
    },
    
    description: {
        type: String
    },
    
    category: {
        type: Schema.ObjectId,
        ref: 'Category'
    },
    completion_points: {
        type: Number,
        default: 0
    },
    image: {
        type: Schema.ObjectId,
        ref: 'Media'
    },
   
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
RewardSchema.statics = {
    /**
     * Get reward
     * @param {ObjectId} id - The objectId of reward.
     * @returns {Promise<reward, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((reward) => {
          if (reward) {
            return reward;
          }
          const err = new APIError('No such reward exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List rewards in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of rewards to be skipped.
     * @param {number} limit - Limit number of rewards to be returned.
     * @returns {Promise<Reward[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Reward
    */
module.exports = mongoose.model('Reward', RewardSchema);