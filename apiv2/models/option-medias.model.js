'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OptionMediasSchema = Schema({

    option_id: {
        type: Schema.ObjectId, 
        ref: 'OptionMedias'
    },
    media_id: {
        type: Schema.ObjectId,
        ref: 'Media'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});



/**
 * Statics
 */
OptionMediasSchema.statics = {
    /**
     * Get optionMedias
     * @param {ObjectId} id - The objectId of optionMedias.
     * @returns {Promise<optionMedias, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((optionMedias) => {
          if (optionMedias) {
            return optionMedias;
          }
          const err = new APIError('No such optionMedias exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List optionMediass in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of optionMediass to be skipped.
     * @param {number} limit - Limit number of optionMediass to be returned.
     * @returns {Promise<OptionMediasMedias[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef OptionMedias
    */
module.exports = mongoose.model('OptionMedias', OptionMediasSchema);