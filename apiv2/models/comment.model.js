'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = Schema({

    content: {
        type: String,
    },
    post: {
        type: Schema.ObjectId,
        ref: 'Post'
    },
    reply_to: {
        type: Schema.ObjectId,
        ref: 'Comment'
    },
    sender_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
CommentSchema.statics = {
    /**
     * Get comment
     * @param {ObjectId} id - The objectId of comment.
     * @returns {Promise<comment, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((comment) => {
          if (comment) {
            return comment;
          }
          const err = new APIError('No such comment exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List comments in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of comments to be skipped.
     * @param {number} limit - Limit number of comments to be returned.
     * @returns {Promise<Comment[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Comment
    */

module.exports = mongoose.model('Comment', CommentSchema);