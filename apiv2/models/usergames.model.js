'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserGamesSchema = Schema({

    game_id: {
        type: Schema.ObjectId,
        ref: 'Game'
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('UserGames', UserGamesSchema);