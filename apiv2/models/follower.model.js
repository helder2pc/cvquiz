'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const FollowerSchema = Schema({

    user_id: { 
        type: Schema.ObjectId,
        ref: 'User'
    },
    followed_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
 

/**
 * Statics
 */
FollowerSchema.statics = {
    /**
     * Get followers
     * @param {ObjectId} id - The objectId of followers.
     * @returns {Promise<Follower, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((follower) => {
          if (follower) {
            return follower;
          }
          const err = new APIError('No such follower exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List followers in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of followers to be skipped.
     * @param {number} limit - Limit number of followers to be returned.
     * @returns {Promise<Follower[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
 * @typedef Follower
 */
module.exports = mongoose.model('Follower', FollowerSchema);