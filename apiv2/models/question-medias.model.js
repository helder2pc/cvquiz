'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionMediasSchema = Schema({

    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    media_id: {
        type: Schema.ObjectId,
        ref: 'Media'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
QuestionMediasSchema.statics = {
    /**
     * Get questionMedia
     * @param {ObjectId} id - The objectId of questionMedia.
     * @returns {Promise<questionMedia, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((questionMedia) => {
          if (questionMedia) {
            return questionMedia;
          }
          const err = new APIError('No such questionMedia exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List questionMedias in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of questionMedias to be skipped.
     * @param {number} limit - Limit number of questionMedias to be returned.
     * @returns {Promise<QuestionMedia[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
    * @typedef QuestionMedia
    */

module.exports = mongoose.model('QuestionMedias', QuestionMediasSchema);