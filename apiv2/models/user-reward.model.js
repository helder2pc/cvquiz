'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserRewardSchema = Schema({

    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    reward_id: {
        type: Schema.ObjectId,
        ref: 'Reward'
    },
    points: {
        type: Number
    },
   
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
UserRewardSchema.statics = {
    /**
     * Get userReward
     * @param {ObjectId} id - The objectId of userReward.
     * @returns {Promise<userReward, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((userReward) => {
          if (userReward) {
            return userReward;
          }
          const err = new APIError('No such userReward exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List userRewards in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of userRewards to be skipped.
     * @param {number} limit - Limit number of userRewards to be returned.
     * @returns {Promise<UserReward[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef UserReward
    */
module.exports = mongoose.model('UserReward', UserRewardSchema);