'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameSchema = Schema({
    gametype_id: {
        type: Schema.Types.ObjectId,
        ref: 'Game',
        required: true
    },
    state: {
        type: String,
        enum: ['created', 'ready', 'running', 'finished'],
        default: 'created'
    },
    start_time: {
        type: Date
    },
    end_time: {
        type: Date
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
GameSchema.statics = {
    /**
     * Get game
     * @param {ObjectId} id - The objectId of game.
     * @returns {Promise<game, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((game) => {
          if (game) {
            return game;
          }
          const err = new APIError('No such game exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List games in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of games to be skipped.
     * @param {number} limit - Limit number of games to be returned.
     * @returns {Promise<Game[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Game
    */

module.exports = mongoose.model('Game', GameSchema);