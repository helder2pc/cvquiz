'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = Schema({

    title: {
        type: String,
        require: true
    },
    summary: {
        type: String,
        require: true
    },
    content: {
        type: String
    },
    category_id: {
        type: Schema.ObjectId,
        ref: 'Category'
    },
    publisher_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});


/**
 * Statics
 */
PostSchema.statics = {
    /**
     * Get post
     * @param {ObjectId} id - The objectId of post.
     * @returns {Promise<post, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((post) => {
          if (post) {
            return post;
          }
          const err = new APIError('No such post exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List posts in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of posts to be skipped.
     * @param {number} limit - Limit number of posts to be returned.
     * @returns {Promise<Post[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Post
    */
module.exports = mongoose.model('Post', PostSchema);