'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionCategoriesSchema = Schema({

    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    category_id: {
        type: Schema.ObjectId,
        ref: 'Category'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
QuestionCategorySchema.statics = {
    /**
     * Get questionCategory
     * @param {ObjectId} id - The objectId of questionCategory.
     * @returns {Promise<questionCategory, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((questionCategory) => {
          if (questionCategory) {
            return questionCategory;
          }
          const err = new APIError('No such questionCategory exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List questionCategorys in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of questionCategorys to be skipped.
     * @param {number} limit - Limit number of questionCategorys to be returned.
     * @returns {Promise<QuestionCategory[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef QuestionCategory
    */

module.exports = mongoose.model('QuestionCategories', QuestionCategoriesSchema);