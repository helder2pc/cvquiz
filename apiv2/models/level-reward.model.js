'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LevelRewardSchema = Schema({

    level_id: {
        type: Schema.ObjectId,
        ref: 'Level'
    },
    reward_id: {
        type: Schema.ObjectId,
        ref: 'Reward'
    },
   
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
LevelRewardSchema.statics = {
    /**
     * Get levelReward
     * @param {ObjectId} id - The objectId of levelReward.
     * @returns {Promise<levelReward, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((levelReward) => {
          if (levelReward) {
            return levelReward;
          }
          const err = new APIError('No such levelReward exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List levelRewards in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of levelRewards to be skipped.
     * @param {number} limit - Limit number of levelRewards to be returned.
     * @returns {Promise<LevelReward[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef LevelReward
    */
module.exports = mongoose.model('LevelReward', LevelRewardSchema);