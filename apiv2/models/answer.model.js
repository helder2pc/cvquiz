'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnswerSchema = Schema({ 

    time_spent: {
        type: Number,
        default: 0,
    },
    attempts: {
        type: Number,
        default: 0,
    },
    game_id: {
        type: Schema.ObjectId,
        ref: 'Game',
        required: true
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    question_id: {
        type: Schema.ObjectId,
        ref: 'Question',
        required: true
    },
    option_id: {
        type: Schema.ObjectId,
        ref: 'Answer',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
AnswerSchema.statics = {
    /**
     * Get answer
     * @param {ObjectId} id - The objectId of answer.
     * @returns {Promise<Answer, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((answer) => {
          if (answer) {
            return answer;
          }
          const err = new APIError('No such answer exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List answers in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of answers to be skipped.
     * @param {number} limit - Limit number of answers to be returned.
     * @returns {Promise<Answer[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
    * @typedef Answer
    */
module.exports = mongoose.model('Answer', AnswerSchema);