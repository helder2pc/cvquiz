'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MessageSchema = Schema({

    body: {
        type: String,
        require: true,
    },
    sender_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
MessageSchema.statics = {
    /**
     * Get message
     * @param {ObjectId} id - The objectId of message.
     * @returns {Promise<message, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((message) => {
          if (message) {
            return message;
          }
          const err = new APIError('No such message exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List messages in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of messages to be skipped.
     * @param {number} limit - Limit number of messages to be returned.
     * @returns {Promise<Message[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Message
    */

module.exports = mongoose.model('Message', MessageSchema);