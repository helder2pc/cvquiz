'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MediaSchema = Schema({
    type: {
        type: String,
        enum: ['video', 'image', 'binary', 'text'],
        required: true
    },
    name: {
        type: String,
        required: true
    },
    file: {
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});


/**
 * Statics
 */
MediaSchema.statics = {
    /**
     * Get medias
     * @param {ObjectId} id - The objectId of medias.
     * @returns {Promise<Medias, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((media) => {
          if (media) {
            return media;
          }
          const err = new APIError('No such media exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List medias in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of medias to be skipped.
     * @param {number} limit - Limit number of medias to be returned.
     * @returns {Promise<Media[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    }
  };

  /**
 * @typedef Media
 */
module.exports = mongoose.model('Media', MediaSchema);