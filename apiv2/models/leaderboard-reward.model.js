'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LeaderBoardRewardSchema = Schema({
  
    reward_id: {
        type: Schema.ObjectId,
        ref: 'Reward'
    },
    leaderboard_id: {
        type: Schema.ObjectId,
        ref: 'Leaderboard'
    },
    
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
LeaderBoardRewardSchema.statics = {
    /**
     * Get leaderBoardReward
     * @param {ObjectId} id - The objectId of leaderBoardReward.
     * @returns {Promise<leaderBoardReward, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((leaderBoardReward) => {
          if (leaderBoardReward) {
            return leaderBoardReward;
          }
          const err = new APIError('No such leaderBoardReward exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List leaderBoardRewards in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of leaderBoardRewards to be skipped.
     * @param {number} limit - Limit number of leaderBoardRewards to be returned.
     * @returns {Promise<LeaderBoardReward[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef LeaderBoardReward
    */
module.exports = mongoose.model('LeaderBoardReward', LeaderBoardRewardSchema);