'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LevelSchema = Schema({

    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    
    xp: {
        type: Number
    },

    image: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
  
  
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

/**
 * Statics
 */
LevelSchema.statics = {
    /**
     * Get level
     * @param {ObjectId} id - The objectId of level.
     * @returns {Promise<level, APIError>}
     */
    get(id) {
      return this.findById(id)
        .exec()
        .then((level) => {
          if (level) {
            return level;
          }
          const err = new APIError('No such level exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
    },
  
    /**
     * List levels in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of levels to be skipped.
     * @param {number} limit - Limit number of levels to be returned.
     * @returns {Promise<Level[]>}
     */
    list({
      skip = 0,
      limit = 50
    } = {}) {
      return this.find()
        .sort({
          created_at: -1
        })
        .skip(+skip)
        .limit(+limit)
        .exec();
    } 
  };

  /**
    * @typedef Level
    */
module.exports = mongoose.model('Level', LevelSchema);