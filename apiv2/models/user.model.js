import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import crypto from 'crypto';
import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;

const UserSchema = new Schema({

  first_name: {
    type: String,
    trim: true
  },
  last_name: {
    type: String,
    trim: true
  },
  username: {
    type: String,
    trim: true,
    lowercase: true,
    required: [true, 'can\'t be blank'],
    match: [/^[a-zA-Z0-9_]+$/, 'is invalid'],
    index: true,
    unique: [true, 'is already taken.']
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    required: [true, 'can\'t be blank'],
    match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'is invalid'],
    index: true,
    unique: [true, 'is already taken.']
  },
  hash: String,
  salt: String,
  role: [{
    type: String
  }],
  photo: {
    type: Schema.ObjectId,
    ref: 'Media'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UserSchema.method({

  setPassword(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
  },

  validPassword(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
  }
});

/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @param {string[]} relations - Relations to populate.
   * @returns {Promise<User, APIError>}
   */
  get(id, relations = []) {
    let findQuery = this.findById(id);
    relations.forEach(relation => {
      findQuery = findQuery.populate(relation);
    });
    return findQuery
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @param {string[]} relations - Relations to populate.
   * @returns {Promise<User[]>}
   */
  list({
    skip = 0,
    limit = 50,
    relations = []
  } = {}) {
    let findQuery = this.find();
    relations.forEach(relation => {
      findQuery = findQuery.populate(relation);
    });
    return findQuery
      .sort({
        created_at: -1
      })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

/**
 * @typedef User
 */
module.exports = mongoose.model('User', UserSchema);
