import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import gameCtrl from '../controllers/game.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/games - Get list of games */
  .get(expressJwt({
    secret: config.jwtSecret
  }), gameCtrl.list)

  /** POST /api/games - Create new game */
  //.post(validate(paramValidation.createGame), gameCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createGame), gameCtrl.create);

router.route('/:gameId')
  /** GET /api/games/:gameId - Get game */
  .get(expressJwt({
    secret: config.jwtSecret
  }), gameCtrl.get)

 /** PUT /api/users/:gameId - Update game */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateGame), gameCtrl.update)
  
  /** DELETE /api/games/:gameId - Delete game */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), gameCtrl.remove);

export default router;
