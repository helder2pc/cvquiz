import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import leaderboardRewardCtrl from '../controllers/leaderboard-reward.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/leaderboardRewards - Get list of leaderboardRewards */
  .get(expressJwt({
    secret: config.jwtSecret
  }), leaderboardRewardCtrl.list)

  /** POST /api/leaderboardRewards - Create new leaderboardReward */
  //.post(validate(paramValidation.createLeaderboardReward), leaderboardRewardCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createLeaderboardReward), leaderboardRewardCtrl.create);

router.route('/:leaderboardRewardId')
  /** GET /api/leaderboardRewards/:leaderboardRewardId - Get leaderboardReward */
  .get(expressJwt({
    secret: config.jwtSecret
  }), leaderboardRewardCtrl.get)

 /** PUT /api/users/:leaderboardRewardId - Update leaderboardReward */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateLeaderboardReward), leaderboardRewardCtrl.update)
  
  /** DELETE /api/leaderboardRewards/:leaderboardRewardId - Delete leaderboardReward */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), leaderboardRewardCtrl.remove);

export default router;
