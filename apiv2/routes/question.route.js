import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import questionCtrl from '../controllers/question.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/questions - Get list of questions */
  .get(expressJwt({
    secret: config.jwtSecret
  }), questionCtrl.list)

  /** POST /api/questions - Create new question */
  //.post(validate(paramValidation.createQuestion), questionCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createQuestion), questionCtrl.create, questionCtrl.associateMedias);

router.route('/:questionId')
  /** GET /api/questions/:questionId - Get question */
  .get(expressJwt({
    secret: config.jwtSecret
  }), questionCtrl.get)

 /** PUT /api/users/:questionId - Update question */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateQuestion), questionCtrl.update)
  
  /** DELETE /api/questions/:questionId - Delete question */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), questionCtrl.remove);

export default router;
