import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import questionCategoryCtrl from '../controllers/question-category.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/questionCategorys - Get list of questionCategorys */
  .get(expressJwt({
    secret: config.jwtSecret
  }), questionCategoryCtrl.list)

  /** POST /api/questionCategorys - Create new questionCategory */
  //.post(validate(paramValidation.createQuestionCategory), questionCategoryCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createQuestionCategory), questionCategoryCtrl.create);

router.route('/:questionCategoryId')
  /** GET /api/questionCategorys/:questionCategoryId - Get questionCategory */
  .get(expressJwt({
    secret: config.jwtSecret
  }), questionCategoryCtrl.get)

 /** PUT /api/users/:questionCategoryId - Update questionCategory */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateQuestionCategory), questionCategoryCtrl.update)
  
  /** DELETE /api/questionCategorys/:questionCategoryId - Delete questionCategory */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), questionCategoryCtrl.remove);

export default router;
