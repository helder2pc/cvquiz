import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import gameTypeCtrl from '../controllers/game-type.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/gameTypes - Get list of gameTypes */
  .get(expressJwt({
    secret: config.jwtSecret
  }), gameTypeCtrl.list)

  /** POST /api/gameTypes - Create new gameType */
  //.post(validate(paramValidation.createGameType), gameTypeCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createGameType), gameTypeCtrl.create);

router.route('/:gameTypeId')
  /** GET /api/gameTypes/:gameTypeId - Get gameType */
  .get(expressJwt({
    secret: config.jwtSecret
  }), gameTypeCtrl.get)

 /** PUT /api/users/:gameTypeId - Update gameType */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateGameType), gameTypeCtrl.update)
  
  /** DELETE /api/gameTypes/:gameTypeId - Delete gameType */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), gameTypeCtrl.remove);

export default router;
