import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import categoryCtrl from '../controllers/category.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/categorys - Get list of categorys */
  .get(expressJwt({
    secret: config.jwtSecret
  }), categoryCtrl.list)

  /** POST /api/categorys - Create new category */
  //.post(validate(paramValidation.createCategory), categoryCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createCategory), categoryCtrl.create);

router.route('/:categoryId')
  /** GET /api/categorys/:categoryId - Get category */
  .get(expressJwt({
    secret: config.jwtSecret
  }), categoryCtrl.get)

 /** PUT /api/users/:categoryId - Update category */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateCategory), categoryCtrl.update)
  
  /** DELETE /api/categorys/:categoryId - Delete category */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), categoryCtrl.remove);

export default router;
