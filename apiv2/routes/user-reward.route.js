import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import userRewardCtrl from '../controllers/user-reward.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/userRewards - Get list of userRewards */
  .get(expressJwt({
    secret: config.jwtSecret
  }), userRewardCtrl.list)

  /** POST /api/userRewards - Create new userReward */
  //.post(validate(paramValidation.createUserReward), userRewardCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createUserReward), userRewardCtrl.create);

router.route('/:userRewardId')
  /** GET /api/userRewards/:userRewardId - Get userReward */
  .get(expressJwt({
    secret: config.jwtSecret
  }), userRewardCtrl.get)

 /** PUT /api/users/:userRewardId - Update userReward */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateUserReward), userRewardCtrl.update)
  
  /** DELETE /api/userRewards/:userRewardId - Delete userReward */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), userRewardCtrl.remove);

export default router;
