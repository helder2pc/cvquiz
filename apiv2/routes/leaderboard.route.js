import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import leaderboardCtrl from '../controllers/leaderboard.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/leaderboards - Get list of leaderboards */
  .get(expressJwt({
    secret: config.jwtSecret
  }), leaderboardCtrl.list)

  /** POST /api/leaderboards - Create new leaderboard */
  //.post(validate(paramValidation.createLeaderboard), leaderboardCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createLeaderboard), leaderboardCtrl.create);

router.route('/:leaderboardId')
  /** GET /api/leaderboards/:leaderboardId - Get leaderboard */
  .get(expressJwt({
    secret: config.jwtSecret
  }), leaderboardCtrl.get)

 /** PUT /api/users/:leaderboardId - Update leaderboard */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateLeaderboard), leaderboardCtrl.update)
  
  /** DELETE /api/leaderboards/:leaderboardId - Delete leaderboard */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), leaderboardCtrl.remove);

export default router;
