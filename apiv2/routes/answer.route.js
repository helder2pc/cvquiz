import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import answerCtrl from '../controllers/answer.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/answers - Get list of answers */
  .get(expressJwt({
    secret: config.jwtSecret
  }), answerCtrl.list)

  /** POST /api/answers - Create new answer */
  //.post(validate(paramValidation.createAnswer), answerCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createAnswer), answerCtrl.create);

router.route('/:answerId')
  /** GET /api/answers/:answerId - Get answer */
  .get(expressJwt({
    secret: config.jwtSecret
  }), answerCtrl.get)

 /** PUT /api/users/:answerId - Update answer */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateAnswer), answerCtrl.update)
  
  /** DELETE /api/answers/:answerId - Delete answer */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), answerCtrl.remove);

export default router;
