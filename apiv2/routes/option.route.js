import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import optionCtrl from '../controllers/option.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/options - Get list of options */
  .get(expressJwt({
    secret: config.jwtSecret
  }), optionCtrl.list)

  /** POST /api/options - Create new option */
  //.post(validate(paramValidation.createOption), optionCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createOption), optionCtrl.create);

router.route('/:optionId')
  /** GET /api/options/:optionId - Get option */
  .get(expressJwt({
    secret: config.jwtSecret
  }), optionCtrl.get)

 /** PUT /api/users/:optionId - Update option */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateOption), optionCtrl.update)
  
  /** DELETE /api/options/:optionId - Delete option */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), optionCtrl.remove);

export default router;
