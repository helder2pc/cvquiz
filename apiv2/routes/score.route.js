import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import scoreCtrl from '../controllers/score.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/scores - Get list of scores */
  .get(expressJwt({
    secret: config.jwtSecret
  }), scoreCtrl.list)

  /** POST /api/scores - Create new score */
  //.post(validate(paramValidation.createScore), scoreCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createScore), scoreCtrl.create);

router.route('/:scoreId')
  /** GET /api/scores/:scoreId - Get score */
  .get(expressJwt({
    secret: config.jwtSecret
  }), scoreCtrl.get)

 /** PUT /api/users/:scoreId - Update score */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateScore), scoreCtrl.update)
  
  /** DELETE /api/scores/:scoreId - Delete score */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), scoreCtrl.remove);

export default router;
