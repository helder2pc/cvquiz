import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import userLevelCtrl from '../controllers/user-level.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/userLevels - Get list of userLevels */
  .get(expressJwt({
    secret: config.jwtSecret
  }), userLevelCtrl.list)

  /** POST /api/userLevels - Create new userLevel */
  //.post(validate(paramValidation.createUserLevel), userLevelCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createUserLevel), userLevelCtrl.create);

router.route('/:userLevelId')
  /** GET /api/userLevels/:userLevelId - Get userLevel */
  .get(expressJwt({
    secret: config.jwtSecret
  }), userLevelCtrl.get)

 /** PUT /api/users/:userLevelId - Update userLevel */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateUserLevel), userLevelCtrl.update)
  
  /** DELETE /api/userLevels/:userLevelId - Delete userLevel */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), userLevelCtrl.remove);

export default router;
