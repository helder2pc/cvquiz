import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import postCtrl from '../controllers/post.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/posts - Get list of posts */
  .get(expressJwt({
    secret: config.jwtSecret
  }), postCtrl.list)

  /** POST /api/posts - Create new post */
  //.post(validate(paramValidation.createPost), postCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createPost), postCtrl.create);

router.route('/:postId')
  /** GET /api/posts/:postId - Get post */
  .get(expressJwt({
    secret: config.jwtSecret
  }), postCtrl.get)

 /** PUT /api/users/:postId - Update post */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updatePost), postCtrl.update)
  
  /** DELETE /api/posts/:postId - Delete post */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), postCtrl.remove);

export default router;
