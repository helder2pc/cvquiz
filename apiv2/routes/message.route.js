import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import messageCtrl from '../controllers/message.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/messages - Get list of messages */
  .get(expressJwt({
    secret: config.jwtSecret
  }), messageCtrl.list)

  /** POST /api/messages - Create new message */
  //.post(validate(paramValidation.createMessage), messageCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createMessage), messageCtrl.create);

router.route('/:messageId')
  /** GET /api/messages/:messageId - Get message */
  .get(expressJwt({
    secret: config.jwtSecret
  }), messageCtrl.get)

 /** PUT /api/users/:messageId - Update message */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateMessage), messageCtrl.update)
  
  /** DELETE /api/messages/:messageId - Delete message */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), messageCtrl.remove);

export default router;
