import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import levelRewardCtrl from '../controllers/level-reward.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/levelRewards - Get list of levelRewards */
  .get(expressJwt({
    secret: config.jwtSecret
  }), levelRewardCtrl.list)

  /** POST /api/levelRewards - Create new levelReward */
  //.post(validate(paramValidation.createLevelReward), levelRewardCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createLevelReward), levelRewardCtrl.create);

router.route('/:levelRewardId')
  /** GET /api/levelRewards/:levelRewardId - Get levelReward */
  .get(expressJwt({
    secret: config.jwtSecret
  }), levelRewardCtrl.get)

 /** PUT /api/users/:levelRewardId - Update levelReward */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateLevelReward), levelRewardCtrl.update)
  
  /** DELETE /api/levelRewards/:levelRewardId - Delete levelReward */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), levelRewardCtrl.remove);

export default router;
