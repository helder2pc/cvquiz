import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import chatCtrl from '../controllers/chat.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/chats - Get list of chats */
  .get(expressJwt({
    secret: config.jwtSecret
  }), chatCtrl.list)

  /** POST /api/chats - Create new chat */
  //.post(validate(paramValidation.createChat), chatCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createChat), chatCtrl.create);

router.route('/:chatId')
  /** GET /api/chats/:chatId - Get chat */
  .get(expressJwt({
    secret: config.jwtSecret
  }), chatCtrl.get)

 /** PUT /api/users/:chatId - Update chat */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateChat), chatCtrl.update)
  
  /** DELETE /api/chats/:chatId - Delete chat */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), chatCtrl.remove);

export default router;
