import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import mediaCtrl from '../controllers/media.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/medias - Get list of medias */
  .get(expressJwt({
    secret: config.jwtSecret
  }), mediaCtrl.list)

  /** POST /api/medias - Create new media */
  //.post(validate(paramValidation.createMedia), mediaCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createMedia), mediaCtrl.create);

router.route('/:mediaId')
  /** GET /api/medias/:mediaId - Get media */
  .get(expressJwt({
    secret: config.jwtSecret
  }), mediaCtrl.get)

 /** PUT /api/users/:mediaId - Update media */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateMedia), mediaCtrl.update)
  
  /** DELETE /api/medias/:mediaId - Delete media */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), mediaCtrl.remove);

export default router;
