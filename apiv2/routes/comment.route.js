import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import commentCtrl from '../controllers/comment.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/comments - Get list of comments */
  .get(expressJwt({
    secret: config.jwtSecret
  }), commentCtrl.list)

  /** POST /api/comments - Create new comment */
  //.post(validate(paramValidation.createComment), commentCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createComment), commentCtrl.create);

router.route('/:commentId')
  /** GET /api/comments/:commentId - Get comment */
  .get(expressJwt({
    secret: config.jwtSecret
  }), commentCtrl.get)

 /** PUT /api/users/:commentId - Update comment */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateComment), commentCtrl.update)
  
  /** DELETE /api/comments/:commentId - Delete comment */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), commentCtrl.remove);

export default router;
