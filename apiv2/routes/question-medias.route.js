import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import questionMediaCtrl from '../controllers/question-media.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/question-medias - Get list of questionmedias */
  .get(expressJwt({
    secret: config.jwtSecret
  }), questionMediaCtrl.list)

  /** POST /api/question-medias - Create new questionmedia */
  //.post(validate(paramValidation.createQuestionmedia), questionmediaCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createQuestionMedia), questionMediaCtrl.create);

router.route('/:questionmediaId')
  /** GET /api/questionmedias/:question-mediaId - Get questionmedia */
  .get(expressJwt({
    secret: config.jwtSecret
  }), questionMediaCtrl.get)

 /** PUT /api/users/:question-mediaId - Update questionmedia */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateQuestionMedia), questionMediaCtrl.update)
  
  /** DELETE /api/question-medias/:questionmediaId - Delete questionmedia */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), questionMediaCtrl.remove);

export default router;
