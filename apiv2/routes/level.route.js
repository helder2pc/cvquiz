import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import levelCtrl from '../controllers/level.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/levels - Get list of levels */
  .get(expressJwt({
    secret: config.jwtSecret
  }), levelCtrl.list)

  /** POST /api/levels - Create new level */
  //.post(validate(paramValidation.createLevel), levelCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createLevel), levelCtrl.create);

router.route('/:levelId')
  /** GET /api/levels/:levelId - Get level */
  .get(expressJwt({
    secret: config.jwtSecret
  }), levelCtrl.get)

 /** PUT /api/users/:levelId - Update level */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateLevel), levelCtrl.update)
  
  /** DELETE /api/levels/:levelId - Delete level */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), levelCtrl.remove);

export default router;
