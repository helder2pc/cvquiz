import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import mailCtrl from '../controllers/mail.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/mails - Get list of mails */
  .get(expressJwt({
    secret: config.jwtSecret
  }), mailCtrl.list)

  /** POST /api/mails - Create new mail */
  //.post(validate(paramValidation.createMail), mailCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createMail), mailCtrl.create);

router.route('/:mailId')
  /** GET /api/mails/:mailId - Get mail */
  .get(expressJwt({
    secret: config.jwtSecret
  }), mailCtrl.get)

 /** PUT /api/users/:mailId - Update mail */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateMail), mailCtrl.update)
  
  /** DELETE /api/mails/:mailId - Delete mail */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), mailCtrl.remove);

export default router;
