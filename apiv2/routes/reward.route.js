import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import rewardCtrl from '../controllers/reward.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/rewards - Get list of rewards */
  .get(expressJwt({
    secret: config.jwtSecret
  }), rewardCtrl.list)

  /** POST /api/rewards - Create new reward */
  //.post(validate(paramValidation.createReward), rewardCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createReward), rewardCtrl.create);

router.route('/:rewardId')
  /** GET /api/rewards/:rewardId - Get reward */
  .get(expressJwt({
    secret: config.jwtSecret
  }), rewardCtrl.get)

 /** PUT /api/users/:rewardId - Update reward */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateReward), rewardCtrl.update)
  
  /** DELETE /api/rewards/:rewardId - Delete reward */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), rewardCtrl.remove);

export default router;
