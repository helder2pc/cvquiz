import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import participantCtrl from '../controllers/chat-participant.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/participants - Get list of participants */
  .get(expressJwt({
    secret: config.jwtSecret
  }), participantCtrl.list)

  /** POST /api/participants - Create new participant */
  //.post(validate(paramValidation.createParticipant), participantCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createParticipant), participantCtrl.create);

router.route('/:participantId')
  /** GET /api/participants/:participantId - Get participant */
  .get(expressJwt({
    secret: config.jwtSecret
  }), participantCtrl.get)

 /** PUT /api/users/:participantId - Update participant */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateParticipant), participantCtrl.update)
  
  /** DELETE /api/participants/:participantId - Delete participant */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), participantCtrl.remove);

export default router;
