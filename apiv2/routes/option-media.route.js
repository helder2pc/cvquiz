import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import optionMediasCtrl from '../controllers/option-media.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/optionMediass - Get list of optionMediass */
  .get(expressJwt({
    secret: config.jwtSecret
  }), optionMediasCtrl.list)

  /** POST /api/optionMediass - Create new optionMedias */
  //.post(validate(paramValidation.createOptionMedias), optionMediasCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createOptionMedias), optionMediasCtrl.create);

router.route('/:optionMediasId')
  /** GET /api/optionMediass/:optionMediasId - Get optionMedias */
  .get(expressJwt({
    secret: config.jwtSecret
  }), optionMediasCtrl.get)

 /** PUT /api/users/:optionMediasId - Update optionMedias */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateOptionMedias), optionMediasCtrl.update)
  
  /** DELETE /api/optionMediass/:optionMediasId - Delete optionMedias */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), optionMediasCtrl.remove);

export default router;
