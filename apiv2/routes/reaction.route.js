import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import reactionCtrl from '../controllers/reaction.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/reactions - Get list of reactions */
  .get(expressJwt({
    secret: config.jwtSecret
  }), reactionCtrl.list)

  /** POST /api/reactions - Create new reaction */
  //.post(validate(paramValidation.createReaction), reactionCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createReaction), reactionCtrl.create);

router.route('/:reactionId')
  /** GET /api/reactions/:reactionId - Get reaction */
  .get(expressJwt({
    secret: config.jwtSecret
  }), reactionCtrl.get)

 /** PUT /api/users/:reactionId - Update reaction */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateReaction), reactionCtrl.update)
  
  /** DELETE /api/reactions/:reactionId - Delete reaction */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), reactionCtrl.remove);

export default router;
