import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import recipientCtrl from '../controllers/mail-recipient.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/recipients - Get list of recipients */
  .get(expressJwt({
    secret: config.jwtSecret
  }), recipientCtrl.list)

  /** POST /api/recipients - Create new recipient */
  //.post(validate(paramValidation.createRecipient), recipientCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createRecipient), recipientCtrl.create);

router.route('/:recipientId')
  /** GET /api/recipients/:recipientId - Get recipient */
  .get(expressJwt({
    secret: config.jwtSecret
  }), recipientCtrl.get)

 /** PUT /api/users/:recipientId - Update recipient */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateRecipient), recipientCtrl.update)
  
  /** DELETE /api/recipients/:recipientId - Delete recipient */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), recipientCtrl.remove);

export default router;
