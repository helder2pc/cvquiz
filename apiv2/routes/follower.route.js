import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import followerCtrl from '../controllers/follower.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/followers - Get list of followers */
  .get(expressJwt({
    secret: config.jwtSecret
  }), followerCtrl.list)

  /** POST /api/followers - Create new follower */
  //.post(validate(paramValidation.createFollower), followerCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createFollower), followerCtrl.create);

router.route('/:followerId')
  /** GET /api/followers/:followerId - Get follower */
  .get(expressJwt({
    secret: config.jwtSecret
  }), followerCtrl.get)

  
  /** DELETE /api/followers/:followerId - Delete follower */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), followerCtrl.remove);

export default router;
