import express from 'express';
import expressJwt from 'express-jwt';
import validate from 'express-validation';
import config from '../config/config';
import paramValidation from '../config/param-validation';
import gameQuestionCtrl from '../controllers/game-question.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/gameQuestions - Get list of gameQuestions */
  .get(expressJwt({
    secret: config.jwtSecret
  }), gameQuestionCtrl.list)

  /** POST /api/gameQuestions - Create new gameQuestion */
  //.post(validate(paramValidation.createGameQuestion), gameQuestionCtrl.create);
  .post(expressJwt({
    secret: config.jwtSecret
    }), validate(paramValidation.createGameQuestion), gameQuestionCtrl.create);

router.route('/:gameQuestionId')
  /** GET /api/gameQuestions/:gameQuestionId - Get gameQuestion */
  .get(expressJwt({
    secret: config.jwtSecret
  }), gameQuestionCtrl.get)

 /** PUT /api/users/:gameQuestionId - Update gameQuestion */
 .put(expressJwt({
  secret: config.jwtSecret
}), validate(paramValidation.updateGameQuestion), gameQuestionCtrl.update)
  
  /** DELETE /api/gameQuestions/:gameQuestionId - Delete gameQuestion */
  .delete(expressJwt({
    secret: config.jwtSecret
  }), gameQuestionCtrl.remove);

export default router;
