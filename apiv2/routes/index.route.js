import express from 'express';
import userRoutes from './user.route';
import authRoutes from './auth.route';
import followerRoutes from './follower.route';
import questionRoutes from './question.route';
import optionRoutes from './option.route';
import mediaRoutes from './media.route';
import answerRoutes from './answer.route';
import questionMediasRoutes from './question-medias.route';
import gametypeRoutes from './game-type.route';
import gameRoutes from './game.route';
import categoryRoutes from './category.route';
import gameQuestionRoutes from './game-question.route';
import questionCategoryRoutes from './question-category.route';
import optionMediasRoutes from './option-media.route';
import mailsRoute from './mail.route';
import chatsRoute from './chat.route';
import messagesRoute from './message.route';
import reactionsRoute from './reaction.route';
import mailRecipientsRoute from './mail-recipient.route';
import chatParticipantsRoute from './chat-participant.route';
import postsRoute from './post.route';
import commentsRoute from './comment.route';
import rewardsRoute from './reward.route';
import levelsRoute from './level.route';
import leaderboardsRoute from './leaderboard.route';
import scoresRoute from './score.route';
import userLevelsRoute from './user-level.route'
import userRewardsRoute from './user-reward.route'
import levelRewardsRoute from './level-reward.route'
import leaderboardRewardsRoute from './leaderboard-reward.route'









const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/users', userRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount follower routes at /follower
router.use('/followers', followerRoutes);

// mount question routes at /question
router.use('/questions', questionRoutes);

// mount option routes at /option
router.use('/options', optionRoutes);

// mount media routes at /media
router.use('/medias', mediaRoutes);

// mount answer routes at /answer
router.use('/answer', answerRoutes);

// mount answer routes at /answer
router.use('/question-medias', questionMediasRoutes);

// mount gameType routes at /gametype
router.use('/gametypes', gametypeRoutes);

// mount game routes at /game
router.use('/games', gameRoutes);

// mount category routes at /category
router.use('/categories', categoryRoutes);

// mount gameQuestion routes at /game-question
router.use('/game-questions', gameQuestionRoutes);

// mount questionCategory routes at /question-categories
router.use('/question-categories', questionCategoryRoutes);

// mount optionMedias routes at /option-medias
router.use('/option-medias', optionMediasRoutes);

// mount mail routes at /mails
router.use('/mails', mailsRoute);

// mount chat routes at /chat
router.use('/chats', chatsRoute);

// mount chat messages at /messages
router.use('/messages', messagesRoute);

// mount chat reactions at /reactions
router.use('/reactions', reactionsRoute);

// mount chat mail-recipients at /mail-recipients
router.use('/mail-recipients', mailRecipientsRoute);

// mount chat chat-participants at /chat-participants
router.use('/chat-participants', chatParticipantsRoute);

// mount post at /posts
router.use('/posts', postsRoute);

// mount comment at /comments
router.use('/comments', commentsRoute);

// mount reward at /rewards
router.use('/rewards', rewardsRoute);

// mount level at /levels
router.use('/levels', levelsRoute);

// mount Leaderboard at /Leaderboards
router.use('/leaderboards', leaderboardsRoute);

// mount Score at /score
router.use('/scores', scoresRoute);

// mount userLevel at /user-Levels
router.use('/user-Levels', userLevelsRoute);


// mount userRewards at /user-rewards
router.use('/user-rewards', userRewardsRoute);

// mount levelRewards at /level-rewards
router.use('/level-rewards', levelRewardsRoute);

// mount leaderboard-reward at /leaderboard-rewards
router.use('/leaderboard-rewards', leaderboardRewardsRoute);

export default router;
