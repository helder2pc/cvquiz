export class GameSingle{
    constructor(
        public _id:string,
        public category:string,
        public mode:string,
        public score: number,
        public currectAnswer: number,
        public wrongAnswer: number,
        public created_at:string,
        public user: string
    ){}
}