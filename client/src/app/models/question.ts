
    export class Question{
    constructor(
        public _id: string,
        public question: string,
        public answers: Array<string>,
        public image: string,
        public correct: string,
        public created_at: string
    ){}
}