
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
//import { User } from '../models/user';
import { FeedInfo } from './../models/feedinfo';
import { GLOBAL} from './global';
//import { Http } from '@angular/http'; //Response, ResponseOptions,Headers

@Injectable()
export class FeedInfoService{

    public url: string;
   
    constructor(public _http:HttpClient){
        this.url = GLOBAL.url;
    }

    addFeedInfo(token, feedInfo):Observable<any>{
        let params = JSON.stringify(feedInfo);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                        .set('Authorization',token); 
        return  this._http.post(this.url + 'feedInfo/', params, {'headers':headers});
    }

    getFeedInfo(token, page=1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization',token); 

        return  this._http.get(this.url + 'feedInfos/'+page, {'headers':headers});
    }

    getFeedInfosUser(token, user_id,  page=1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization',token); 

        return  this._http.get(this.url + 'feedinfos-user/'+user_id+'/'+page, {'headers':headers});
    }

    deleteFeedInfo(token, id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization',token); 
        return  this._http.delete(this.url + 'feedinfo/'+id, {'headers':headers});
    }
   


    
}