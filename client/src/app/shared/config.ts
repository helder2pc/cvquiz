export   class  Config {
    public static gameName = 'CVQuiz';
    public static playerName = 'Player1';
    public static categorySelected = 'History';
    public static gameType = 'single';
    public static gameMode = 2;
    public static wrongAnswer = 0;
    public static currectAnswer = 0;
    public static score = 0;
    public static progressbar = 1; 

}
