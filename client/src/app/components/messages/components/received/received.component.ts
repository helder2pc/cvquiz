
import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MessageService } from './../../../../services/message.service';
import { UserService } from './../../../../services/user.service';
import { Message } from './../../../../models/message';
import {Follow} from '../../../../models/follow';
import {FollowService} from '../../../../services/follow.service';
import { GLOBAL} from '../../../../services/global';

@Component({
  selector: 'sended',
  templateUrl: './received.component.html',
  styleUrls: ['./received.component.css'],
  providers:[FollowService, MessageService,UserService],
})
export class ReceivedComponent implements OnInit {
  public title;
  public identity;
  public token;
  public url;
  public page;
  public status;
  public follows;
  public next_page;
  public prev_page;
  public pages;
  public total;
  public messages:Message[];
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _messageService: MessageService,
    private _followService: FollowService,
    private _userService: UserService
  ) {
    this.title ="Mesagens recebidas";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
   }

  ngOnInit() {
   this.actualPage();
  }

  actualPage(){
    this._route.params.subscribe(params=>{
      let page =+ params['page'];
  
      console.log("page " +page)
      this.page=page;

        if(!params['page']){
          page=1;
        }
      if(!page){
        this.page=1;
        page=1;
        console.log(page + " again")
      }
      else{
  
        this.next_page=page+1;
        this.prev_page=page-1;
        
        if( this.prev_page<=0){
          this.prev_page=1;
        }
      }

      //devolver os utilizadores
      this.getMessages(this.token, this.page); ;
    });

    
  }

  getMessages(token,page){
    this._messageService.getMyMessages(token,page).subscribe(
      response =>{
        if(response.messages){
          console.log(response.messages);
          this.messages =response.messages;
          this.total =response.total;
          this.pages = response.pages;
        
        }
      },
      error =>{
        console.log(<any>error);
      }
    )
  }

}
