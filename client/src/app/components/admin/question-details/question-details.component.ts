import { Question } from './../../../models/question';
import { AdminService } from './../admin.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.css'],
  providers:[AdminService]
})
export class QuestionDetailsComponent implements OnInit, OnDestroy {
  private inscription: Subscription;
  public question: Question;
  public category: string;
  public idQuestion: string;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private adminService: AdminService,
    
  ) { }

  ngOnInit() {

  this.getQuestion();

  /*  this.inscription = this.route.data.subscribe(
      (info: {question: Pergunta}) => {
        console.log('Recebendo o obj pergunta do resolver');
        this.question = info.question;
        alert(this.question);
        if (this.question == null) {
          this.question = new Pergunta(0, 'question', ['option', 'option', 'option', 'option']);
          }
      }
    );*/
  }

  getQuestion(){
    this.inscription = this.route.params.subscribe(
     
      (params: any) => {
             let id = params['id'];
             let cat = params['category'];
             this.idQuestion = id;
             this.category = cat;
     
            this.adminService.getQuestion(cat, id).subscribe(
             response =>{
               this.question=response.qa;
               },
               error => {
                 var errorMessage = <any>error;
                 if(errorMessage!=null){
                  console.log(errorMessage);
                 }
               }
            );
           }
         );
  }

  editarQuestion() {
    this.router.navigate(['/admin/question', this.category, this.question._id, 'edit']);
  
    //alert(this.question.id);
  }

  deleteQuestion() {

    
    this.inscription = this.adminService.deleteQuestion(this.idQuestion, 'history').subscribe(
      response =>{
        console.log(response);
        alert("pergunta removida")
        this.router.navigate(['/admin'])
        },
        error => {
          var errorMessage = <any>error;
         
          if(errorMessage!=null){
            console.log(errorMessage);
          }
        }
     );
    //this.adminService.removeQuestion(this.idQuestion, this.category);
    //this.router.navigate(['/admin/question', this.category, +this.question._id]);
    //this.ngOnInit();

  }
  ngOnDestroy() {
    this.inscription.unsubscribe();
  }

 
   
}
