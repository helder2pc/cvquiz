import { AdminComponent } from './../admin.component';
import { QuestionDetailsComponent } from './../question-details/question-details.component';
import { QuestionFormComponent } from './../question-form/question-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
/*
const adminRoutes: Routes = [
     {path: '', component: AdminComponent, children : [
      {path: 'question/new/:category', component: QuestionFormComponent},
      {path: 'question/:category/:id', component: QuestionDetailsComponent},
      {path: 'question/:category/:id/edit', component: QuestionFormComponent},
     

  ]},
];*/

 
const adminRoutes: Routes = [
  {path: '', component: AdminComponent, children : [
   { path: '', redirectTo: '/timeline', pathMatch: 'full' },
   {path: 'admin/question/new/:category', component: QuestionFormComponent},
   {path: 'admin/question/:category/:id', component: QuestionDetailsComponent},
   {path: 'admin/question/:category/:id/edit', component: QuestionFormComponent},

]},
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
