import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluateQuestionComponent } from './evaluate-question.component';

describe('EvaluateQuestionComponent', () => {
  let component: EvaluateQuestionComponent;
  let fixture: ComponentFixture<EvaluateQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluateQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluateQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
