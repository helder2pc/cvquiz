import { Question } from './../../models/question';
import { QAService } from '../../services/qa.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'evaluate-question',
  templateUrl: './evaluate-question.component.html',
  styleUrls: ['./evaluate-question.component.css'],
  providers:[QAService]
})
export class EvaluateQuestionComponent implements OnInit {

  private inscription: Subscription;
  public question: Question;
  public questions: Question;
  public category: string;
  public idQuestion: number;
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _qAService:  QAService,
  ) { }

  ngOnInit() { 
    this.getAllQuestions();
    this.idQuestion = 0;
  }

  getAllQuestions() {
    //this._adminService.getAllQuestion.("history",1).subscribe()
    this.inscription = this._qAService.getAllQuestion(this.category, 1).subscribe(
      response => { 
        this.questions = response.qa;
        this.question = this.questions[this.idQuestion]
       
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        alert(567)
        if (errorMessage != null) {
          //this.status ="error";
        }
      }
    );
  }
  ;

  editarQuestion() {
   // this.router.navigate(['/admin/question', this.category, this.question._id, 'edit']);
  
    //alert(this.question.id);
  }

  deleteQuestion() {

    
    /*this.inscription = this.adminService.deleteQuestion(this.idQuestion, 'history').subscribe(
      response =>{
        console.log(response);
        alert("pergunta removida")
        this.router.navigate(['/admin'])
        },
        error => {
          var errorMessage = <any>error;
         
          if(errorMessage!=null){
            console.log(errorMessage);
          }
        }
     );*/
    //this.adminService.removeQuestion(this.idQuestion, this.category);
    //this.router.navigate(['/admin/question', this.category, +this.question._id]);
    //this.ngOnInit();

  }
  ngOnDestroy() {
    this.inscription.unsubscribe(); 
  }

  nextQuestion(){
    this.idQuestion++;
    this.question = this.questions[this.idQuestion];
  }
 
   
//}


}
