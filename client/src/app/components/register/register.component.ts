import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';
import {trigger, state, style, transition, animate} from '@angular/animations'

import {User} from '../../models/user';
import {UserService} from '../../services/user.service';


@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService],
  animations:[
    trigger('animation-register',[
      state('create',style({
        opacity: 1
      })),
  
      transition('void => create',[
        style({
          opacity:0,
           transform: 'translate(50px,0)'
        }),
        animate(
          //duracao, delay e acelaracao
          '500ms 0s ease-in-out' 
        )
      ])
    ])
  ]
  
   
})

export class RegisterComponent implements OnInit {
    public user: User;
    public title:string;
    public status:string;
    public stateRegister: string = 'create';
  
    constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) { 
    this.title = 'Registar';
    this.user = new User("","","","","","","ROLE_USER","");
  }

  ngOnInit() {
  }

  onSubmit(form){
    //console.log(this.user);
    this._userService.register(this.user).subscribe(
      response =>{
        if(response.user && response.user._id){
          this.status='success';
          form.reset();
          //console.log(response.user);
        }
       else{
        this.status='error';
       }
      },
      error =>{
        console.log(<any> error);
      }
    );
  }
  resolved(captchaResponse: string){
    console.log('Resolved '+ captchaResponse )
  }

}
