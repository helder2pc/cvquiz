import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';

import {User} from '../../models/user';
import {Follow} from '../../models/follow';
import {UserService} from '../../services/user.service';
import {FollowService} from '../../services/follow.service';
import { Observable } from 'rxjs/Observable';
import { GLOBAL} from '../../services/global';


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserService,FollowService],
})
export class UsersComponent implements OnInit {

  public title:string;
   // private user: User;
    public status: string;
    public identity;
    public token;
    public url;
    public page;
    public next_page;
    public prev_page;
    public pages;
    public total;
    public users:User[];
    public follows;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService
  ) {
    this.title ="Pessoas"; 
   // this.user = new User("","","","","","","ROLE_USER","");
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.page=1
   }

  ngOnInit() {
    console.log("Componente users foi carregado");
    let getf = this.actualPage();
 
    setTimeout(function(){ getf }, 3000);
    /*console.log(" this.next_page "+  this.next_page);
    console.log(" this.prev_page "+  this.prev_page);
    console.log(" thispage "+  this.page);*/

  }

  actualPage(){
    this._route.params.subscribe(params=>{
      let page =+ params['page'];
      console.log("page " +page)
      this.page=page;

        if(!params['page']){
          page=1;
        }
      if(!page){
        this.page=1;
        page=1;
        console.log(page + " again")
      }
      else{
  
        this.next_page=page+1;
        this.prev_page=page-1;
        
        if( this.prev_page<=0){
          this.prev_page=1;
        }
      }

      //devolver os utilizadores
      this.getUsers(page);
    });
  }

  getUsers(page){
    this._userService.getUsers(page).subscribe(
      response => {
          if(!response.users){
            this.status ="error";
          }
          else{
            console.log(response);
            this.total =response.total;
            this.users=response.users;
            this.pages = response.pages;
            this.follows = response.user_following;
            console.log("pppp " +this.follows.length);

            if(page>this.pages){
              this._router.navigate(['/users/',1]);
            }

          }
      },

      error =>{
          var errorMessage = <any>error;
          console.log(errorMessage);
          if(errorMessage!=null){
            this.status ="error";
          }
      }
    );

  }

  public followUserOver;


  mouseEnter(user_id){
    this.followUserOver = user_id;
  }
  mouseLeave(user_id){
    this.followUserOver = 0;
  }

  followUser(followed){
    var follow= new Follow('',this.identity._id,followed);

    this._followService.addFollow(this.token, follow).subscribe(
      response =>{
        if(!response.follow){
          this.status = 'error';
        }
        else{
          this.status = "success";

          this.follows.push(followed);
        }
    },
    error =>{
      var errorMessage = <any>error;
      console.log(errorMessage);
      if(errorMessage!=null){
        this.status ="error";
      }
    }
    )};

    unFollowUser(followed){
      this._followService.deleteFollow(this.token, followed).subscribe(
        response =>{
         
         var search =this.follows.indexOf(followed);

         if(search !=-1){
           this.follows.splice(search,1);
         }
      },
      error =>{
        var errorMessage = <any>error;
        console.log(errorMessage);
        if(errorMessage!=null){
          this.status ="error";
        }
      }
      )
    
    }
  
    search(event){
        var text:string =event.target.value.trim();
        if(text=="") return;
        console.log(event.target.value)
        this._userService.searchUsers(text).subscribe(
          response => {
              if(!response.users){
                this.status ="error";
              }
              else{
                console.log(response);
               this.total =response.total;
                this.users=response.users;
                this.pages = response.pages;
                this.follows = response.user_following;
                console.log("pppp " +this.follows.length);
    
                /*if(page>this.pages){
                  this._router.navigate(['/users/',1]);
                }*/
    
              }
    })
  }

  onKey(event){
console.log(event.target.value)
  }

}
