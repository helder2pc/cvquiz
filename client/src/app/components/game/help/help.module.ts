import { HelpComponent } from './help.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    HelpComponent
  ]
})
export class HelpModule { }
