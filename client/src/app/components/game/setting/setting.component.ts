import { Config } from './../../../shared/config';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

    private gameMode: number;
    private progressbarCheckbox: number;
  constructor() {
    this.gameMode = Config.gameMode;
    this.progressbarCheckbox = Config.progressbar;
  }


  ngOnInit() {
  }

  classicBtn(btn) {

    console.log(btn.value);
    Config.gameMode = this.gameMode = btn.value;
    //Config.gameMode = btn.value;
  }

  clickProgressBar( btn ) {
    Config.progressbar = this.progressbarCheckbox = btn.value;
  }
}
