import { SettingComponent } from './setting.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SettingComponent
  ]
})
export class SettingModule { }
