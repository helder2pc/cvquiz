import { RankingComponent } from './ranking.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    RankingComponent
  ]
})
export class RankingModule { }
