import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {

  constructor() { }

  getCategory() {
    return [
      {id: 1, name: 'Historia', value: 'history'},
      {id: 2, name: 'Geografia' , value: 'geography'},
      {id: 3, name: 'Desporto' , value: 'sport'},
      {id: 4, name: 'Música' , value: 'art'},
      //{id: 5, name: 'Entretenimento'},
      {id: 5, name: 'Tour', value: 'tour'},
      {id: 6, name: 'Aleatorio', value: 'random'},
    ];
  }

}
