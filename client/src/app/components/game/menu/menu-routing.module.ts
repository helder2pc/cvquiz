import { CategoryComponent } from './category/category.component';
import { MenuComponent } from './menu.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routesMenu: Routes = [
  { path: '', component: MenuComponent },
  { path: 'menu/category', component: CategoryComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routesMenu)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
