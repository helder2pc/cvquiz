import { MenuService } from './menu.service';
import { CategoryComponent } from './category/category.component';
import { MenuComponent } from './menu.component';
import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MenuRoutingModule } from './menu-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MenuRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
  ],
  declarations: [
    MenuComponent,
    CategoryComponent,
  ],
  providers: [MenuService],
 exports: [
    MenuComponent,
    CategoryComponent
  ],
})
export class MenuModule { }
