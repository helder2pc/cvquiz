import { TestBed, async, inject } from '@angular/core/testing';

import { GameAreaDesactivateGuard } from './game-area-desactivate.guard';

describe('GameAreaDesactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameAreaDesactivateGuard]
    });
  });

  it('should ...', inject([GameAreaDesactivateGuard], (guard: GameAreaDesactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
