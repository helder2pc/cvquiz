import { GLOBAL } from './../../../services/global';
import { Subscription } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { QuestionsService } from './questions.service';
import { Config } from './../../../shared/config';
import { Pergunta } from './../../../shared/pergunta';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Question } from '../../../models/question';

@Component({
  selector: 'app-game-area',
  templateUrl: './game-area.component.html',
  styleUrls: ['./game-area.component.css'],
  providers:[QuestionsService]
})
export class GameAreaComponent implements OnInit {

  public idQuestion: number;
  public question: Question;
  public listQuestion: Question[];
  public answers: string[];
  public currectAnswer: string;
  public score: number;
  public time: number;
  public time2: number;
  public timeShow: number;
  public interval;
  public gameMode: number;
  public gameCategory: string;
  public countQuestion: number;
  private wrongAnswer: number;
  private ids: number[];
  private inscription: Subscription;
  public index;
  public page;
  public pages;
  public url;


  constructor(
    private _questionsService: QuestionsService,
    private router: Router,
  ) {
    this.idQuestion = 1;
    this.score = 0;
    this.wrongAnswer = 0;
    this.countQuestion = 0;
    this.gameCategory = Config.categorySelected;
    this.gameMode = Config.gameMode;
    this.answers = [];
    this.index=0;
    this.page=1;
    this.url = GLOBAL.url;
  }


  ngOnInit() {
    this.getLisQuestion();
/*
    this.shuffleIdAnswers();
   this.onInitQuestiAnswer();

    //alert(this.currectAnswer)
    this.shuffleAnswers();


    if (Config.gameMode == 2) {
        this.time = 20;
        this.interval = setInterval(() => {
        this.time -= 0.1;
        this.timeShow = Math.round((this.time / 20) * 100);
        this.time2 =  Math.round(this.time);
        if (this.time <= 0) {
          //this.nextQuestion();
          this.gameFinished();
        }
      }, 100);
    }*/
  }

  initializeQuestion(){
    //this.shuffleArray(this.listQuestion);
    //this.shuffleIdAnswers();
    this.onInitQuestiAnswer();
    this.shuffleAnswers();


    if (Config.gameMode == 2) {
        this.time = 20;
        this.interval = setInterval(() => {
        this.time -= 0.1;
        this.timeShow = Math.round((this.time / 20) * 100);
        this.time2 =  Math.round(this.time);
        if (this.time <= 0) {
          //this.nextQuestion();
          this.gameFinished();
        }
      }, 100);
    }
  }

  getQuestion() {
    if(this.index==this.listQuestion.length)
      this.index--;
    this.question = this.listQuestion[this.index++];
  }

  getCorrectAnswer() {
    return this.currectAnswer;
  }

  checkAnswer(btn: HTMLButtonElement) {

    btn.classList.remove('btn-info');
    this.countQuestion++;
    let text: string = btn.getAttribute('textValue');
    if (text == this.getCorrectAnswer()) {
      btn.classList.add('btn-success');
      this.score += 10;
     

     
    } else {

      let  btns: HTMLCollection = btn.parentElement.children;
      for(let b=0; b< btns.length; b++){
        console.log(btns[b].getAttribute('textValue'))
        if(btns[b].getAttribute('textValue') == this.getCorrectAnswer())
        {
          btns[b].classList.remove('btn-info'); 
          btns[b].classList.add('btn-success');
        }
      }

      this.wrongAnswer++;
      btn.classList.add('btn-danger');

    }
    this.idQuestion++;
    window.clearInterval(this.interval);
    setTimeout(function () {
     
      btn.classList.remove('btn-danger');
      btn.classList.remove('btn-success');
      btn.classList.add('btn-info');
      if (this.wrongAnswer > 0 && (this.gameMode == 1 || this.gameMode == 2  )) {
        this.gameFinished ();
        return;
      }
      this.initializeQuestion();
    }.bind(this), 1000);
    
    if (this.gameMode == 3 && this.countQuestion == 10  ) {
      this.gameFinished ();
    
    }
  }

  nextQuestion() {
    window.clearInterval(this.interval);
    this.idQuestion++;
    this.countQuestion++;
    this.initializeQuestion();
  }


  shuffleAnswers() {
    let array = this.answers;
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      let temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    this.answers = array;
    //return array;
  }

  convertMode(mode: number) {
    if (mode == 1) {
      return 'Clássico';
    }
    else if (mode == 2) {
      return "Contra-relógio";
    }
    else if (mode == 3) {
      return "10x";
    }

    return "20x";
  }

  gameFinished () {
    Config.wrongAnswer = this.wrongAnswer;
    Config.currectAnswer = this.countQuestion - this.wrongAnswer;
    Config.score = this.score;
    this.router.navigate(['/sumare']);

    this.idQuestion = 1;
    this.score = 0;
    this.wrongAnswer = 0;
    this.countQuestion = 0;
  }

  getLisQuestion(){
    this.inscription = this._questionsService.getAllQuestion(this.gameCategory).subscribe(
      response =>{
        this.listQuestion = response.qa;
        this.page=response.page;
        this.pages=response.pages;
        this.shuffleArray(this.listQuestion);
        this.initializeQuestion();

        },
        error => {
          var errorMessage = <any>error;
          if(errorMessage!=null){
            //this.status ="error";
          }
        }
     );

  }

  onInitQuestiAnswer() {

    //this.question=this.listQuestion[this.index++];
    this.getQuestion();
    this.currectAnswer = this.question.answers[0];
    this.answers[0] = this.question.answers[0];
    this.answers[1] = this.question.answers[1];
    this.answers[2] = this.question.answers[2];
    this.answers[3] = this.question.answers[3];
}

/*shuffleIdAnswers() {
  let array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  this.ids = array;
  //return array;
}*/
  hideTimer() {
   // console.log(" Config.progressbar "+  Config.progressbar);
    if (this.gameMode == 2 && Config.progressbar == 0) {
      return false;
    }
    return true;
  }

  hideprogressbar() {
    if (this.gameMode == 2 && Config.progressbar == 1) {
      return false;
    }
    return true;
  }

  OnDestroy() {
    window.clearInterval(this.interval);
    this.inscription.unsubscribe();
  }


  shuffleArray(array){
  
    let m = array.length, t, i;
  
    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);
  
      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
  
    return array;
  } 
}
