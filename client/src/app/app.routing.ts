import { CategoryComponent } from './components/game/menu/category/category.component';
import { FeedinfoComponent } from './components/feedinfo/feedinfo.component';
import { EvaluateQuestionComponent } from './components/evaluate-question/evaluate-question.component';
import { PageNotFoundComponent } from './components/game/page-not-found/page-not-found.component';
import { MenuComponent } from './components/game/menu/menu.component';
import { ResultPanelComponent } from './components/game/result-panel/result-panel.component';
import { HelpComponent } from './components/game/help/help.component';
import { SettingComponent } from './components/game/setting/setting.component';
import { RankingComponent } from './components/game/ranking/ranking.component';
import { GameAreaDesactivateGuard } from './components/game/game-area/game-area-desactivate.guard';
import { GameAreaComponent } from './components/game/game-area/game-area.component';
import { AdminComponent } from './components/admin/admin.component';
import { FollowedComponent } from './components/followed/followed.component';
import { FollowingComponent } from './components/following/following.component';
import { ModuleWithProviders }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Componets
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UserGuard } from './services/user.guard';
import { UserQuestionComponent } from './components/user-question/user-question.component';
import { InfoComponent } from './components/info/info.component';


const appRoutes: Routes = [
   
    { path: 'home', component: HomeComponent,canActivate:[UserGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'meus-dados', component: UserEditComponent,canActivate:[UserGuard] },
    { path: 'users', component: UsersComponent ,canActivate:[UserGuard]},
    { path: 'users/:page', component: UsersComponent ,canActivate:[UserGuard]},
    { path: 'timeline', component: TimelineComponent ,canActivate:[UserGuard]},
    { path: 'perfil/:id', component: ProfileComponent ,canActivate:[UserGuard]},
    { path: 'seguindo/:id/:page', component: FollowingComponent ,canActivate:[UserGuard] },
    { path: 'seguidores/:id/:page', component: FollowedComponent ,canActivate:[UserGuard]},
    { path: 'game-area', component: GameAreaComponent, canDeactivate: [GameAreaDesactivateGuard] },
   // { path: 'admin', loadChildren: 'app/components/admin/module/admin.module#AdminModule'},
   { path: 'admin', component: AdminComponent, canActivate:[UserGuard] }, 
    { path: 'ranking', component: RankingComponent ,canActivate:[UserGuard]},
    { path: 'setting', component: SettingComponent ,canActivate:[UserGuard]},
    { path: 'help', component: HelpComponent ,canActivate:[UserGuard]},
    { path: 'info', component: InfoComponent ,canActivate:[UserGuard]},
    { path: 'pagenotfound', component: PageNotFoundComponent ,canActivate:[UserGuard]},
    { path: 'sumare', component: ResultPanelComponent ,canActivate:[UserGuard]},  
    { path: 'category', component: CategoryComponent ,canActivate:[UserGuard]},
    { path: 'user-question', component: UserQuestionComponent ,canActivate:[UserGuard]},
    { path: 'feedinfo', component: FeedinfoComponent ,canActivate:[UserGuard]},
    { path: 'user-question/edit/:id', component: EvaluateQuestionComponent ,canActivate:[UserGuard]},

   // { path: 'menu', loadChildren: 'app/components/game/menu/menu.module#MenuModule', },
    { path: '', redirectTo: '/timeline', pathMatch: 'full' },
  //  { path: 'admin', component: AdminComponent },
    { path: '**', component: PageNotFoundComponent },
    //{ path: 'heroes', component: HeroesComponent }
  ];

  export const appRoutingProviders: any[]=[];
  export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);