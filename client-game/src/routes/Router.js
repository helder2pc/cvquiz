import React from 'react';
import '../App.css';
// import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import {  Route,  Switch } from "react-router-dom";
import history from './history'
import {ConnectedRouter} from 'connected-react-router';
import { connect } from 'react-redux';

function CVQRouter(props) {
  // const {isAuthenticated} = props;
  
  return (
    <ConnectedRouter history={history}> 
      <div className="App"> 
      {/* { isAuthenticated && <Toolbar></Toolbar>} */}
      {/* <Toolbar></Toolbar>
        */}
        <Switch>

          <Route exact path="/" component={Login} />
          <Route path="/home" component={Home} /> 
         
          
        </Switch>
      </div>
  </ConnectedRouter>
  );
}

// export default CVQRouter;
const mapStateToProps = state => ({
  isAuthenticated: state.Auth.isAuthenticated
})

export default connect(
  mapStateToProps,
  null
)(CVQRouter);


const Home =()=>(<div>home</div>);
const Login =()=>(<div>login</div>);
