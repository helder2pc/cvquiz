export const IS_AUTHENTICATED = 'is_Authenticated';
export const LOGOUT = 'logout';
export const START_GAME = 'start_game';
export const NEXT_QUESTION = 'next_question';
