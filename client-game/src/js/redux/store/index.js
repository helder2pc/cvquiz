import { createStore, applyMiddleware, compose } from 'redux'
//import logger from 'redux-logger'
// import {connectRouter, routerMiddleware} from 'connected-react-router'
import {routerMiddleware} from 'connected-react-router'
import thunk from 'redux-thunk';

import reducer from '../reducers'
import history from '../../../routes/history'
// import { routerMiddleware } from 'connected-react-router';

const initialState = {}
const enhancers = []
const middleware = [
  thunk, routerMiddleware(history)
]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)


const store = createStore(
  reducer,
  initialState,
  composedEnhancers,
)

export default store