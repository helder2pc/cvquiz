
import {
  IS_AUTHENTICATED, LOGOUT
} from '../constants';

export default function reducer(state={
    data: [],
    navigation: [],
    loading: true,
    loaded: false,
    login_user: "",
    login_password: "",
    error: null,
    isAuthenticated:false,
  }, action) {

    switch (action.type) {

      /*case LOCATION_CHANGE: {
        console.log('location change')
        return {...state, data: [] }
      }*/
      

      case "GETTING_PAGE": {
          return {
            ...state, 
            loading: true,
            loaded: false
             }
      }

      case IS_AUTHENTICATED: {
        console.log('IS_AUTHENTICATED')
        return {
          ...state, 
          isAuthenticated: true,
           }
    }

    case LOGOUT: {
      return {
        ...state, 
        isAuthenticated: false,
         }
  }
      case "GOT_PAGE": {
        
        return {
          ...state,
          loading: false,
          loaded: true,
          siteReady: true,
          data: action.payload,
        }
      }
      default: {
        return {
          ...state
        };
      }
    }
}