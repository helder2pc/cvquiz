import { combineReducers } from "redux"

import Auth from "./Auth";
import Game from "./Game";
import history from '../../../routes/history';
// export default combineReducers({
// 	Auth,
// });


import { connectRouter } from 'connected-react-router'
export default combineReducers({
   Auth,Game,
  router: connectRouter(history),
});