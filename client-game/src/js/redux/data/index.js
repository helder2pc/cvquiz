
// import * as geography from './';
// export {AuthActions }
import geographyData from './geography.json';
import historyData from './history.json';
import artData from './art.json';
import sportData from './sport.json';
export {geographyData,historyData,artData, sportData }
