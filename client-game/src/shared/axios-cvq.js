import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://cvquiz.herokuapp.com/api/'
});

export default instance;