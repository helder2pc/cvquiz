'use strict'

var express = require('express');
var ArtController = require('../controllers/qa/art');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/qa-art', ArtController.saveQA);
api.delete('/qa-art/:id',  ArtController.deleteQA);
api.get('/qas-art/:page?',  ArtController.getQAs);
api.get('/qa-art/:id?',  ArtController.getQA);
api.put('/qa-art/:id',  ArtController.updateQA);



module.exports=api;