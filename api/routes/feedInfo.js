'use strict'

var express = require('express');
var FeedInfoController = require('../controllers/feedInfo');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

// dividir o ficheiros em partes
var multipart = require('connect-multiparty'); 
var md_upload = multipart({uploadDir:'./uploads/feedInfos'});

//api.get('/test',FeedInfoController.test);
//api.get('/user/:id',md_auth.ensureAuth,UserController.getUser);
api.post('/feedinfo',md_auth.ensureAuth,FeedInfoController.saveFeedInfo);
api.get('/feedinfos/:page?',md_auth.ensureAuth,FeedInfoController.getFeedInfos);
api.get('/feedinfos-user/:id/:page?',md_auth.ensureAuth,FeedInfoController.getFeedInfoUser);
api.get('/feedinfo/:id',md_auth.ensureAuth,FeedInfoController.getFeedInfo);
api.delete('/feedinfo/:id',md_auth.ensureAuth,FeedInfoController.deleteFeedInfo);
api.post('/up-feedinfo/:id',[md_auth.ensureAuth,md_upload],FeedInfoController.uploadImage);
api.get('/get-img-feedinfo/:imageFile',FeedInfoController.getImageFile);
module.exports=api;