'use strict'

var express = require('express');
var SportController = require('../controllers/qa/sport');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/qa-sport', SportController.saveQA);
api.delete('/qa-sport/:id',  SportController.deleteQA);
api.get('/qas-sport/:page?',  SportController.getQAs);
api.get('/qa-sport/:id?',  SportController.getQA);
api.put('/qa-sport/:id',  SportController.updateQA);



module.exports=api;