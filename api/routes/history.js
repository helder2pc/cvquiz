'use strict'

var express = require('express');
var HistoryController = require('../controllers/qa/history');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/qa-history', HistoryController.saveQA);
api.delete('/qa-history/:id',  HistoryController.deleteQA);
api.get('/qas-history/:page?',  HistoryController.getQAs);
api.get('/qa-history/:id?',  HistoryController.getQA);
api.put('/qa-history/:id',  HistoryController.updateQA);



module.exports=api;