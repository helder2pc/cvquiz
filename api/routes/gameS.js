'use strict'

var express = require('express');
var GameSController = require('../controllers/gameS');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');


api.post('/game-single',md_auth.ensureAuth,GameSController.saveGameS);
api.get('/game-single-list/:category/:page?',md_auth.ensureAuth,GameSController.getGameSList);
api.get('/game-single-user/:id/:category/:page?',md_auth.ensureAuth,GameSController.getGameSUser);
api.get('/game-single/:id',md_auth.ensureAuth,GameSController.getGameS);
api.delete('/game-single/:id',md_auth.ensureAuth,GameSController.deleteGameS);

module.exports=api;