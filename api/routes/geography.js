'use strict'

var express = require('express');
var GeographyController = require('../controllers/qa/geography');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/qa-geography', GeographyController.saveQA);
api.delete('/qa-geography/:id',  GeographyController.deleteQA);
api.get('/qas-geography/:page?',  GeographyController.getQAs);
api.get('/qa-geography/:id?',  GeographyController.getQA);
api.put('/qa-geography/:id',  GeographyController.updateQA);



module.exports=api;