'use strict'

var express = require('express');
var QaImageController = require('../controllers/qa/quizImage');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

// dividir o ficheiros em partes
var multipart = require('connect-multiparty'); 
var md_upload = multipart({uploadDir:'./uploads/qa'});

api.post('/qa-image', QaImageController.saveQA);
api.delete('/qa-image/:id',  QaImageController.deleteQA);
api.get('/qas-image/:page?',  QaImageController.getQAs);
api.get('/qa-image/:id?',  QaImageController.getQA);
api.put('/qa-image/:id',  QaImageController.updateQA);
api.post('/upload-image-qa/:id',[md_upload], QaImageController.uploadImage);
api.get('/get-image-qa/:imageFile', QaImageController.getImageFile);



module.exports=api;