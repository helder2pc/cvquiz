'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionMediasSchema = Schema({

    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    media_id: {
        type: Schema.ObjectId,
        ref: 'Media'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('QuestionMedias', QuestionMediasSchema);