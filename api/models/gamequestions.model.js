'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameQuestionsSchema = Schema({

    game_id: {
        type: Schema.ObjectId,
        ref: 'Game'
    },
    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('GameQuestions', GameQuestionsSchema);