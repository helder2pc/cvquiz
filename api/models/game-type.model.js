'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameTypeSchema = Schema({

    name: {
        type: String,
        required: true,
        trim: true
    },
    min_players: {
        type: Number,
        min: 1
    },
    max_players: {
        type: Number,
        min: 1
    },
    max_duration: { // time in seconds
        type: Number,
    },
    time_per_question: { // time in seconds
        type: Number,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('GameType', GameTypeSchema);