'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OptionMediasSchema = Schema({

    option_id: {
        type: Schema.ObjectId,
        ref: 'Option'
    },
    media_id: {
        type: Schema.ObjectId,
        ref: 'Media'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('OptionMedias', OptionMediasSchema);