'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChatSchema = Schema({

    name: {
        type: String,
        require: true,
    },
    messages: [{
        type: Schema.ObjectId,
        ref: 'Message'
    }],

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Chat', ChatSchema);