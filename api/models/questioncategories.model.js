'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionCategoriesSchema = Schema({

    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    category_id: {
        type: Schema.ObjectId,
        ref: 'Category'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('QuestionCategories', QuestionCategoriesSchema);