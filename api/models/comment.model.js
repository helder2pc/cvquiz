'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = Schema({

    content: {
        type: String,
    },
    post: {
        type: Schema.ObjectId,
        ref: 'Post'
    },
    reply_to: {
        type: Schema.ObjectId,
        ref: 'Comment'
    },
    sender_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Comment', CommentSchema);