'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedInfoSChema =Schema({
    text:String,
    file:String,
    type: String,
    url: String, 
    position: String,
    created_at:String,
    user: {type:Schema.ObjectId,ref: 'User'}
});

module.exports = mongoose.model('FeedInfo',FeedInfoSChema);