'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReactionSChema = Schema({
    type: {
        type: String,
        enum: ['like', 'dislike'],
        required: true
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    observable_id: {
        type: Schema.ObjectId,
        required: true
    },
    observable_type: {
        type: String,
        enum: ['Post', 'Comment', 'Message'],
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Reaction', ReactionSchema);