'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionSchema = Schema({

    question: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    difficulty: {
        type: String,
        enum: ['very_easy.', 'easy', 'medium', 'difficult', 'very_difficult'],
        required: true,
    },
    type: {
        type: String,
        enum: ['mcq', 'mcq_ma', 'true_false', 'fill_in_blank', 'match', 'order', 'short_answer', 'hotspot'],
        required: true,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Question', QuestionSchema);