'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameSchema = Schema({
    gametype_id: {
        type: Schema.Types.ObjectId,
        ref: 'GameType',
        required: true
    },
    state: {
        type: String,
        enum: ['created', 'ready', 'running', 'finished'],
        default: 'created'
    },
    start_time: {
        type: Date
    },
    end_time: {
        type: Date
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Game', GameSchema);