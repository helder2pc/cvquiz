'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameSSChema =Schema({
    category: {
        type: String,
        required: true,
        trim: true
    },
    mode: {
        type: String,
        required: true,
        trim: true
    },
    score: {
        type: Number,
        required: true,
 
    },
    currect_answer: {
        type: Number,
        required: true,
    
    },
    wrong_answer: {
        type: Number,
        required: true,
    },
  
    
    end: {
        type: Boolean,
    },
    created_at: Date,
    user: {type:Schema.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('GameSingle', GameSSChema);