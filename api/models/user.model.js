'use strict'

var mongoose = require('mongoose');
var crypto = require('crypto');

var Schema = mongoose.Schema;

var UserSchema = Schema({

    first_name: {
        type: String,
        required: true,
        trim: true
    },
    last_name: {
        type: String,
        required: true,
        trim: true
    },
    username: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, 'can\'t be blank'],
        match: [/^[a-zA-Z0-9_]+$/, 'is invalid'],
        index: true,
        unique: [true, 'is already taken.']
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, 'can\'t be blank'],
        match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'is invalid'],
        index: true,
        unique: [true, 'is already taken.']
    },
    hash: String,
    salt: String,
    role: [{
        type: String
    }],
    media: {
        type: Schema.ObjectId,
        ref: 'Media'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

UserSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

module.exports = mongoose.model('User', UserSchema);