'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnswerSchema = Schema({

    time_spent: {
        type: Number,
        default: 0,
    },
    attempts: {
        type: Number,
        default: 0,
    },
    game_id: {
        type: Schema.ObjectId,
        ref: 'Game',
        required: true
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    question_id: {
        type: Schema.ObjectId,
        ref: 'Question',
        required: true
    },
    option_id: {
        type: Schema.ObjectId,
        ref: 'Option',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Answer', AnswerSchema);