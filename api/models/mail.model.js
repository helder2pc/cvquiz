'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MailSchema = Schema({

    subject: {
        type: String,
        require: true,
    },
    body: {
        type: String,
        require: true,
    },
    sender_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Mail', MailSchema);