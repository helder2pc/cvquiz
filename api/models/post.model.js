'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = Schema({

    title: {
        type: String,
        require: true
    },
    summary: {
        type: String,
        require: true
    },
    content: {
        type: String
    },
    category_id: {
        type: Schema.ObjectId,
        ref: 'Category'
    },
    publisher_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Post', PostSchema);