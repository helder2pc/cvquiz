'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MediaSchema = Schema({
    type: {
        type: String,
        enum: ['video', 'image', 'binary', 'text'],
        required: true
    },
    name: {
        type: String,
        required: true
    },
    file: {
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Media', MediaSchema);