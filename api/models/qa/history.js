'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var HistorySchema =Schema({
	question: String,
	answers: Array,
	//category: String,
    //island : String,
	correct: String, 
	created_at: String
});

module.exports = mongoose.model('History', HistorySchema);