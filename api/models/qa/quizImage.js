'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuizImageSchema =Schema({
	question: String,
	answers: Array,
	image: String,
	//category: String,
    //island : String,
	correct: String, 
	created_at: String,
	
});

module.exports = mongoose.model('QuizImage', QuizImageSchema);