'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChatParticipantsSchema = Schema({

    chat_id: {
        type: Schema.ObjectId,
        ref: 'Chat'
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('ChatParticipants', ChatParticipantsSchema);