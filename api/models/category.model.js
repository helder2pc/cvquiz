'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = Schema({

    name: {
        type: String,
        trim: true
    },
    description: {
        type: String,
    },
    sub_categories: [{
        type: Schema.ObjectId,
        ref: 'Category'
    }],
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Category', CategorySchema);