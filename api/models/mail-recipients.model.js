'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MailRecipientsSchema = Schema({

    mail_id: {
        type: Schema.ObjectId,
        ref: 'Mail'
    },
    user_id: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('MailRecipients', MailRecipientsSchema);