'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OptionSchema = Schema({

    answer: {
        type: String,
        required: true
    },
    question_id: {
        type: Schema.ObjectId,
        ref: 'Question'
    },
    feedback: {
        type: String
    },
    order: {
        type: Number,
        default: 0
    },
    correct: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Option', OptionSchema);