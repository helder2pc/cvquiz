'use strict'

var jwt = require ('jwt-simple');
var moment = require('moment');
var secret = 'chave_secreta_CVQuiz';

exports.ensureAuth = function(req,res,next){
    
    if(!req.headers.authorization){
        return res.status(403).send({message:'o pedido nao tem autenticacao no cabecalho'});
    }
    var token = req.headers.authorization.replace(/['"]+/g,'');

    try {
        var payload = jwt.decode(token,secret);
        if(payload.exp <=moment().unix()){
            return res.status(401).send({
                message:'o token ja expirou'
            })
        }
    } catch (error) {
        return res.status(404).send({
            message:'o token e invalido'
        })
    }
    req.user=payload;

    next();

}
