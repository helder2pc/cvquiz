'use strict'

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var mongoosePagination = require('mongoose-pagination');


//var User = require('../models/user');
//var Follow = require('../models/follow');
//5acd2ad054b96b1e443a9a99
var Sport = require('../../models/qa/sport');

function saveQA(req, res){
    var params = req.body;
    
    if(params.question === undefined || params.answers === undefined) {
        return res.status(200).send({
            message:'Deves a pergunta e resposta'
        });
    }

     var qaSport = new Sport();
     qaSport.question=params.question; 
     qaSport.answers=params.answers; 

     qaSport.correct=qaSport.answers[0];
     qaSport.created_at=moment().unix();   
     
     qaSport.save((err,qaSportStored) => {
        if(err) return res.status(500).send({
            message: 'Error a guardar a pergunta e resposta'
        })

        if(!qaSportStored) return res.status(404).send({
            message: 'a pergunta e a resposta nao foram guardadas'
        });
 
       
        return res.status(200).send({qa: qaSportStored});
     });
}

function updateQA(req,res){

    var qaId = req.params.id;
    var update = req.body;
    Sport.findOneAndUpdate({"_id": qaId},update,{new:true},(err,question) =>{
   
        if(err) return res.status(500).send({
            message: 'Error em devolver QA'
        }); 

        if(!question) return res.status(404).send({
            message: 'Nao exite QA'
        }); 
        return res.status(200).send({
            qa:question,
        });
    });
}
function getQAs(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }
    var itemsPerPage = 10;

   

    //console.log(follow_clean);
    Sport.find().paginate(page,itemsPerPage,(err,question,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver QA'
        }); 

        if(!question) return res.status(404).send({
            message: 'Nao ha QA'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            qa:question,
        });
    })

  
}


function getQA(req, res){
    var qaId = req.params.id;

    Sport.findById(qaId, (err, question) => {
        if(err) return res.status(500).send({
            message: 'Error em devolver QA'
        }); 

        if(!question) return res.status(404).send({
            message: 'Nao exite QA'
        }); 
        return res.status(200).send({
            qa:question,
        });
    });
}


function deleteQA(req, res){
    var qaId = req.params.id;

    Sport.find({'_id':qaId}).remove ((err) =>{
        if(err) return res.status(500).send({
            message: 'Error em remover QA'
        }); 

      
        return res.status(200).send({
            message: 'QA eliminada'
        });
    })
}


// actualizar dados do utilizador 
function uploadImage(req,res){
    var qaId = req.params.id;

 /*   if(publicationId != req.user.sub){
       return  removeFilesUpload(res,files_path,'Nao tem permissao para inserir imagem')
        //return res.status(500).send({message: ''});
    }*/
   

    if(req.files){
        var filesImage=req.files.file;
        
        if(!filesImage){
            console.log("entra aqui")
            return res.status(404).send({message: 'Precisa enviar a imagem'});
        }
        var files_path=req.files.file.path; 
        var file_split = files_path.split('\\');
        var file_name = file_split[2];
        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];
      //  console.log(file_name);
        //
       
        if(file_ext == 'png'|| file_ext == 'PNG'|| file_ext == 'jpg'||file_ext == 'jpeg'){
            Sport.findOne({'user':req.user.sub, '_id':qaId}).exec((err,question)=>{ 
                console.log(question);
                if(question){
                    Sport.findByIdAndUpdate(qaId, {file:file_name}, {new:true}, (err,qaUpdate)=>{
                        if(err) return res.status(500).send({message: 'erro na consulta base dados'});
                        if(!qaUpdate) return res.status(404).send({message: 'nao foi possivel a imagem'});
                          
                        return res.status(200).send({
                            qa: qaUpdate
                        })
                   });
                }
                else{
                    return removeFilesUpload(res,files_path,'nao tem permisao para atualizar esta qa');
                }

            });
          
           
        }
        else{
         return removeFilesUpload(res,files_path,'Extensao nao valida');
        }
    }
    else{
        return res.status(200).send({message:'nao foi enviado o imagem'});
    }
}


function removeFilesUpload(res,files_path,msg){
    fs.unlink(files_path,(err)=>{
        return res.status(200).send({message:msg});
    });
}

    
// Enviar imagem
function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/qa/'+image_file;


    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }
        else{
            res.status(404).send({'message': 'A imagem nao existe'});
        }
    })
}



module.exports={
    saveQA,
    getQAs,
    getQA,
    deleteQA,
    uploadImage,
    updateQA,

} 