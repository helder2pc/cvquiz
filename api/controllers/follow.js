'use strict'

//var mongoosePagination = require('mongoose-pagination');
//var fs = require('fs');
var path = require('path');

var User = require('../models/user');
var Follow = require('../models/follow');


function saveFollow(req, res){
    var params = req.body;

    var follow = new Follow();
    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err,followStored) =>{
        if(err) return res.status(500).send({
            message:'Erro em guardar o seguimento'
        });

        if(!followStored) return res.status(500).send({
            message:'Naõ foi possivel guardar seguir'
        });

        return  res.status(200).send({
            follow:followStored
        })

    })
   
}

function deleteFollow(req, res){
    var userId = req.user.sub;
    var followId = req.params.id;

    Follow.find({'user':userId, followed:followId}).remove(err=>{
        if(err) return res.status(500).send({
            message:'Erro em deixar de seguir'
        });

        return res.status(200).send({
            message: 'seguimento removido'
        })


    });
}

//Obter os utilizadores que um utilizador segui
function getFollowingUsers(req, res){
    var userId = req.user.sub;
    if(req.params.id && req.params.page){
        userId = req.params.id;
    }
    var page = 1;

    if(req.params.page){
        page=req.params.page;
    }
    else{
        page=req.params.id;
    }

    var itemsPerPage = 4;

    Follow.find({user:userId}).populate({path: 'followed'}).paginate(page,itemsPerPage,(err, follows,total)=>{
        if(err) return res.status(500).send({
            message:'Erro no servidor'
        });

        if(!follows) return res.status(404).send({
            message:'Nao esta a seguir nenhum utilizador'
        });

        followUserIds(req.user.sub, userId).then((value) => {

            return res.status(200).send({
                total:total,
                pages: Math.ceil(total/itemsPerPage),
                follows,
                following: value.following,
                followed: value.followed
            });
        })    
    })
}




async function followUserIds(user_id) {
    try {

        var following = await Follow.find({ user: user_id }).select({'_id':0,__v:0,'user':0}).exec()
            .then((follow) => {
               
                return follow;
            })
            .catch((err) => {
                return handleError(err);
            });
    

        var followed = await Follow.find({followed: user_id }).select({'_id':0,__v:0,'followed':0}).exec()
            .then((follow) => {
               
                return follow;
            })
            .catch((err) => {
                return handleError(err);
            });
// processar following
            var following_clean =[];
            following.forEach((follow) => {
                following_clean.push(follow.followed);
            })

            var followed_clean =[];
            followed.forEach((follow) => {
                followed_clean.push(follow.user);
            })

        return {
            following: following_clean,
            followed: followed_clean
        }
    } catch (error) {
        console.log(error);
    }
    
}



//obter os utilizadores que o ulizdador id segui
function getFollowedUsers(req, res){
    var userId = req.user.sub;
    if(req.params.id && req.params.page){
        userId = req.params.id;
    }
    var page = 1;

    if(req.params.page){
        page=req.params.page;
    }
    else{
        page=req.params.id;
    }

    var itemsPerPage = 4;

    Follow.find({followed:userId}).populate('user').paginate(page,itemsPerPage,(err, follows,total)=>{
        if(err) return res.status(500).send({
            message:'Erro no servidor'
        });

        if(!follows) return res.status(404).send({
            message:'Nao es seguido por nenhum utilizador'
        });

        
        followUserIds(req.user.sub, userId).then((value) => {

            return res.status(200).send({
                total:total,
                pages: Math.ceil(total/itemsPerPage),
                follows,
                following: value.following,
                followed: value.followed
            });
        }) 
        
    });
}

//obter os utilizadores que estou  a seguir sem paginação
function getMyFollows(req, res){
    var userId = req.user.sub;

    Follow.find({user:userId}).populate('user followed').exec((err, follows)=>{
        if(err) return res.status(500).send({
            message:'Erro no servidor'
        });

        if(!follows) return res.status(404).send({
            message:'Nao segues nenhum utilizador'
        });

        return res.status(200).send({
            follows
        });
    });
}


//obter os meus seguidores sem paginação
function getFollowsBacks(req, res){
    var userId = req.user.sub;
    //var followed =  

   var find= Follow.find({user:userId});
   if(req.params.followed){
    find= Follow.find({followed:userId});
   }
   find.populate('user followed').exec((err, follows)=>{
        if(err) return res.status(500).send({
            message:'Erro no servidor'
        });

        if(!follows) return res.status(404).send({
            message:'Nao segues nenhum utilizador'
        });

        return res.status(200).send({
            follows
        });
    });

}

module.exports={
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows,
    getFollowsBacks,
}