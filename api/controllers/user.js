'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');
var jwt = require('../services/jwt');
var mongoosePagination = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');


function home(req, res){
    res.status(200).send({
        message:'Bem Vido ao CVQ'
    })
}

//Registar
function saveUser(req, res){
    var params = req.body;
    var user = new User();
    if(params.name && params.surname && 
        params.nick && params.email && params.password){
            user.name=params.name;
            user.surname=params.surname;
            user.nick=params.nick;
            user.email=params.email;
            user.role = 'ROLE_USER';
            user.image = null;

            //Controlar utilzador duplicado
            User.find({ $or:[
                {email:user.email.toLowerCase()},
                {nick: user.nick.toLowerCase()}

            ]}).exec((err,users)=>{
                if(err) return res.status(500).send({message: 'Erro em criar utilizador'});

                if(user && users.length>=1){
                    return res.status(200).send({message:'Nick ou email ja existe'});
                }
                else{
                    
                    bcrypt.hash(params.password, null,null,(err,hash) => {
                        user.password = hash;
                        user.save((err,userStored)=>{
                            if(err) return res.status(500).send({message: 'Erro em guardar utilizador'});

                            if(userStored){
                                res.status(200).send({user:userStored});
                            }
                            else{
                                res.status(404).send({message: 'Utilizador não existe'});
                            }
                        })
                    })

                }
            });

        }
        else{
            res.status(200).send({
                message :'Por favor envia tosos os campos necessario'
            })
        }
}

//login
function loginUser(req,res){
    var params = req.body;

    var email = params.email;
    var password= params.password;

    User.findOne({email:email}, (err, user)=>{
        if(err) return res.status(500).send({message:'Erro no longin'});
    
        if(user){
            bcrypt.compare(password,user.password,(err,check)=>{
                if(check){
                    if(params.gettoken){
                        return res.status(200).send({
                            token: jwt.createToken(user)
                        }) 
                    }
                    else{
                        user.password=undefined;
                        return res.status(200).send({user});
                    }
                }
                else{
                    return res.status(404).send({message: 'Password errado'})
                }
            })
        }
        else{
            return res.status(404).send({message: 'Password ou email errado'})
        }
    });
}



//Obter um utilizador pelo id
function getUser(req, res) {
    var userId = req.params.id;
    User.findById(userId, (err, user) => {
        if (err) return res.status(500).send({ message: 'Erro na consulta do utilizador, utilizador nao existe' });
        if (!user) return res.status(404).send({ message: 'Utilizador nao existe' });
        followThisUser(req.user.sub, userId).then((value) => {
            user.password = undefined;
            return res.status(200).send({
            user,
            following: value.following,
            followed: value.followed
            });
        });
    });
}

async function followUserIds(user_id) {
    try {

        var following = await Follow.find({ user: user_id }).select({'_id':0,__v:0,'user':0}).exec()
            .then((follow) => {
               
                return follow;
            })
            .catch((err) => {
                return handleError(err);
            });
    

        var followed = await Follow.find({followed: user_id }).select({'_id':0,__v:0,'followed':0}).exec()
            .then((follow) => {
               
                return follow;
            })
            .catch((err) => {
                return handleError(err);
            });
// processar following
            var following_clean =[];
            following.forEach((follow) => {
                following_clean.push(follow.followed);
            })

            var followed_clean =[];
            followed.forEach((follow) => {
                followed_clean.push(follow.user);
            })

        return {
            following: following_clean,
            followed: followed_clean
        }
    } catch (error) {
        console.log(error);
    }
    
}

async function followThisUser(identity_user_id, user_id) {
    try {
        var following = await Follow.findOne({ user: identity_user_id, followed: user_id }).exec()
            .then((following) => {
                return following;
            })
            .catch((err) => {
                return handleError(err);
            });
        var followed = await Follow.findOne({ user: user_id, followed: identity_user_id }).exec()
            .then((followed) => {
                return followed;
            })
            .catch((err) => {
                return handleError(err);
            });
        return {
            following: following,
            followed: followed
        }
    } catch (e) {
        console.log(e);
    }
}


//Obter todos os utilizadores 
function getUsers(req,res){
    var identity_user_id=req.user.sub;

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page,itemsPerPage,(err,users,total)=> {
        if(err) return res.status(500).send({message: 'Erro na consulta de utilzador'});
        if(!users) return res.status(404).send({message: 'Nao tem utilizadores disponiveis'});
        followUserIds(identity_user_id).then((value)=>{
            return res.status(200).send({
                users,
                total,
                pages: Math.ceil(total/itemsPerPage),
                user_following:value.following,
                user_follow_me:value.followed,

            });
        })
        
    });
}


//Obter todos os utilizadores 
function searchUsers(req,res){
   var identity_user_id=req.user.sub;
   var textSearch=req.params.search;

var regexValue='\.*'+textSearch+'*\.';
var regexValue2='\.'+textSearch+'\.'; //email completo

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;
console.log(textSearch)
    User.find(
       {$or: [
        {"name":new RegExp(regexValue, 'i')},
        {"surname":new RegExp(regexValue, 'i')},
        {"nick":new RegExp(regexValue, 'i')} ,
        //{"email":new RegExp(regexValue2, 'i')}  
        ]}
       /* {$or: [
        {"name": {'$regex': textSearch}}, 
        {"surname": {'$regex': textSearch}}]}*/
    ).sort('_id').paginate(page,itemsPerPage,(err,users,total)=> {
        if(err) return res.status(500).send({message: 'Erro na consulta de utilzador'});
        if(!users) return res.status(404).send({message: 'Nao tem utilizadores disponiveis'});
        followUserIds(identity_user_id).then((value)=>{
            return res.status(200).send({
                users,
                total,
                pages: Math.ceil(total/itemsPerPage),
                user_following:value.following,
                user_follow_me:value.followed,

            });
        })
        
    });
}

function getCounters(req,res){
    var userId = req.user.sub;
    if(req.params.id){
        userId =req.params.id;
    }
    getCountFollow(userId).then((value)=>{
        return res.status(200).send(value);
    })
}

async function getCountFollow(user_id) {

    try {
        var following = await Follow.count({"user":user_id}).exec()
            .then((count) => {
                return count;
            })
            .catch((err) => {
                return handleError(err);
            });


        var followed = await Follow.count({"followed":user_id}).exec()
            .then((count) => {
                return count;
            })
            .catch((err) => {
                return handleError(err);
            });

            var publications = await Publication.count({"user":user_id}).exec()
            .then((count) => {
                return count;
            })
            .catch((err) => {
                return handleError(err);
            });
    } catch (error) {
        console.log(error);
    }

    return{
        following: following,
        followed: followed,
        publications: publications
    }
}

// actualizar dados do utilizador 
function updateUsers(req,res){
    var userId = req.params.id;
    var update = req.body;

    delete update.password;
    delete update._id;
    
    if(userId != req.user.sub){
        return res.status(500).send({message: 'nao tem permissao para atulizar os dados deste utilizador'});      
    }

     User.find({ $or:[
        {email:update.email.toLowerCase()},
        {nick: update.nick.toLowerCase()}

   ]}).exec((errorT,users) =>{
        var user_isset=false;
            
            users.forEach((userAux) => {
                if(userAux && userAux._id != userId){
                    user_isset=true;
                }
            });
           
            if(user_isset){  return res.status(404).send({message: 'Os dados já estão em uso'});}
            
            User.findOneAndUpdate({"_id": userId},update,{new:true},(err,userUpdate) =>{
        
                if(err){ console.log(err); return res.status(500).send({message: 'erro na consulta base dados'});}
                if(!userUpdate) return res.status(404).send({message: 'nao foi possivel atualizzar os dados'});
                 
                return res.status(200).send({
                    user:userUpdate
                })
            });

    })
  
   
}

// actualizar dados do utilizador 
function uploadImage(req,res){
    var userId = req.params.id;

    if(userId != req.user.sub){
       return  removeFilesUpload(res,files_path,'Nao tem permissao para atualizar dados')
        //return res.status(500).send({message: ''});
    }
    if(req.files){
        var filesImage=req.files.image;
        
        if(!filesImage)
            return res.status(200).send({message: 'Precisa enviar a imagem'});

        var files_path=req.files.image.path; 
       

        var file_split = files_path.split('\\');

        var file_name = file_split[2];

        var ext_split = file_name.split('\.');

        var file_ext = ext_split[1];

        if(file_ext == 'png'|| file_ext == 'PNG'|| file_ext == 'jpg'||file_ext == 'jpeg'){
           User.findByIdAndUpdate(userId, {image:file_name}, {new:true}, (err,userUpdate)=>{
                if(err) return res.status(500).send({message: 'erro na consulta base dados'});
                if(!userUpdate) return res.status(404).send({message: 'nao foi possivel a imagem'});
                    
                return res.status(200).send({
                    user:userUpdate
                })
           });
        }
        else{
         return removeFilesUpload(res,files_path,'Extensao nao valida');
        }
    }
    else{
        return res.status(200).send({message:'nao foi enviado o imagem'});
    }
}

function removeFilesUpload(res,files_path,msg){
    fs.unlink(files_path,(err)=>{
        return res.status(200).send({message:msg});
    });
}
    
// Enviar imagem
function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }
        else{
            res.status(200).send({'message': 'A imagem nao existe'});
        }
    })
}
//Enviar foto de utilizador, avatar utilizador 
module.exports={
    home,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    searchUsers,
    getCounters,
    updateUsers,
    uploadImage,
    getImageFile,
}