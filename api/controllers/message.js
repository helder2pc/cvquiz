'use strict'

var mongoosePagination = require('mongoose-pagination');
var moment= require('moment');

var User = require('../models/user');
var Follow = require('../models/follow');
var Message = require('../models/message');


function saveMessage(req, res){
    var params = req.body;

    if(!params.text || !params.receiver)
        return res.status(200).send({
            message:'Envia os campos necessarios'
        });

        var message = new Message();
        message.emitter = req.user.sub;
        message.receiver = params.receiver;
        message.text = params.text;
        message.viewed ='false';
        message.created_at =  moment().unix();

        message.save((err, messageStored)=>{
            if(err){
                return res.status(500).send({
                    message:'Erro na consulta a bd'
                });
            }
            if(!messageStored){
                return res.status(500).send({
                    message:'Erro ao enviar mensagem'
                });
             }
             return  res.status(200).send({
                message: messageStored
            });
        })

}

function getReceivedMessages(req, res){
    var userId = req.user.sub;
    var page =  1;

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage =4;

    Message.find({'receiver':userId}).populate('emitter','name surname _id nick image').sort('-created_at').paginate(page,itemsPerPage,(err,messages,total)=>{
        if(err){
            return res.status(500).send({
                message:'Erro na consulta a bd'
            });
        }
        if(!messages){
            return res.status(404).send({
                message:'nao tem mensagem'
            });
        }

        return res.status(200).send({
          total: total,
          pages: Math.ceil(total/itemsPerPage),
          messages,
        });
    });
}

function getEmittedMessages(req, res){
    var userId = req.user.sub;
    var page =  1;

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage =4;

    Message.find({'emitter':userId}).populate('emitter receiver','name surname _id nick image').sort('-created_at').paginate(page,itemsPerPage,(err,messages,total)=>{
        if(err){
            return res.status(500).send({
                message:'Erro na consulta a bd'
            });
        }
        if(!messages){
            return res.status(404).send({
                message:'nao tem mensagem'
            });
        }
        //console.log(messages)
        return res.status(200).send({
          total: total,
          pages: Math.ceil(total/itemsPerPage),
          messages,
        });
    });
}

function getUnviewedMessages(req, res){
    var userId = req.user.sub;
    Message.count({receiver:userId, viewed: 'false'}).exec((err, count)=>{
        if(err){
            return res.status(500).send({
                message:'Erro na consulta a bd'
            });
        }
        return res.status(200).send({
            unviwer: count
        });
       
    })
}


function setViewedMessages(req, res){
    var userId = req.user.sub;

    Message.update({receiver:userId, viewed:'false'}, {viewed:'true'},{"multi":true}, (err, messageUpdate)=>{
        if(err){
            return res.status(500).send({
                message:'Erro na consulta a bd'
            });
        }
            if(!messageUpdate){
                return res.status(404).send({
                    message:'Nao foi possivel atualizar'
                });
            }

            return res.status(200).send({
                message:messageUpdate
            });

    })

}

module.exports={
    saveMessage,
    getReceivedMessages,
    getEmittedMessages,
    getUnviewedMessages,
    setViewedMessages,
}