'use strict'

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var mongoosePagination = require('mongoose-pagination');

var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');

function savePublication(req, res){
    var params = req.body;
    
    if(params.text === undefined) {
        return res.status(200).send({
            message:'Deves enviar paramentro texto'
        });
    }

     var publication = new Publication();
     publication.text=params.text; 
     publication.file=null;   
     publication.user=req.user.sub;
     publication.created_at=moment().unix();   
     console.log(publication);

     publication.save((err,publicationStored) => {
        if(err) return res.status(500).send({
            message: 'Error a guardar publicacao'
        })

        if(!publicationStored) return res.status(404).send({
            message: 'a publicacao nao foi guardada'
        });
 
       
        return res.status(200).send({publication: publicationStored});
     });
}


function getPublications(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }
    var itemsPerPage = 4;

    Follow.find({user:req.user.sub}).populate('followed').exec((err, follows)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver seguimento'
        }); 

        var follow_clean=[];

        follows.forEach((follow)=>{
            follow_clean.push(follow.followed);
        });

        follow_clean.push(req.user.sub);


    //console.log(follow_clean);
    Publication.find({user: {"$in":follow_clean}}).sort('-created_at').populate('user').paginate(page,itemsPerPage,(err,publication,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver publicacao'
        }); 

        if(!publication) return res.status(404).send({
            message: 'Nao ha publicacao'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            publication,
        });
    })

    })
}



function getPublicationsUser(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }

    var user_id=req.user.sub;
    if(req.params.id){
        user_id =req.params.id;
    }
   
    var itemsPerPage = 4;

    Publication.find({user: user_id}).sort('-created_at').populate('user').paginate(page,itemsPerPage,(err,publication,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver publicacao'
        }); 

        if(!publication) return res.status(404).send({
            message: 'Nao ha publicacao'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            publication,
        });
    })


}


function getPublication(req, res){
    var publicationId = req.params.id;

    Publication.findById(publicationId, (err, publication) => {
        if(err) return res.status(500).send({
            message: 'Error em devolver publicacao'
        }); 

        if(!publication) return res.status(404).send({
            message: 'Nao exite publicacao'
        }); 
        return res.status(200).send({
            publication,
        });
    });
}

function deletePublication(req, res){
    var publicationId = req.params.id;

    Publication.find({'user':req.user.sub, '_id':publicationId}).remove ((err) =>{
        if(err) return res.status(500).send({
            message: 'Error em remover publicacao'
        }); 

      
        return res.status(200).send({
            message: 'Publicacao eliminada'
        });
    })
}


// actualizar dados do utilizador 
function uploadImage(req,res){
    var publicationId = req.params.id;

 /*   if(publicationId != req.user.sub){
       return  removeFilesUpload(res,files_path,'Nao tem permissao para inserir imagem')
        //return res.status(500).send({message: ''});
    }*/
   

    if(req.files){
        var filesImage=req.files.file;
        
        if(!filesImage){
            console.log("entra aqui")
            return res.status(404).send({message: 'Precisa enviar a imagem'});
        }
        var files_path=req.files.file.path; 
        var file_split = files_path.split('\\');
        var file_name = file_split[2];
        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];
      //  console.log(file_name);
        //
       
        if(file_ext == 'png'|| file_ext == 'PNG'|| file_ext == 'jpg'||file_ext == 'jpeg'){
            Publication.findOne({'user':req.user.sub, '_id':publicationId}).exec((err,publication)=>{
                console.log(publication);
                if(publication){
                    //Actualizar documento publicação
                    Publication.findByIdAndUpdate(publicationId, {file:file_name}, {new:true}, (err,publicationUpdate)=>{
                        if(err) return res.status(500).send({message: 'erro na consulta base dados'});
                        if(!publicationUpdate) return res.status(404).send({message: 'nao foi possivel a imagem'});
                          
                        return res.status(200).send({
                            publication: publicationUpdate
                        })
                   });
                }
                else{
                    return removeFilesUpload(res,files_path,'nao tem permisao para atualizar esta publicacao');
                }

            });
          
           
        }
        else{
         return removeFilesUpload(res,files_path,'Extensao nao valida');
        }
    }
    else{
        return res.status(200).send({message:'nao foi enviado o imagem'});
    }
}


function removeFilesUpload(res,files_path,msg){
    fs.unlink(files_path,(err)=>{
        return res.status(200).send({message:msg});
    });
}
/*
function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }
        else{
            res.status(200).send({'message': 'A imagem nao existe'});
        }
    })
    http://localhost:3800/api/get-image-user/EhoixqRkkjviwKAuwwfbSffN.PNG
    http://localhost:3800/api/get-image-pub/60jkwjLAMXRaJTUWCrM1vcMx.jpg
                                            60jkwjLAMXRaJTUWCrM1vcMx
}*/
    
// Enviar imagem
function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/publications/'+image_file;
console.log(image_file);
console.log(path_file);

    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }
        else{
            res.status(404).send({'message': 'A imagem nao existe'});
        }
    })
}



module.exports={
    savePublication,
    getPublications,
    getPublicationsUser,
    getPublication,
    deletePublication,
    uploadImage,
    getImageFile,

} 