'use strict'

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var mongoosePagination = require('mongoose-pagination');

var FeedInfo = require('../models/feedInfo');
var User = require('../models/user');
var Follow = require('../models/follow');

function saveFeedInfo(req, res){
    var params = req.body;
    
    if(params.text === undefined) {
        return res.status(200).send({
            message:'Deves enviar paramentro texto'
        });
    }

     var feedInfo = new FeedInfo();
     feedInfo.text=params.text; 
     feedInfo.file=(params.text)?params.file:null;;   
     feedInfo.user=req.user.sub;
     feedInfo.type=params.type;
     feedInfo.created_at=moment().unix();   
     
     feedInfo.save((err,feedInfoStored) => {
        if(err) return res.status(500).send({
            message: 'Error a guardar feedInfo'
        })

        if(!feedInfoStored) return res.status(404).send({
            message: 'a feedInfo nao foi guardada'
        });
 
       
        return res.status(200).send({feedInfo: feedInfoStored});
     });
}


function getFeedInfos(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }
    var itemsPerPage = 10;

   

    //console.log(follow_clean);
    FeedInfo.find().sort('-created_at').populate('user').paginate(page,itemsPerPage,(err,feedInfo,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver feedInfo'
        }); 

        if(!feedInfo) return res.status(404).send({
            message: 'Nao ha feedInfo'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            feedInfo,
        });
    })

   
}



function getFeedInfoUser(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }

    var user_id=req.user.sub;
    if(req.params.id){
        user_id =req.params.id;
    }
   
    var itemsPerPage = 4;

    FeedInfo.find({user: user_id}).sort('-created_at').populate('user').paginate(page,itemsPerPage,(err,feedInfo,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver feedInfo'
        }); 

        if(!feedInfo) return res.status(404).send({
            message: 'Nao ha feedInfo'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            feedInfo,
        });
    })


}


function getFeedInfo(req, res){
    var feedInfoId = req.params.id;

    FeedInfo.findById(feedInfoId, (err, feedInfo) => {
        if(err) return res.status(500).send({
            message: 'Error em devolver feedInfo'
        }); 

        if(!feedInfo) return res.status(404).send({
            message: 'Nao exite feedInfo'
        }); 
        return res.status(200).send({
            feedInfo,
        });
    });
}

function deleteFeedInfo(req, res){
    var feedInfoId = req.params.id;

    FeedInfo.find({'user':req.user.sub, '_id':feedInfoId}).remove ((err) =>{
        if(err) return res.status(500).send({
            message: 'Error em remover feedInfo'
        }); 

      
        return res.status(200).send({
            message: 'FeedInfo eliminada'
        });
    })
}


// actualizar dados do utilizador 
function uploadImage(req,res){
    var feedInfoId = req.params.id;

 /*   if(publicationId != req.user.sub){
       return  removeFilesUpload(res,files_path,'Nao tem permissao para inserir imagem')
        //return res.status(500).send({message: ''});
    }*/
   

    if(req.files){
        var filesImage=req.files.file;
        
        if(!filesImage){
            console.log("entra aqui")
            return res.status(404).send({message: 'Precisa enviar a imagem'});
        }
        var files_path=req.files.file.path; 
        var file_split = files_path.split('\\');
        var file_name = file_split[2];
        var ext_split = file_name.split('\.');
        var file_ext = ext_split[1];
      //  console.log(file_name);
        //
       
        if(file_ext == 'png'|| file_ext == 'PNG'|| file_ext == 'jpg'||file_ext == 'jpeg'){
            FeedInfo.findOne({'user':req.user.sub, '_id':feedInfoId}).exec((err,feedInfo)=>{
                console.log(feedInfo);
                if(feedInfo){
                    //Actualizar documento publicação
                    FeedInfo.findByIdAndUpdate(feedInfoId, {file:file_name}, {new:true}, (err,feedInfoUpdate)=>{
                        if(err) return res.status(500).send({message: 'erro na consulta base dados'});
                        if(!feedInfoUpdate) return res.status(404).send({message: 'nao foi possivel a imagem'});
                          
                        return res.status(200).send({
                            feedInfo: feedInfoUpdate
                        })
                   });
                }
                else{
                    return removeFilesUpload(res,files_path,'nao tem permisao para atualizar esta feedInfo');
                }

            });
          
           
        }
        else{
         return removeFilesUpload(res,files_path,'Extensao nao valida');
        }
    }
    else{
        return res.status(200).send({message:'nao foi enviado o imagem'});
    }
}


function removeFilesUpload(res,files_path,msg){
    fs.unlink(files_path,(err)=>{
        return res.status(200).send({message:msg});
    });
}
/*
function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }
        else{
            res.status(200).send({'message': 'A imagem nao existe'});
        }
    })
    http://localhost:3800/api/get-image-user/EhoixqRkkjviwKAuwwfbSffN.PNG
    http://localhost:3800/api/get-image-pub/60jkwjLAMXRaJTUWCrM1vcMx.jpg
                                            60jkwjLAMXRaJTUWCrM1vcMx
}*/
    
// Enviar imagem
function getImageFile(req,res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/feedInfos/'+image_file;
console.log(image_file);
console.log(path_file);

    fs.exists(path_file,(exists)=>{
        if(exists){
            res.sendFile(path.resolve(path_file));
        }
        else{
            res.status(404).send({'message': 'A imagem nao existe'});
        }
    })

   
}



module.exports={
    saveFeedInfo,
    getFeedInfos,
    getFeedInfoUser,
    getFeedInfo,
    deleteFeedInfo,
    uploadImage,
    getImageFile,

} 