'use strict'

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var mongoosePagination = require('mongoose-pagination');

var GameS = require('../models/gameS');
var User = require('../models/user');
var Follow = require('../models/follow');

function saveGameS(req, res){
    var params = req.body;
    
    if(params.category === undefined) {
        return res.status(200).send({
            message:'Deves enviar paramentro category'
        });
    }

    if(params.mode === undefined) {
        return res.status(200).send({
            message:'Deves enviar paramentro mode'
        });
    }

    if(params.score === undefined) {
        return res.status(200).send({
            message:'Deves enviar paramentro score'
        });
    }

     var gameS = new GameS();
     gameS.category=params.category;
     gameS.mode=params.mode;
     gameS.score=params.score;
     gameS.currect_answer=(params.currectAnswer)?params.currectAnswer:0;
     gameS.wrong_answer=(params.wrongAnswer)?params.wrongAnswer:0;   
     gameS.end=true;  
     gameS.user=req.user.sub;
     gameS.created_at=moment().unix();   
     
     gameS.save((err,gameSStored) => {
        if(err) return res.status(500).send({
            message: 'Error a guardar jogo'
        })

        if(!gameSStored) return res.status(404).send({
            message: 'o jogo nao foi guardada'
        });
 
       
        return res.status(200).send({gameS: gameSStored});
     });
}


function getGameSList(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }

    var category = 'history';
    if(req.params.category){
        category =req.params.category;
    }

    
    var itemsPerPage = 10;

    Follow.find({user:req.user.sub,'category':category}).populate('followed').exec((err, follows)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver seguimento'
        }); 

        var follow_clean=[];

        follows.forEach((follow)=>{
            follow_clean.push(follow.followed);
        });

        follow_clean.push(req.user.sub);

    //console.log(follow_clean);
    GameS.find({user: {"$in":follow_clean}}).sort('-created_at').populate('user').paginate(page,itemsPerPage,(err,gameS,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver game'
        }); 

        if(!gameS) return res.status(404).send({
            message: 'Nao ha game'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            gameS,
        });
    })

    })
}



function getGameSUser(req, res){
    var page = 1;
    if(req.params.page){
        page =req.params.page;
    }

    var category = 'history';
    if(req.params.category){
        category =req.params.category;
    }


    var user_id=req.user.sub;
    if(req.params.id){
        user_id =req.params.id;
    }
   
    var itemsPerPage = 10;


    if(category=='total'){}
  
    GameS.find({user: user_id,category:category}).sort('-score').populate('user').paginate(page,itemsPerPage,(err,gameS,total)=>{
        if(err) return res.status(500).send({
            message: 'Error em devolver game'
        }); 

        if(!gameS) return res.status(404).send({
            message: 'Nao ha game'
        }); 

        return res.status(200).send({
            total_items: total,
            pages: Math.ceil(total/itemsPerPage),
            page: page,
            itemsPerPage:itemsPerPage,
            gameS,
        });
    })


}


function getGameS(req, res){
    var gameSId = req.params.id;

    GameS.findById(gameSId, (err, gameS) => {
        if(err) return res.status(500).send({
            message: 'Error em devolver game'
        }); 

        if(!gameS) return res.status(404).send({
            message: 'Nao exite game'
        }); 
        return res.status(200).send({
            gameS,
        });
    });
}

function deleteGameS(req, res){
    var gameSId = req.params.id;

    GameS.find({'user':req.user.sub, '_id':gameSId}).remove ((err) =>{
        if(err) return res.status(500).send({
            message: 'Error em remover game'
        }); 

      
        return res.status(200).send({
            message: 'Publicacao eliminada'
        });
    })
}




module.exports={
    saveGameS,
    getGameSList, 
    getGameSUser,
    getGameS,
    deleteGameS,
   

} 