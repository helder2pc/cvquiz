require('dotenv').config();

export default {
    env: process.env.NODE_ENV || 'development',
    server: {
        port: process.env.PORT || 3800
    },
    jwt: {
        secret: process.env.JWT_SECRET || 'abracadabraPe-de-cabra'
    },
	logger: {
		host: process.env.LOGGER_HOST, // Papertrail Logging Host
		port: process.env.LOGGER_PORT, // Papertrail Logging Port
	}
};