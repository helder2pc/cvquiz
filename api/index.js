'use strict'

var mongoose = require ('mongoose');
var app = require('./app');
var port = 3800;
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/bd_cvq')
        .then(()=>{
            console.log("Conecção com a base de dados db_cvq");
            //criar o servidor
            app.listen(port,()=>{
                console.log("Servidor Correndo na  Porta 3800");
            })
        })
        .catch(err=> console.log(err));