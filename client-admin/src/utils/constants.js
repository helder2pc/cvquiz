const difficult = [
    { id: 'very_easy', title: "Muito Simples" },
    { id: 'easy', title: "Fácil" },
    { id: 'medium', title: "Médio" },
    { id: 'difficult', title: "Difícil" },
    { id: 'very_difficult', title: "Muito Difícil" },
];

const type = [
    { id: 'mcq', title: "mcq" },
    { id: 'mcq_ma', title: "mcq_ma" },
    { id: 'true_false', title: "true_false" },
    { id: 'fill_in_blank', title: "fill_in_blank" },
    { id: 'match', title: "match" },
    { id: 'order', title: "order" },
    { id: 'short_answer', title: "short_answer" },
    { id: 'hotspot', title: "hotspot" },
];

const category = [
    { id: 'art', title: "Arte" },
    { id: 'sport', title: "Desporto" },
    { id: 'history', title: "História" },
    { id: 'geography', title: "Geografia" },
    { id: 'tour', title: "Tour" },
    { id: 'music', title: "Música" },
    { id: 'literature', title: "Literatura" },
];


const status = [
    { id: 'all', title: "Todos" },
    { id: 'active', title: "Activo" },
    { id: 'disabled', title: "Desativado" },
    { id: 'suspense', title: "Suspenso" },
];

const userRole = [
    { id: 'user', title: "User" },
    { id: 'admin', title: "admin" },  
];

const idiomas = [
    { id: 'pt', title: "Português" },
    { id: 'en', title: "Inglês" },
    { id: 'fr', title: "Francês" },
    { id: 'es', title: "Espanhol" },
];

const listItemsPerPage = [
    { id: '2', value: 2 },
    { id: '3', value: 3 },
    { id: '20', value: 20 },
    { id: '50', value: 50 },
];

export { 
        type, 
        difficult, 
        category, 
        status, 
        userRole,
        idiomas,
        listItemsPerPage,
    }