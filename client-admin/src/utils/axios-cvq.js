import axios from 'axios';

const axiosInstance = axios.create({
    // baseURL: 'https://cvquiz.herokuapp.com/api/',
    baseURL: 'http://localhost:4040/api/',
    headers: {
		// 'Authorization': '1234',
		'Content-Type': 'application/json',
		'Accept': 'application/json',
	}
});

axiosInstance.interceptors.response.use(
	(response) => response,
	(error) => {
	  if (error.response.status === 401) {
		// dispatch something to your store
	  }

	  if (error.response.status === 404) {
		// dispatch something to your store
	  }
  
	  return Promise.reject(error);
	}
  )

export default axiosInstance;
