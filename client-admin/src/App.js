import React, { useEffect } from 'react';

import './App.css';
import CVQRouter from './routes/router';
import {Auth} from './js/redux/actions';
import { connect } from 'react-redux';


 
const App =(props) => {

  useEffect(() => {
    props.authCheckState();
  }, [])


  return ( 
    <div id='root'>
       <CVQRouter/>
    </div>
  );
}

// export default App;
export default connect(
  null,
  Auth
)(App)
