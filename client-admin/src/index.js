import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import "video-react/dist/video-react.css"; // import css
import {I18nextProvider} from 'react-i18next';
import { ThemeProvider } from '@material-ui/core/styles';

import './index.css';
import './utils/custom.css';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import App from './App';

// import i18n (needs to be bundled ;)) 
import i18n from './i18n/index';
import theme from './themes';
import { CssBaseline } from '@material-ui/core';

// ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();


// import React from 'react';

// import './App.css';
// import CVQRouter from './Router';
// import icons from './js/components/Icons/icons';
import store from './js/redux/store';
import { Provider } from 'react-redux';


ReactDOM.render(
   <I18nextProvider i18n={i18n}>
      <Provider store={store}>
      <ThemeProvider theme={theme}>
      <CssBaseline />
         <App/> 
      </ThemeProvider>
      </Provider>
    </I18nextProvider>,document.getElementById('root')
   
   );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

export default App;
