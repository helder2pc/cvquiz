import React from 'react';
import {Tab, Tabs, Button} from 'react-bootstrap';
import Iframe from 'react-iframe';
import { ModalSidebarInfo } from '..';
import YouTube from 'react-youtube';

export default class sidebarInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { list: [
      {id:1, isVideo:true, text:"Condimentum nec, nisi. Vivamus quis mi. Phasellus magna. Praesent venenatis ", videoPath:"FGfixIe0biA"},
      {id:2,  isVideo:false, text:"Nullam 56 tincidunt adipiscing enim. Suspendisse enim turpis, dictum sed, iaculis a,", imagePath:"https://www.capeverdeislands.org/wp-content/uploads/2015/07/monte-gordo-sao-nicolau.jpg" },
      {id:3,  isVideo:true, text:"Nullam tincidunt adipiscing enim. Suspendisse enim turpis, dictum sed, iaculis a,", videoPath:"xDMP3i36naA"}

    ]};

   
  }


  render() {
    return (
      <div  className="sidebar-info" >
          
            {this.state.list.map((item) =>  <ModalSidebarInfo  key={item.id} item={item}> </ModalSidebarInfo>     )}
      
          {/*   <Info item={item} key={item.id}></Info> */}

         

         
        

      </div>
    );
  }
}


function Info(props) {
  return <div className="info"> 
   

{/*   { props.item.isVideo &&  <Iframe url={props.item.videoPath}
            width="100%"
            height="50%"
            id="myId"
            className="iframe"
            display="initial"
            position="relative"/>
          } */}

 { props.item.isVideo &&  <YouTube
  videoId="2g811Eo7K8U"
  opts={opts}
 
/>   } 

  { !props.item.isVideo &&  <div className="info-image">
    <img className="info-image" src={props.item.imagePath} />
  </div>
        }

          <p className="info-text"> {props.item.text}</p>


  </div>;
}



const opts = {
  height: '390',
  width: '640',
  playerVars: { // https://developers.google.com/youtube/player_parameters
    autoplay: 1
  }
}