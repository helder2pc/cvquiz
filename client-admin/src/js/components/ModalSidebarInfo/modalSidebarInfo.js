import React from 'react';
import {Button,Modal} from 'react-bootstrap';
import YouTube from 'react-youtube';

export default class modalSidebarInfo extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false,
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <>
        <Button variant="light" onClick={this.handleShow}>
          <Info item={this.props.item}></Info>
        </Button>

        <Modal  size="lg" show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body  >
          <Info item={this.props.item}></Info>

          </Modal.Body>
         {/*  <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.handleClose}>
              Save Changes
            </Button>
          </Modal.Footer> */}
        </Modal>
      </>
    );
  }
}


function Info(props) {

  const opts = {
    height: '100%',
    width: '100%',
    playerVars: { // https://developers.google.com/youtube/player_parameters
      autoplay: 0
    }
  }
  return <div className="info"> 
   

  { props.item.isVideo &&  <YouTube
      videoId={props.item.videoPath}
      opts={opts}
     /*  onReady={this._onReady}  */
    />
          }
  { !props.item.isVideo &&  <div className="info-image">
    <img className="info-image img-fluid" src={props.item.imagePath} />
  </div>
        }

          <p className="info-text"> {props.item.text}</p>


  </div>;
}



