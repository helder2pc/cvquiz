import React, {useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PermMedia';
import { type, difficult, idiomas, category as listCategory } from '../../../utils/constants';
import { connect } from 'react-redux';
import { Question } from '../../redux/actions';
import { toastr } from 'react-redux-toastr';

import {
    fade,
    ThemeProvider,
    withStyles,
    makeStyles,
    createMuiTheme,
} from '@material-ui/core/styles';
import AddMCQ from './addMCQ';
import AddTrueFalse from './addTrueFalse';
import AddShortAnswer from './addShortAnswer';
import AddMCQMultipleAnswer from './addMCQMultipleAnswer';
import ChipInput from '../UI/ChipInput';

const AddQuestion = (props) => {

    const formInitialState = {
        question: "",
        description: "w",
        category:'',
        difficulty:'easy',
        type:'mcq',
        // tags:['caboverde'],
        language:'pt'
        
    }
    const classes = useStyles();
    const [tags, setTags] = useState(['caboverde',]);
    const {setOpenModal, questionToEdit} = props;
    // console.log('questionToEdit')
    // console.log(questionToEdit)
    const [formData, setFormData] = useState((questionToEdit)?questionToEdit:formInitialState);


    const handleAddChip = (chip) =>{
        console.log(chip)
    }

    const handleDeleteChip = (chip, index) =>{
        console.log(chip)
    }


    const handleSubmit = (event) => {
        // console.log(formData);
        event.preventDefault();
        console.log(formData)


     
        // if(formData.question === "" ){
        //     toastr.warning('warning', 'Os campos pergunta e dificuldade  são obrigatórios');
        //      return;   
        // }

            if(questionToEdit)
                props.updateQuestionStart(formData);
            else
                props.addQuestionStart(formData);
            setFormData(formInitialState);
    }

    const handleClear = (event) => {
        setFormData({ ...formInitialState });
    }


    return (
        
        <Box className={classes.addQuestionWrapper} >
            <Box m={0} bgcolor="primary.main" p={2} display="flex">
                <Typography className={classes.title} variant="h4" gutterBottom>
                    Adicionar Perguntas
            </Typography>

                <Select
                    value={formData.type}
                    // onChange={(event) => setQuestionType(event.target.value)}
                    onChange={(event) => setFormData({ ...formData, type: event.target.value })}
                    displayEmpty
                    className={classes.selectType}
                    inputProps={{ 'aria-label': 'Without label' }}
                >

                    {type.map(item => <MenuItem key={'type'+item.id} value={item.id}>{item.title}</MenuItem>)}

                </Select>

                <IconButton 
                    aria-label="delete" 
                    onClick={() => setOpenModal(false)}
                    
                >
                    <CloseIcon  color="secondary" fontSize="small" />
                </IconButton>

            </Box>
            {/* <div className="add-question-wrapper"> */}

            <form onSubmit={handleSubmit} >

                <Box className={classes.addQuestionWrapper} padding={5}  >


                {formData.type === 'mcq' && <AddMCQ formData={formData} setFormData={setFormData} />}
                {formData.type === 'true_false' && <AddTrueFalse />}
                {formData.type === 'short_answer' && <AddShortAnswer />}
                {formData.type === 'mcq_ma' && <AddMCQMultipleAnswer />}


                <Box display="flex" alignItems="center">
                    <Box  ml={1} mr={3}>Tags:</Box>
                <ChipInput
                        fullWidth
                        defaultValue={tags}
                        onAdd={(chip) => handleAddChip(chip)}
                        onDelete={(chip, index) => handleDeleteChip(chip, index)}
                        //  onChange={(chips) => handleChange(chips)}
                        />
                </Box>

                <div className={classes.controlWrapper}>

                    <div>
                        <Select
                            value={formData.category}
                            onChange={(event) => setFormData({ ...formData, category: event.target.value })}
                            displayEmpty
                            className={classes.selectEmpty}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            <MenuItem value="" disabled>
                                Categoria
                        </MenuItem>
                            {listCategory.map(item => <MenuItem key={'category'+item.id} value={item.id}>{item.title}</MenuItem>)}

                        </Select>

                        <Select
                            value={formData.difficulty}
                            onChange={(event) => setFormData({ ...formData, difficulty: event.target.value })}
                            displayEmpty
                            className={classes.selectDif}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            <MenuItem value="" disabled>
                                Dificuldade
                        </MenuItem>
                            {difficult.map(item => <MenuItem key={'difficulty'+item.id} value={item.id}>{item.title}</MenuItem>)}

                        </Select>
                       

                        <Select
                            value={formData.language}
                            onChange={(event) => setFormData({ ...formData, language: event.target.value })}
                            displayEmpty
                            className={classes.selectDif}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            <MenuItem value="" disabled>
                                Idioma
                            </MenuItem>

                            {idiomas.map(item => <MenuItem key={'language'+item.id} value={item.id}>{item.title}</MenuItem>)}

                        </Select>

                        <IconButton color="primary" aria-label="upload picture" component="span">
                            <PhotoCamera />
                        </IconButton>
                    </div>

                    <div>


                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            size="small"
                            startIcon={<DeleteIcon />}
                            onClick={(event) => handleClear(event)}
                        >
                            Limpar
                     </Button>

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            size="small"
                            endIcon={<SendIcon />}
                            type="submit"
                        >
                            Enviar
                </Button>
                    </div>

                </div>

            </Box>
            
            </form>
        </Box>
    )
}



const mapStateToProps = ({Question}) => ({
    questionToEdit: Question.questionToEdit,
    // loading: User.loading,
  })

export default connect(
    mapStateToProps,
    Question
)(AddQuestion)

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    title: {
        color: '#fff',
        flex: 1,
    },

    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },
    addQuestionWrapper: {

        maxWidth: 960,
        width: '100%',
        backgroundColor: theme.palette.background.default
    },

    controlWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        marginLeft: 10,
        marginTop: 10,

    },

    button: {
        marginLeft: 10
    },

    selectDif: {
        marginLeft: 20,
        marginRight: 10,
    },

    selectType: {
        color: "#fff",
        width: 100
    }
}));

