import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';


export default function AddMCQ(props) {
    const classes = useStyles();

    const {formData, setFormData} =props;

    return (
        <>

            <TextField
                className={classes.textField}
                label="Pergunta"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
                rows={5}
                value={formData.question}
                onChange={(event) => setFormData({ ...formData, question: event.target.value })}

            />

            <TextField
                className={classes.textFieldAnswer}
                label="Opção Correta"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
            />

            <TextField
                className={classes.textFieldAnswer}
                label="Opção 1"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
            />

            <TextField
                className={classes.textFieldAnswer}
                label="Opção 2"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
            />

            <TextField
                className={classes.textFieldAnswer}
                label="Opção 3"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
            />

        </>

    )
}








const useStyles = makeStyles((theme) => ({


    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },


}));
