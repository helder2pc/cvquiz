import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import { generateUniqueId } from '../../../utils/functionAux'

export default function AddMCQMultipleAnswer(props) {
    const classes = useStyles();
    const [checked, setChecked] = React.useState('');

    const [options, setOptions] = React.useState([
        { id: '233', checked: true, text: '' },
        { id: '234', checked: true, text: '' },
        { id: '235', checked: false, text: '' },
    ]);


    const addOptions = () => {
        let _options = [
            ...options,
            { id: generateUniqueId(), checked: false, text: '' }
        ]
        setOptions(_options);
    }

    const removeOptions = (id) => {
         let _options = options.filter( (item) =>item.id!=id);
        setOptions(_options);
    }

    return (
        <>
            <TextField
                className={classes.textField}
                label="Pergunta"
                variant="outlined"
                // id="outlined-input-question"
                multiline={true}
                rows={5}
            />


            {options.map((item) => (

                <Box key={item.id}  display="flex">

                    <TextField
                        className={classes.textFieldAnswer}
                        label="Opção"
                        variant="outlined"
                        // id="outlined-input-question"
                        multiline={true}
                        value={item.id}
                    />

                    <TextField
                        className={classes.textFieldClass}
                        // label="Opção"
                        // variant="outlined"
                        // id="outlined-input-question"
                        // multiline={true}
                    />

                    <Checkbox
                        checked={item.checked}
                        onChange={(event) => setChecked(event.target.checked)}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />

                    <IconButton 
                        aria-label="delete" 
                        className={classes.margin}
                        onClick={() => removeOptions(item.id)}
                        >
                        <DeleteIcon fontSize="small" />
                    </IconButton>

                </Box>


            ))}

            <Fab
                size="small"
                color="secondary"
                aria-label="add"
                onClick={() => addOptions()}>
                <AddIcon />
            </Fab>

        </>

    )
}








const useStyles = makeStyles((theme) => ({


    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },
    textFieldClass:{
        width:30,
        display:'flex',
        alignSelf:'center',
        marginBottom:-10
    },


}));
