import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import Box from '@material-ui/core/Box';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { makeStyles } from '@material-ui/core/styles';


export default function AddTrueFalse(props) {
    const classes = useStyles();
    const [isTrue, setIstrue] = React.useState(true);


    return (
        <>

            <TextField
                className={classes.textField}
                label="Pergunta"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
                rows={5}
            />
    
    <Box display="flex" alignItems="center">
    <Box component="span" mb={1} mx={1}>False</Box>
        <FormControlLabel
            control={<Switch checked={isTrue} onChange={()=>setIstrue(!isTrue)} name="checkedA" />}
            label=""
        />

        <Box component="span" mb={1} ml={-2}>True</Box>
    </Box>
           

    

        </>

    )
}








const useStyles = makeStyles((theme) => ({


    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },


}));
