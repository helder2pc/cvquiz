import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';


export default function AddShortAnswer(props) {
    const classes = useStyles();

    return (
        <>

            <TextField
                className={classes.textField}
                label="Pergunta"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
                rows={5}
            />

            <TextField
                className={classes.textFieldAnswer}
                label="Resposta"
                variant="outlined"
                id="outlined-input-question"
                multiline={true}
            />


        </>

    )
}








const useStyles = makeStyles((theme) => ({


    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },


}));
