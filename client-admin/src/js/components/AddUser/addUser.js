import React, { useState, useRef } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import Select from '@material-ui/core/Select';
import Avatar from '@material-ui/core/Avatar';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PermMedia';
import { connect } from 'react-redux';
import { User } from '../../redux/actions';
import { status, userRole } from '../../../utils/constants';
import { makeStyles, } from '@material-ui/core/styles';
import { toastr } from 'react-redux-toastr';
import { AddPhotoAlternate } from '@material-ui/icons';

const AddUser = (props) => {

    const classes = useStyles();
    const [confirmPassword, setConfirmPassword] = useState('');
    const formInitialState = {
        first_name: "",
        last_name: "",
        email: "",
        username: "",
        password: "",
        role: "user",
        status: "active",
        photo: null,
    }
    const { setOpenModal, userToEdit, } = props;
  
    const [formData, setFormData] = useState((userToEdit)?userToEdit:formInitialState);
   

    const handleSubmit = (event) => {
        console.log(formData);
        event.preventDefault();
     
        if (formData.password === confirmPassword) {
            if(userToEdit)
                props.updateUserStart(formData);
            else
                props.addUserStart(formData);
            setFormData(formInitialState);
        }
        else
            // alert("Palavra-passe tem que ser igual")
            toastr.warning('warning', 'Palavra-passe tem que ser igual');
    }

    const addPhoto=(event)=>{
        const reader = new FileReader();
        const target = event.target ;
        const files =  target.files;
        if (files && files[0]) {
            const file = files[0];
            reader.readAsDataURL(file);
            reader.onload = (value) => {
                console.log(value.target.result)
                setFormData({ ...formData, photo: value.target.result })
                
            };
        }
    }

    const handleClear = (event) => {
        setFormData({ ...formInitialState });
    }



    return (
        <Box className={classes.addUserWrapper} >
            <Box m={0} bgcolor="primary.main" p={2} display="flex">
                <Typography className={classes.title} variant="h4" gutterBottom>
                    Adicionar User
            </Typography>


                <IconButton
                    aria-label="delete"
                    onClick={() => setOpenModal(false)}>
                    <CloseIcon color="secondary" fontSize="small" />
                </IconButton>

            </Box>
            {/* <div className="add-question-wrapper"> */}
            <form onSubmit={handleSubmit} >
                <Box className={classes.addUserWrapper} padding={5}  >

                    <Avatar className={classes.userAvatar} 
                        alt="user-photo"
                        src={formData.photo}/>

                    <Box display="flex">
                        <TextField
                            key={"name"}
                            className={classes.textFieldAnswer}
                            label="Nome"
                            variant="outlined"
                            value={formData.first_name}
                            onChange={(event) => setFormData({ ...formData, first_name: event.target.value })}
                        />


                        <TextField
                            key={"surname"}
                            className={classes.textFieldAnswer}
                            label="Apelido"
                            variant="outlined"
                            value={formData.last_name}
                            onChange={(event) => setFormData({ ...formData, last_name: event.target.value })}


                        />
                    </Box>

                    <Box display="flex">
                        <TextField
                            key={"email"}
                            className={classes.textFieldAnswer}
                            label="E-mail"
                            variant="outlined"
                            type="email"
                            value={formData.email}
                            onChange={(event) => setFormData({ ...formData, email: event.target.value })}


                        />

                        <TextField
                            key={"username"}
                            className={classes.textFieldAnswer}
                            label="Username"
                            variant="outlined"
                            value={formData.username}
                            onChange={(event) => setFormData({ ...formData, username: event.target.value })}
                        />
                    </Box>

                    <Box display="flex">

                        <TextField
                            key={"password"}
                            className={classes.textFieldAnswer}
                            label="Palavra-passe"
                            variant="outlined"
                            type="password"
                            value={formData.password}
                            onChange={(event) => setFormData({ ...formData, password: event.target.value })}

                        />

                        <TextField
                            key={"confirmPassword"}
                            className={classes.textFieldAnswer}
                            label="Confirmar palavra-passe"
                            variant="outlined"
                            type="password"
                            value={confirmPassword}
                            onChange={(event) => setConfirmPassword(event.target.value)}

                        />

                    </Box>


                    <Box display="flex">


                        <Select
                            key={"status"}
                            // value={category}
                            // onChange={(event) => setCategory(event.target.value)}
                            value={formData.status}
                            onChange={(event) => setFormData({ ...formData, status: event.target.value })}
                            displayEmpty
                            className={classes.selectCat}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            <MenuItem value="" disabled>
                                Estado
                        </MenuItem>
                            {status.map(item => <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>)}

                        </Select>


                        <Select
                            key={"role"}
                            value={formData.role}
                            // onChange={(event) => setCategory(event.target.value)}
                            onChange={(event) => setFormData({ ...formData, role: event.target.value })}
                            displayEmpty
                            className={classes.selectRole}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            <MenuItem value="" disabled>
                                Cargo
                    </MenuItem>
                            {userRole.map(item => <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>)}
                        </Select>


                        {/* <IconButton 
                                color="primary" 
                                aria-label="upload picture" 
                                component="span"
                                onClick={()=>inputEl.current.}
                                >
                            <PhotoCamera />
                        </IconButton> */}

                        <input 
                            accept="image/*"
                            className={classes.inputFile}
                            id="contained-button-file"
                            multiple
                            type="file"
                            onChange={addPhoto}
                            /> 

                    <label htmlFor="contained-button-file">
                    
                            <IconButton 
                                color="primary" 
                                aria-label="upload picture" 
                                component="span"
                                
                                >
                            <PhotoCamera />
                        </IconButton>
                        </label>
                    </Box>





                    <div className={classes.controlWrapper}>





                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            size="small"
                            startIcon={<DeleteIcon />}
                            onClick={(event) => handleClear(event)}

                        >
                            Limpar
                     </Button>

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            size="small"
                            endIcon={<SendIcon />}
                            type="submit"
                        >
                            Enviar
                </Button>


                    </div>

                </Box>
            </form>
        </Box>
    )
}


// export default AddUser;

const mapStateToProps = ({User}) => ({
    userToEdit: User.userToEdit,
    // loading: User.loading,
  })

export default connect(
    mapStateToProps,
    User
)(AddUser)

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    title: {
        color: '#fff',
        flex: 1,
    },

    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },
    inputFile:{
        display:"none"
    },
    addUserWrapper: {

        maxWidth: 960,
        width: '100%',
        backgroundColor: theme.palette.background.default
    },

    controlWrapper: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginLeft: 10,
        marginTop: 10,

    },

    button: {
        marginLeft: 10
    },

    selectDif: {
        marginLeft: 20,
        marginRight: 10,
    },

    selectType: {
        color: "#fff",
        width: 100
    },

    selectCat: {
        marginTop: 10,
        marginLeft: 10,
    },

    selectRole: {
        marginTop: 10,
        marginLeft: 30,
    },

    userAvatar: {
        width: theme.spacing(10),
        height: theme.spacing(10),
        marginRight: 'auto',
        marginLeft: 'auto',
        marginBottom: 10,
        marginTop: -10,
    },

}));
