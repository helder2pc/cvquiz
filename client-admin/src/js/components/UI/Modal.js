
import React from 'react';
import MaterialModal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { makeStyles } from '@material-ui/core/styles';



export default function Modal({children, openModal,setOpenModal }) {

    const classes = useStyles();

    return(
    <MaterialModal
    aria-labelledby="transition-modal-title"
    aria-describedby="transition-modal-description"
    className={classes.modal}
    open={openModal}
    onClose={() => setOpenModal(false)}
    closeAfterTransition
    BackdropComponent={Backdrop}
    BackdropProps={{
      timeout: 500,
    }}
  >
  <Fade in={openModal}> 
        {children}
    </Fade>
  </MaterialModal>

    )
}

// Modal.propTypes = {

// }


const useStyles = makeStyles({
   
  
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  
  });
  