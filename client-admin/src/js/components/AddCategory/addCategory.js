import React, {useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PermMedia';
import { category as listCategory } from '../../../utils/constants';
import { connect } from 'react-redux';
import { Category } from '../../redux/actions';
import { toastr } from 'react-redux-toastr';


 


import {
    makeStyles,
} from '@material-ui/core/styles';

const AddCategory = (props) => {
    const formInitialState = {
        name: "",
        description: "",
    }
    const classes = useStyles();
    const [category, setCategory] = useState('');
    const { setOpenModal, categoryToEdit, } = props;

    const [formData, setFormData] = useState((categoryToEdit)?categoryToEdit:formInitialState);
   

    const handleSubmit = (event) => {
        // console.log(formData);
        event.preventDefault();
     
        if(formData.name === "" || formData.description === ""){
            toastr.warning('warning', 'Os campos name e descrição são obrigatórios');
             return;   
        }

            if(categoryToEdit)
                props.updateCategoryStart(formData);
            else
                props.addCategoryStart(formData);
            setFormData(formInitialState);
    }

    const handleClear = (event) => {
        setFormData({ ...formInitialState });
    }


    





    return (
        <Box className={classes.addQuestionWrapper} >
            <Box m={0} bgcolor="primary.main" p={2} display="flex">
                <Typography className={classes.title} variant="h4" >
                    Adicionar Categoria
            </Typography>


                <IconButton
                    aria-label="delete"
                    onClick={() => setOpenModal(false)}>
                    <CloseIcon color="secondary" fontSize="small" />
                </IconButton>

            </Box>
            {/* <div className="add-question-wrapper"> */}

            <form onSubmit={handleSubmit} >
                <Box className={classes.addQuestionWrapper} padding={5}  >


                <TextField
                    className={classes.textFieldAnswer}
                    label="Name"
                    variant="outlined"
                    id="outlined-input-question"
                    // multiline={true}
                    value={formData.name}
                    onChange={(event) => setFormData({ ...formData, name: event.target.value })}

                />

                <TextField
                    className={classes.textField}
                    label="Descrição"
                    variant="outlined"
                    id="outlined-input-description"
                    multiline={true}
                    rows={5}
                    value={formData.description}
                    onChange={(event) => setFormData({ ...formData, description: event.target.value })}

                />

                <Select
                    value={category}
                    onChange={(event) => setCategory(event.target.value)}
                    displayEmpty
                    className={classes.selectCat}
                    inputProps={{ 'aria-label': 'Without label' }}
                >
                    <MenuItem value="" disabled>
                        Categoria Main
                        </MenuItem>
                    {listCategory.map(item => <MenuItem key={item.id} value={item.id}>{item.title}</MenuItem>)}

                </Select>

                <div className={classes.controlWrapper}>

                    <div>
                        <IconButton color="primary" aria-label="upload picture" component="span">
                            <PhotoCamera />
                        </IconButton>
                    </div>

                    <div>


                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            size="small"
                            startIcon={<DeleteIcon />}
                            onClick={(event) => handleClear(event)}
                        >
                            Limpar
                     </Button>

                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            size="small"
                            endIcon={<SendIcon />}
                            type="submit"
                        >
                            Enviar
                </Button>
                    </div>

                </div>

            </Box>
            </form>
        </Box>
    )
}



const mapStateToProps = ({Category}) => ({
    categoryToEdit: Category.categoryToEdit,
    // loading: User.loading,
  })

export default connect(
    mapStateToProps,
    Category
)(AddCategory)


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    title: {
        color: '#fff',
        flex: 1,
    },

    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textFieldAnswer: {
        margin: theme.spacing(1),
        width: '100%',

    },
    addQuestionWrapper: {

        maxWidth: 960,
        width: '100%',
        backgroundColor: theme.palette.background.default
    },

    controlWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        marginLeft: 10,
        marginTop: 10,

    },

    button: {
        marginLeft: 10
    },

    selectDif: {
        marginLeft: 20,
        marginRight: 10,
    },

    selectType: {
        color: "#fff",
        width: 100
    },

    selectCat: {
        marginTop: 10,
        marginLeft: 10,
    }

}));
