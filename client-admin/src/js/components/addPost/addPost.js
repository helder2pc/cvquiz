import React from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import SendIcon from '@material-ui/icons/Send';
import Select from '@material-ui/core/Select';
import Avatar from '@material-ui/core/Avatar';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PermMedia';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { connect } from 'react-redux';
import { Post } from '../../redux/actions';
import { toastr } from 'react-redux-toastr';
// import Base64UploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/base64uploadadapter';

// import DateFnsUtils from '@date-io/date-fns';
// import {
//     MuiPickersUtilsProvider,
//     KeyboardTimePicker,
//     KeyboardDatePicker,
//   } from '@material-ui/pickers';

import { status, idiomas, category as listCategories } from '../../../utils/constants';


import {
    fade,
    ThemeProvider,
    withStyles,
    makeStyles,
    createMuiTheme,
} from '@material-ui/core/styles';
import ChipInput from '../UI/ChipInput';

const AddPost = (props) => {

    const classes = useStyles();
    const [category, setCategory] = React.useState('');
    const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));
    const [tags, setTags] = React.useState(['caboverde',]);

    const formInitialState = {
        
        title: "",
        summary: "",
        content: "<p></p> <p></p> <p></p>",
        category_id: "",
        publisher_id: "",
        tags:[],
        language:'pt'
    }
    const { setOpenModal, postToEdit, } = props;
    const [formData, setFormData] = React.useState((postToEdit)?postToEdit:formInitialState);
   

    const handleSubmit = (event) => {
        // console.log(formData);
        event.preventDefault();
     
        console.log(formData);
       
            if(postToEdit)
                props.updatePostStart(formData);
            else
                props.addPostStart(formData);
            setFormData(formInitialState);
    }

    const handleClear = (event) => {
        setFormData({ ...formInitialState });
    }

    return (
        <Box className={classes.addPostWrapper} >
            <Box m={0} bgcolor="primary.main" p={2} display="flex">
                <Typography className={classes.title} variant="h4" gutterBottom>
                    Adicionar Post
            </Typography>


                <IconButton
                    aria-label="delete"
                    onClick={() => setOpenModal(false)}>
                    <CloseIcon color="secondary" fontSize="small" />
                </IconButton>

            </Box>
            {/* <div className="add-question-wrapper"> */}


            <form onSubmit={handleSubmit} >
                 <Box className={classes.addPostWrapper} padding={2}  >


                <TextField
                    className={classes.textField}
                    label="Title"
                    variant="outlined"
                    id="outlined-input-title"
                    size="small"
                    value={formData.title}
                    onChange={(event) => setFormData({ ...formData, title: event.target.value })}

                />

                <TextField
                    className={classes.textField}
                    label="Autor"
                    variant="outlined"
                    id="outlined-input-author"
                    size="small"
                    // onChange={(event) => setFormData({ ...formData, author: event.target.value })}

                />




                <TextField
                    className={classes.textField}
                    label="Sumário"
                    variant="outlined"
                    id="outlined-input-summary"
                    multiline={true}
                    rows={3}
                    size="small"
                    value={formData.summary}
                    onChange={(event) => setFormData({ ...formData, summary: event.target.value })}

                />

                <Box className={classes.editor} ml={1} mt={2} mb={2}>
                    <CKEditor
                        // plugins={ [ Base64UploadAdapter ] }
                        editor={ClassicEditor}
                        data={formData.content}
                        onInit={editor => {
                            // You can store the "editor" and use when it is needed.
                            console.log('Editor is ready to use!', editor);
                        }}
                        onChange={(event, editor) => {
                            const data = editor.getData();
                            // console.log({ event, editor, data });
                            // console.log(data);
                            setFormData({ ...formData, content: data });
                        

                        }}
                        // onBlur={(event, editor) => {
                        //     console.log('Blur.', editor);
                        // }}
                        // onFocus={(event, editor) => {
                        //     console.log('Focus.', editor);
                        // }}
                    />

                </Box>
                <Box display="flex" mt={2} alignItems="center">
                    <Box ml={1} mr={3}>Tags:</Box>
                    <ChipInput
                        fullWidth
                        defaultValue={tags}
                    // onAdd={(chip) => handleAddChip(chip)}
                    // onDelete={(chip, index) => handleDeleteChip(chip, index)}
                    //  onChange={(chips) => handleChange(chips)}
                    />
                </Box>

                <Box display="flex" alignItems="center" mt={2}>



                    <Select
                        value={category}
                        onChange={(event) => setCategory(event.target.value)}
                        displayEmpty
                        className={classes.selectCat}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                        <MenuItem value="" disabled>
                            Estado
                        </MenuItem>
                        {status.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

                    </Select>

                    <Select

                        value={category}
                        onChange={(event) => setCategory(event.target.value)}
                        displayEmpty
                        className={classes.selectCat}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                        <MenuItem value="" disabled>
                            Categoria
                        </MenuItem>
                        {listCategories.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

                    </Select>

                    <Select
                        value={category}
                        onChange={(event) => setCategory(event.target.value)}
                        displayEmpty
                        className={classes.selectCat}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                        <MenuItem value="" disabled>
                            Idioma
                        </MenuItem>
                        {idiomas.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

                    </Select>

                    {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Date picker dialog"
                    format="MM/dd/yyyy"
                    value={selectedDate}
                    onChange={(date)=>{setSelectedDate(date)}} 
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                    </MuiPickersUtilsProvider> */}

                    <TextField
                        id="date"
                        label="Data"
                        type="date"
                        defaultValue={selectedDate}
                        className={classes.textDate}
                        onChange={(date) => { setSelectedDate(date) }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />


                    <IconButton color="primary" aria-label="upload picture" component="span">
                        <PhotoCamera />
                    </IconButton>

                </Box>





                <div className={classes.controlWrapper}>





                    <Button
                        variant="contained"
                        color="secondary"
                        className={classes.button}
                        size="small"
                        startIcon={<DeleteIcon />}
                        onClick={(event) => handleClear(event)}

                    >
                        Limpar
                     </Button>

                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        size="small"
                        endIcon={<SendIcon />}
                        type="submit"
                    >
                        Enviar
                </Button>


                </div>

            </Box>
            </form>
        </Box>
    )
}




const mapStateToProps = ({Post}) => ({
    postToEdit: Post.postToEdit,
    // loading: User.loading,
  })

export default connect(
    mapStateToProps,
    Post
)(AddPost)


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    title: {
        color: '#fff',
        flex: 1,
    },

    textField: {
        margin: theme.spacing(1),
        width: '100%'
    },
    textField: {
        margin: theme.spacing(1),
        width: '100%',

    },
    addPostWrapper: {

        maxWidth: 960,
        width: '100%',
        backgroundColor: theme.palette.background.default
    },

    controlWrapper: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginLeft: 10,
        marginTop: 10,

    },

    button: {
        marginLeft: 10
    },

    selectCat: {
        marginTop: 18,
        marginLeft: 10,
        marginRight: 10,
    },

    textDate: {
        width: 150,
        marginLeft: 10,
        marginRight: 10,
    },

    editor: {
        // marginLeft:10
        // marginTop:10
    },





}));
