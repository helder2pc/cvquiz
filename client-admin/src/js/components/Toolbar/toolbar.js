import React from 'react';
import {Navbar,NavDropdown,Nav} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Link
} from 'react-router-dom';

export default function toobar() {


  return (
    <div className="navigation">
    <Navbar collapseOnSelect expand="lg" className="cvq-navbar"  variant="dark">
      <Navbar.Brand href="/">CVQuiz</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className="mr-auto">
      <Nav.Link href="/menu">
        <FontAwesomeIcon icon="play" /> 
        <span>Jogar</span>
      </Nav.Link>
          <Nav.Link href="#feed">
         
          <FontAwesomeIcon icon="list" /> 
        <span> Timeline</span>
          </Nav.Link>
      </Nav>
        <Nav >
        
          <Nav.Link href="#deets">
            <FontAwesomeIcon icon="envelope" /> 
            

          </Nav.Link>
          <Nav.Link eventKey={2} href="#memes">
          <div className="avatar">
              <img src={require('../../../assets/img/user.png')} />
          </div>
          </Nav.Link>

          <NavDropdown title="User" id="collasible-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Meu Perfil</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Meus dados</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Perguntas</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Definiçoes</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Ajuda</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">Sair</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      
      </Navbar.Collapse>
    </Navbar>
    
    </div>
  );
}