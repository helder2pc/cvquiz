import React from 'react';
import icons from '../../images'
export default class timelinePublication extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <article  className="panel panel-default panel-outline">

                  
      <div className="panel-heading icon">
        
          <img className="icon img-fluid img-rounded" src={this.props.iconPath} />
      </div>
    
      <div className="panel-body">
         
          <div className="image-user float-left">
              <a>
              
                   <img className="icon img-fluid rounded avatar-default" src={icons.user} />
              </a>
          </div>


          <div className="publication-body text-left">

              <div>
                  <a  className="user-name" >
                      {this.props.name}
                  </a>
                  &nbsp;| &nbsp;
                  <span className="date ">
                     há nove dias 
                  </span>
                  <a className="btn btn-info btn-xs btn-publication" >
                      Jogar
                  </a>

                  <a className="btn btn-warning btn-xs btn-publication" > 
                      Duelo
                  </a>

              </div>
              <p className="publication-text text-left ">
                      Vivamus quis mi. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Ut leo.
              </p>

          </div>
      </div>
    

  </article>
 

    )}
}

