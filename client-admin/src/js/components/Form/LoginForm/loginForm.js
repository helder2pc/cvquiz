import React from 'react';
import {Tab, Tabs, Button,ListGroup} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {connect} from "react-redux";
// import {AuthActions} from '../../../redux/actions/index'  

const FormLogin = ({ segnIn }) => (

    <div> 
        <input placeholder='Utilizador' className="form-input" type="text" value="" onChange={()=>{}} />      
        <input placeholder='Password' className="form-input" type="password" value="" onChange={()=>{}} />

          <Button className="forget-password" variant="link">Esqueceste da tua conta?</Button>
          <button className="btn-login btn-secondary" onClick={segnIn}>Login</button>
    </div>
);


export default connect(
  null,
 null,
)(FormLogin);