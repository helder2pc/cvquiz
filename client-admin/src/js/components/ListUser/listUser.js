import React from 'react';
import {Tab, Tabs, Button,ListGroup} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default class listUser extends React.Component {

constructor(props){
  super(props);
  this.state={
    list:[
      {id:1, name: 'Helder Correia', image:'https://robohash.org/velquised.png?size=55x55&set=set1'},
      {id:2, name: 'Gilson Correia', image:'https://robohash.org/utnonhic.png?size=55x55&set=set1'},
      {id:3, name: 'Ana Tavares', image:'https://robohash.org/iurearchitectoquisquam.png?size=55x55&set=set1'},
    ]
    ,data:[],
    textFilter:''
  }
}
	componentWillMount(){
    this.setState({data:this.state.list});
		//this.props.setPage('landing')
		//TweenMax.to(window, 1, {scrollTo:{y:0}, ease:Power4.easeOut});
  }
 

  filterText(textFilter){
    let value = textFilter.target.value;
    this.setState({value});
    this.searchFilterFunction(value);
  }

  searchFilterFunction = text => {
    const {list} = this.state;
    const newData = list.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      text=text.trim();
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ data: newData });
  };

	render()
  {
  
return(
  <div>
    <input placeholder='Procurar Pessoas...' className="form-control input-search" type="text"
     value={this.props.textFilter}  onChange={(textFilter) => this.filterText(textFilter)} />
    
      <ListGroup>
      
        {this.state.data.map((user) =>   
          <ListGroup.Item key={user.id}> 
            <ListItem user={user} ></ListItem>
           </ListGroup.Item>
           )}
         
      </ListGroup>
  </div>
  )
  }
}


function ListItem(props) {
  return <div className="item-user"> 

  <div className="item-user-image-name">
      <div className="image-user">
      
            <a href="#">
            <img src={props.user.image} />
            </a>
      </div>
      <a className="name-surname">{props.user.name}</a>
    </div>

    <div className="item-user-btn">
      <Button variant="success" size="sm">
        <FontAwesomeIcon icon="plus-circle" />
      </Button>
      <Button variant="warning" size="sm">
        Duelo
      </Button>
    </div>
  </div>;
}