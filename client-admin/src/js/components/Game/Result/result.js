import React from 'react';
import {Tab, Tabs, Button,ListGroup} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
/* import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
 */

// Import react-circular-progressbar module and styles
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

// Animation
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./AnimatedProgressProvider";


export default class result extends React.Component {

constructor(props){
  super(props);
  this.state={
  }
}
	componentWillMount(){}

	render(){
    const percentage = 66;
    return(
      <div className="container-center container-result-page">

        <div className="main-wrap">
            <h2>Resultado</h2>

            <div className="container-chart-text">
              <div className="container-text">
                <h3>Sumário</h3>
                <h6 className="total-answer">Total de Perguntas 4</h6>
                <h6 className="correct-answer">Respostas corretas 4</h6>
                <h6 className="wrong-answer">Respostas erradas 0</h6>

              </div>

              <div className=" progressbar-result">
                    <AnimatedProgressProvider
                            valueStart={0}
                            valueEnd={66}
                            duration={1.4}
                            easingFunction={easeQuadInOut}
                            
                          >
                            {value => {
                              const roundedValue = Math.round(value);
                              return (
                                <CircularProgressbar
                                  value={value}
                                  text={`${roundedValue}%`}
                                  /* This is important to include, because if you're fully managing the
                            animation yourself, you'll want to disable the CSS animation. */
                                  styles={buildStyles({ pathTransition: "none" })}
                                />
                              );
                            }}
                      </AnimatedProgressProvider>
                  </div>
              </div>
            <buttom className="share-on-facebook btn btn-primary"> Partilhar no facebook</buttom> 
           
           <div className="container-btn-option">
           
           <buttom className="btn btn-info"> Repetir</buttom> 
           <buttom className="btn btn-info"> Categorias</buttom> 
           <buttom className="btn btn-info"> Timeline</buttom> 
           <buttom className="btn btn-info"> Ranking</buttom> 
           </div>
        </div>

           
         
      </div>
      )
  }
}


function compAux(props) {
  return <div className=""> </div>;
}