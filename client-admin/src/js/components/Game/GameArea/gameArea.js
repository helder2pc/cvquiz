import React from 'react';
import {connect} from "react-redux";
import {ProgressBar} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Row,Col} from 'react-bootstrap'
import { GameActions } from '../../../redux/actions';
import ReactCountdownClock from 'react-countdown-clock';


// import { useTranslation, withTranslation, Trans } from 'react-i18next';
// import { translate } from 'react-i18next'
//import { useTranslation } from 'react-i18next';
// const { t, i18n } = useTranslation();


// import {GameActions} from '../../redux/actions/index' ; 
const staticTimer=20;
 class gameArea extends React.Component {

constructor(props){
  super(props);
  this.state={
    timer:10,
    countdownClock:20,
    _showTimer:true
  }

}

  componentDidMount(){
      this.props.startGame();
      
      let timer =  setInterval(()=>{ 
        this.updateTimer();
       }, 100);
  }

  updateTimer(){
    let _timer=this.state.timer+0.2;
      this.setState({timer:_timer});
    
  }


  _nextQuestion(){
    //const { nextQuestion} = this.props;
    // this.setState({timer:0});
    this.setState({timer:0});
    this.setState({countdownClock:20})
    
    this.props.nextQuestion();
    // alert(this.state.countdownClock)

    this.setState({_showTimer:false})
    setTimeout(()=>{
      this.setState({_showTimer:true})
    },100)
    
  }
 
  showTimer(){
    const {_showTimer} = this.state;
    if(!_showTimer ){
      return(
        <ReactCountdownClock seconds={10}
        color="blue"
        alpha={0.4}
        size={40}
        showMilliseconds={true}
        onComplete={()=> {this.goToResultPage()}} />
      )
    }
    return(
      <ReactCountdownClock seconds={this.state.countdownClock}
        color="blue"
        alpha={0.4}
        size={40}
        showMilliseconds={true}
        onComplete={()=> {this.goToResultPage()}}  />
    )
  }

  goToResultPage(){
   this._nextQuestion();
  }
	render(){
  const {currentQA, nextQuestion,questionId} = this.props;
  const {timer,_showTimer} = this.state;
  // const { t } = this.props;
    return(
      // <div className="countdownMainContainer">
      //  <div className="countdownContainer">3</div>
      // </div>
      
    <div className="wrap-container">

       
            <div className="container-game-area" id="infogame">

              <div className="block  text-primary info">
                   Pontos: 2 
                   {/* {t('play')} */}
              </div>

                

                <div className="block  info text-primary info">
                 { `Pergunta:${questionId+1}/10`}
                </div>
                

                <div className="block   info text-primary info">
                  Categoria: História
                </div>

                <div className="block   info text-primary info">
                  Modo: Single Player
                </div>

                <div className="block  text-primary info">
                  {/* {/* Pontos: 2 /}
                  {/* <!--span className="glyphicon glyphicon-usd"></span-->  *} */}
                  {this.showTimer()}
                </div>
                
                     
            </div>
           

         {/* <div className="progress" >
                <div   className="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow={timer}
                aria-valuemin="0" aria-valuemax="100" >
                </div>
            </div>  */}



             {/* <ProgressBar now={timer} /> */}


    {currentQA && currentQA.question && <div className="question">{currentQA.question}</div> }
           
    

    {currentQA && currentQA.answer &&  <div className="container-btn">
         
            <button type="button" className="btn  btn-info btnOption" onClick={ ()=>{this._nextQuestion()}}> 
                {currentQA.answer[0]}
            </button>

            <button type="button" className="btn  btn-info btnOption" onClick={ ()=>{this._nextQuestion()}}> 
                {currentQA.answer[1]}
            </button>
            
            
            <button type="button" className="btn  btn-info btnOption" onClick={nextQuestion} onClick={ ()=>{this._nextQuestion()}}> 
                {currentQA.answer[2]}
            </button>


            <button type="button" className="btn  btn-info btnOption" onClick={ ()=>{this._nextQuestion()}}> 
                {currentQA.answer[3]}
            </button>
    </div>  }
        

    </div> 
      
      
     
    )
  }
}


function compAux(props) {
  return <div className=""> </div>;
}


const mapStateToProps = state => ({
  questions: state.Game.questions,
  questionId: state.Game.questionId,
  currentQA: state.Game.currentQA,
})
export default connect(
  mapStateToProps,
  GameActions
)(gameArea)