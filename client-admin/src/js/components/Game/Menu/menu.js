import React from 'react';
import {Tab, Tabs, Button,ListGroup} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Icons from '../../../images'
import {NavLink} from 'react-router-dom';
export default class listUser extends React.Component {

constructor(props){
  super(props);
  this.state={
    category:[
      {id:1, name:"Aleatório", iconPath:Icons.categories.random},
      {id:2, name:"Art", iconPath: Icons.categories.art},
      {id:3, name:"Desporto", iconPath:Icons.categories.sport},
      {id:4, name:"História", iconPath:Icons.categories.history},
      {id:5, name:"Geografia", iconPath:Icons.categories.geography},
      {id:6, name:"Tour", iconPath:Icons.categories.tour},
     
    ]
  }
}
	componentWillMount(){}

  handleButtonChange(type) {
    // this.setState({value: event.target.value});

  }
	render(){
  
    return(
      <div className="game-area">
      <div className="logo-container">
        <img className=" logo img-fluid img-rounded" src={Icons.logo} />
        <h3>Modos de jogo</h3>
        </div> 
       
        {this.state.category.map( (cat) => 
          <ButtonCategory 
              key={cat.id}
              name={cat.name}
              iconPath={cat.iconPath}

          
          ></ButtonCategory>
          )}
     
    </div>
      )
  }
}


function ButtonCategory(props) {
  return (
    <NavLink to='/game-area'>
        <Button variant="light" className="btn-category-container" onClick=''>
        <div className="category-logo-container">
        
          <img className="category-logo-img img-fluid img-rounded" src={props.iconPath} />
        
        </div>
        <p>{props.name}</p> 
       </Button>
    </NavLink>
  )
  ;
}