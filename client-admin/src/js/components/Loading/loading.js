import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import { Spinner } from '../../components';


 const Loading = (props) => {
const classes = useStyles();

if(props.loading)
  return (
     <Box className={classes.loginLoad}>
          <Spinner></Spinner>
      </Box>
  );

  return null;
}

export default Loading;

const useStyles = makeStyles((theme) => ({

  loginLoad:{
    position:"absolute",
    top:0,
    left:0,
    bottom:0,
    right:0,
    backgroundColor:"rgba(255,255,255,0.5)",
    zIndex:2,
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
  },

}));
