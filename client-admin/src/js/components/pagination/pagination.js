import React from 'react'
import PropTypes from 'prop-types'
import Box from '@material-ui/core/Box';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Pagination  from '@material-ui/lab/Pagination';
import { makeStyles } from '@material-ui/core/styles';
import { listItemsPerPage  } from "../../../utils/constants";

const TablePagination = ({length, totalPages, itemPerPage, setPage, setItemPerPage}) => {
    const classes = useStyles();

    return (

        <Box marginTop={2} display="flex" justifyContent="space-between" alignItems="center">
            <Pagination count={totalPages} onChange={(event, page) => setPage(page)} />

            <Box marginTop={2} display="flex" alignItems="center" >
                <Typography >{`Total: ${length}  `}</Typography>
                <Select
                    value={itemPerPage}
                    onChange={(event) => setItemPerPage(event.target.value)}
                    displayEmpty
                    className={classes.select}
                    // className={classes.selectCat}
                    inputProps={{ 'aria-label': 'Without label' }}
                >
                    <MenuItem value="" disabled>
                        Item por página
                        </MenuItem>
                    {listItemsPerPage.map(({ id, value }) => <MenuItem key={id} value={value}>{value}</MenuItem>)}

                </Select>
            </Box>
        </Box>
    )
}

TablePagination.propTypes = {

}

export default TablePagination



const useStyles = makeStyles((theme) => ({
   
  
    select: {
      marginLeft: 15,
      color: '#ffffff'
    },
  
  }));
  