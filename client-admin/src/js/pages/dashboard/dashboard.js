import React, {useState} from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
  HelpOutline, Category as CategoryIcon,
  People as PeopleIcon, Assignment as AssignmentIcon,
  EmojiEvents as EmojiEventsIcon,
  ExitToApp as ExitToAppIcon,
} from '@material-ui/icons';

import CategoryTable from './categoryTable';
import UserTable from './userTable';
import RankingTable from './rankingTable';
import QuestionsTable from './questionsTable';
import PostTable from './postTable';
import { Box } from '@material-ui/core';
import { connect } from 'react-redux';
import {Auth} from '../../redux/actions';


const drawerWidth = 240;


const Dashboard = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = useState(true);
  const [itemSelect, setItemSelect] = useState(1);
  const {name, username, user} = props;
  

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>

      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          
          <Box 
           className={classes.header}
            >
          <Typography variant="h6" noWrap>
            Dashboard
          </Typography>

          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={()=>props.logout()}
            edge="start"
            
          >
            <ExitToAppIcon />
          </IconButton>

          </Box>
          

          
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />

{
    open && <>
          <Box textAlign="center" px={1} py={1} >
          <Avatar className={classes.userAvatar} src={user && user.photo && user.photo.file}></Avatar>
          <Typography variant="h4" noWrap  >
          {name}
          </Typography>

          <Typography variant="subtitle1"  color="secondary" noWrap>
            {`@${username}`}
          </Typography>
          </Box>
          
          <Divider  />
  </>
        }

        <List className={classes.lisContainer}>
          {[
            { id: 1, title: 'Perguntas', icon: <HelpOutline /> },
            { id: 2, title: 'Categorias', icon: <CategoryIcon /> },
            { id: 3, title: 'User', icon: <PeopleIcon /> },
            { id: 4, title: 'Posts', icon: <AssignmentIcon /> },
            { id: 5, title: 'Ranking', icon: <EmojiEventsIcon /> },].map(({ id, title, icon }, index) => (

              <ListItem
                button
                key={title}
                className={(itemSelect == id) ? classes.buttonActive : null}
                onClick={() => setItemSelect(id)}
              >
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={title} />
              </ListItem>
            ))}
        </List>

      </Drawer>
      <main className={classes.content}>

        {(itemSelect === 1) && <QuestionsTable />}
        {(itemSelect === 2) && <CategoryTable />}
        {(itemSelect === 3) && <UserTable />}
        {(itemSelect === 4) && <PostTable/>}
        {(itemSelect === 5) && <RankingTable />}


      </main>
    </div>
  );
}

const mapStateToProps = ({Account}) => ({
  name: Account.name,
  user: Account.user,
  username: Account.username,
})

export default connect(
  mapStateToProps,
  Auth
)(Dashboard)






const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },

  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    // backgroundColor: '#3393d1',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },

  buttonActive: {
    // backgroundColor: theme.palette.secondary.light,
    // backgroundColor: 'rgba(255, 143, 0, 0.1)', secondary
    backgroundColor: 'rgba(69, 90, 100, 0.3)',
    //'rgba(51, 147, 209, 0.27)'
    // color:"white"
  },

  lisContainer: {
    paddingTop: 0,
    marginTop:20,
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop:80
  },

  button: {
    margin: theme.spacing(1),
    width: 200
  },

  containerBtb: {

    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 30,

  },


  userAvatar: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom: 10,
    marginTop: 10,
},

  userName:{
    textAlign:'center',
  },
  userUsername:{
    textAlign:'center',
  },

  header:{
    width:"100%",
    display:"flex",
    justifyContent:"space-between",
    alignItems:"center",
  }

}));
