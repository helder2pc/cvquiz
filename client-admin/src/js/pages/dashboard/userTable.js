import React, { useEffect, useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Box from '@material-ui/core/Box';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Avatar from '@material-ui/core/Avatar';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import { Delete as DeleteIcon, Edit as EditIcon } from '@material-ui/icons';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { Modal } from '../../components/UI';
import AddUser from '../../components/AddUser/addUser';
import { connect } from 'react-redux';
import { User } from '../../redux/actions';
import Pagination from '@material-ui/lab/Pagination';
import { Loading, ConfirmDialog, TablePagination } from '../../components';
import { getUsersFilteredByPage, getCurrentPage, getPageItems } from '../../redux/selectors/users';
import { listItemsPerPage,  status as userStatus     } from "../../../utils/constants";
const UserTable = (props) => {

  const classes = useStyles();
  const [openModal, setOpenModal] = useState(false);
  const [status, setStatus] = useState('all');
  const [itemRemove, setItemRemove] = useState(null);
  const { users, loading, initUser, editUser, editUserClear,
    searchUser, totalPages, setPage, currentPage, itemPerPage, setItemPerPage } = props;

  useEffect(() => {
    initUser();
  }, [])


  const handleEditUser = (user) => {
    handleOpenModal();
    editUser(user);
  }

  const handleOpenModal = () => {
    editUserClear();
    setOpenModal(true);
  }

  return (
    <>
      <Loading loading={loading === true} />

      <Modal openModal={openModal} setOpenModal={setOpenModal}>
        <AddUser setOpenModal={setOpenModal} />
      </Modal>

      <ConfirmDialog
        title="Remover Utilizador"
        text="Quer mesmo remover o utilizador?"
        itemRemove={itemRemove}
        setItemRemove={setItemRemove}
        remove={props.removeUser} />

    <Box className={classes.header} >


          <div className={classes.inputQuestionWrapper}>

                <Paper component="form" className={classes.inputWrapper}>

                    <SearchIcon className={classes.iconSearch} />

                  <InputBase
                  
                    className={classes.input}
                    placeholder="Pesquisar.."
                    inputProps={{ 'aria-label': 'Pesquisar..' }}
                    onChange={(event) => searchUser(event.target.value)}
                    className={classes.input}
                  // value={searchText}
                  />

                </Paper>
          </div>

          <Select
                    value={status}
                    onChange={(event) => setStatus(event.target.value)}
                    displayEmpty
                    className={classes.selectStatus}
                    // className={classes.selectCat}
                    inputProps={{ 'aria-label': 'Without label' }}
                  >
                    <MenuItem value="" disabled>
                      ESTADO
                                </MenuItem>
                    {userStatus.map(({ id, title }) => <MenuItem key={id} value={id}>{title}</MenuItem>)}

              </Select>

        </Box>

      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead >
            <TableRow>

              <StyledTableCell className={classes.fieldId}>
                  <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      onClick={()=> props.setSortUser('_id')}
                      >
                      #id
                  </Button>
              </StyledTableCell>
              <StyledTableCell align="left">

              <Button 
                  size="small" 
                  className={classes.buttonFilter}
                  onClick={()=> props.setSortUser('first_name')}
                  >
                  Nome
              </Button>
              </StyledTableCell>

              <StyledTableCell align="left">
             
                  <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      onClick={()=> props.setSortUser('email')}
                      >
                      E-mail
                  </Button>

              </StyledTableCell>

              <StyledTableCell align="left">

              <Button 
                  size="small" 
                  className={classes.buttonFilter}
                  onClick={()=> props.setSortUser('username')}
                  >
                  Username
              </Button>
                
                </StyledTableCell>
              <StyledTableCell align="left">Status</StyledTableCell>


              <StyledTableCell className={classes.colBtns} align="right">
                <Fab
                  size="small" color="secondary" aria-label="add" onClick={() => handleOpenModal()}>
                  <AddIcon />
                </Fab>
              </StyledTableCell>

              {/* <StyledTableCell align="right">Protein&nbsp;(g)</StyledTableCell> */}
            </TableRow>
          </TableHead>
          <TableBody>

            {users.map((_user) => (
              <StyledTableRow key={_user._id}>
                <StyledTableCell style={{ width: 50 }} className={classes.colId} >
                  {_user._id}
                </StyledTableCell>

                <StyledTableCell align="left" >
                  <Box display="flex" alignItems="center" >
                    {/* <Avatar className={classes.purple}>{_user.first_name??[0] + '' + _user.first_name??[0]}</Avatar> */}
                    <Avatar className={classes.avatar} src={_user.photo && _user.photo.file}></Avatar>
                    {_user.first_name + ' ' + _user.last_name}
                  </Box>
                </StyledTableCell>

                <StyledTableCell className={classes.colCat} align="left">

                  {_user.email}
                </StyledTableCell>

                <StyledTableCell className={classes.colDifType} align="left">{_user.username}</StyledTableCell>

                <StyledTableCell className={classes.colDifType} align="left">active</StyledTableCell>


                <StyledTableCell className={classes.colBtns} align="right">

                  <IconButton
                    aria-label="edit"
                    onClick={() => handleEditUser(_user)}
                  >
                    <EditIcon />
                  </IconButton>

                  <IconButton
                    aria-label="delete"
                    onClick={() => setItemRemove(_user._id)}
                  >
                    <DeleteIcon />
                  </IconButton>




                </StyledTableCell>

                {/* <StyledTableCell align="right">{row.protein}</StyledTableCell> */}
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        length={users.length} 
        totalPages ={totalPages} 
        itemPerPage ={itemPerPage}
        setPage ={setPage} 
        setItemPerPage ={setItemPerPage}
       />
     

    </>
  );
}



const mapStateToProps = (state) => ({
 
  users: getUsersFilteredByPage(state),
  currentPage: getCurrentPage(state),
  itemPerPage: getPageItems(state),
  totalPages: state.User.totalPages,
  loading: state.User.loading,
})

export default connect(
  mapStateToProps,
  User
)(UserTable)

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },

}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

function createData(id, name, surname, category, difficult, status, protein) {
  return { id, name, category, surname, difficult, status, protein };
}

const useStyles = makeStyles((theme) => ({
  // const useStyles = makeStyles({

  header:{
    width:"100%",
    display:"flex",
    justifyContent:"space-between",
    marginBottom:15,
  },
    
  table: {
    position: "relative"
    // minWidth: 700,
  },

  tableHeader: {
    display: 'flex',
    alignItems: 'center',
    height: 40,
    backgroundColor: 'red',
    width: '100%',
  },

  inputWrapper: {
    width: '100%',
    paddingLeft: 10,
    paddingTop:5,
    paddingBottom:5,
  },

  iconSearch:{
      marginRight:8,
  },

  buttonFilter:{
    textAlign:"left",
    color: '#ffffff',
  },

  inputQuestionWrapper: {
    display: 'flex',
    alignItems: 'center',
    // justifyContent:'space-between', 
    fontWeight: 'bold',
     width:'100%',
  },
  input:{
      width:'100%',
  },

  fieldId: {
    fontWeight: 'bold',
    width: 50
  },
  select: {
    color: '#ffffff'
  },

  selectStatus: {
    backgroundColor: '#ffffff',
    width:220,
    marginLeft:20,
    padding:"5px 10px",
    borderRadius:5,
  },


  colCat: {
    // width: 300,
  },
  colId: {
    fontSize: 11
  },

  input: {
    width: '82%',
  },

  select: {
    marginLeft: 15,
  },

  avatar: {
    // color: theme.palette.getContrastText(deepPurple[500]),
    // backgroundColor: deepPurple[500],
    // color: theme.palette.primary.light,
    backgroundColor: theme.palette.primary.light,
    marginRight: 10,
  },

  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));
