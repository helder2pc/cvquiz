import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { category as listCategory } from '../../../utils/constants';




export default function RankingTable(props) {
  const classes = useStyles();
  const [category, setCategory] = React.useState('');

  return (

    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead >
          <TableRow>


            <StyledTableCell className={classes.fieldId}>#id</StyledTableCell>
            <StyledTableCell align="right">


              <Select
                value={category}
                onChange={(event) => setCategory(event.target.value)}
                displayEmpty
                className={classes.select}
                inputProps={{ 'aria-label': 'Without label' }}
              >
                <MenuItem value="" disabled>
                  Categorias
                        </MenuItem>
                {listCategory.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

              </Select>

              <div className={classes.inputQuestionWrapper}>

              </div>

            </StyledTableCell>

          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.id}>
              <StyledTableCell style={{ width: 50 }} className="firstRow" >
                {row.id}
              </StyledTableCell>

              <StyledTableCell align="left" >

                <Box display="flex" alignItems="center" >
                  <Avatar className={classes.purple}>{row.name[0] + '' + row.surname[0]}</Avatar>
                  {row.name + ' ' + row.surname}
                </Box>

              </StyledTableCell>

            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>


  );
}




const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },

}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

function createData(id, name, surname, category, difficult, status, protein) {
  return { id, name, category, surname, difficult, status, protein };
}

const rows = [
  createData('3422', 'Helder', 'Correia', 'heldercorreia@gmail.com', '@heldercorreia', 'active', 4.0),
  createData('3423', 'José', 'Paiva', 'josepaiva@hotmail.com', '@josepaiva', 'active', 4.3),
  createData('3424', 'Gibs', 'Correia', 'gibscorreia@gmail.com', '@gibscorreia', 'disabled', 6.0),
  createData('3425', 'Ana', 'Gomes', 'anagomes@cvq.com', '@anagomes', 'removed', 4.3),
  createData('3426', 'Carla', 'Reis', 'carlareis@cvq.com', '@carlareis', 'active', 3.9),
];

const useStyles = makeStyles((theme) => ({
  table: {
  },
  tableHeader: {
    display: 'flex',
    alignItems: 'center',
    height: 40,
    backgroundColor: 'red',
    width: '100%',
  },

  inputWrapper: {
    width: '100%',
    paddingLeft: 10,
  },

  inputQuestionWrapper: {
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
  },

  purple: {
    backgroundColor: theme.palette.primary.light,
    marginRight: 10,
  },

  fieldId: {
    fontWeight: 'bold',
    width: 50
  },

  select: {
    color: '#ffffff'
  },




}));
