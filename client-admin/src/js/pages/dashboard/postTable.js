import React, {useState,useEffect} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem'; 
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { type, difficult, category as listCategory } from '../../../utils/constants';
import { Modal } from '../../components/UI'
import { Delete as DeleteIcon, Edit as EditIcon } from '@material-ui/icons';
import { AddQuestion } from '../../components';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import AddPost from '../../components/addPost/addPost';
import { connect } from 'react-redux';
import { Post } from '../../redux/actions';
import { getPostsFilteredByPage, getCurrentPage, getPageItems } from '../../redux/selectors/post';
import {  status as postStatus } from "../../../utils/constants";
import { Loading, ConfirmDialog, TablePagination } from '../../components';



 function PostTable(props) {
  const classes = useStyles();
  const [category, setCategory] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const [itemRemove, setItemRemove] = useState(null);
  const [status, setStatus] = useState('all');



  const { posts, loading, initPost, editPost, editPostClear,
    searchPost, totalPages, setPage, currentPage, itemPerPage, setItemPerPage } = props;

  const defaultProps = {
    options: listCategory,
    getOptionLabel: (option) => option.title,
  };


  useEffect(() => {
    initPost();
  }, []);

  const handleEditPost = (_post) => {
    handleOpenModal();
    editPost(_post);
    console.log(_post);
  }

  const handleOpenModal = () => {
    editPostClear();
    setOpenModal(true);
  }

  return (

    <>

      <Loading loading={loading === true} /> 

      <ConfirmDialog
        title="Remover Post"
        text="Quer mesmo remover este Post?"
        itemRemove={itemRemove}
        setItemRemove={setItemRemove}
        remove={props.removePost} 
        />

      <Modal openModal={openModal} setOpenModal={setOpenModal}>
        <AddPost setOpenModal={setOpenModal} />
      </Modal>


      <Box className={classes.header} >
        <div className={classes.inputQuestionWrapper}>

          <Paper component="form" className={classes.inputWrapper}>

            <IconButton type="submit" className={classes.iconButton} aria-label="search">
              <SearchIcon />
            </IconButton>

            <InputBase

              className={classes.input}
              placeholder="Pesquisar.."
              inputProps={{ 'aria-label': 'Pesquisar..' }}
              onChange={(event) => searchPost(event.target.value)}
              className={classes.input}
            // value={searchText}
            />

          </Paper>
        </div>

        <Autocomplete
                  {...defaultProps}
                  id="author"
                  className={classes.selectStatus}
                  debug
                  renderInput={(params) => <TextField {...params} label="Autor"   className={classes.autoImput} />}
                />

          

            <Select
                  value={category}
                  onChange={(event) => setCategory(event.target.value)}
                  displayEmpty
                  className={classes.selectStatus}
                  inputProps={{ 'aria-label': 'Without label' }}
                >
                  <MenuItem value="" disabled>
                    Categorias
                        </MenuItem>
                  {listCategory.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

                </Select>

              <Select
                  value={category}
                  onChange={(event) => setCategory(event.target.value)}
                  displayEmpty
                  className={classes.selectStatus}
                  inputProps={{ 'aria-label': 'Without label' }}
                >
                  <MenuItem value="" disabled>
                    Data
                        </MenuItem>
                  {type.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

                </Select>

                <Select
                  value={status}
                  onChange={(event) => setStatus(event.target.value)}
                  displayEmpty
                  className={classes.selectStatus}
                  inputProps={{ 'aria-label': 'Without label' }}
                >
                  <MenuItem value="" disabled>
                    Estado
                        </MenuItem>
                  {postStatus.map(({ id, title }) => <MenuItem key={id} value={id}>{title}</MenuItem>)}
                </Select>



      </Box>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead >
            <TableRow>


              <StyledTableCell className={classes.fieldId}>
                  <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      onClick={()=> props.setSortPost('_id')}
                      >
                      #id
                  </Button>
              </StyledTableCell>
              <StyledTableCell align="left">
              <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      onClick={()=> props.setSortPost('title')}
                      >
                      Título
                  </Button>
             </StyledTableCell>

              <StyledTableCell align="left" className={classes.author}>

              <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      // onClick={()=> props.setSortPost('_id')}
                      >
                      Autor
                  </Button>

              </StyledTableCell>


              <StyledTableCell align="left">

              <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      // onClick={()=> props.setSortPost('_id')}
                      >
                      Categoria
                  </Button>

                

              </StyledTableCell>






              <StyledTableCell
                className={classes.colDate}
                align="left">

                  <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      // onClick={()=> props.setSortPost('_id')}
                      >
                      Autor
                  </Button>

               

              </StyledTableCell>

              <StyledTableCell className={classes.colBtns} align="right">
                <Fab
                  size="small" color="secondary" aria-label="add" onClick={() => setOpenModal(true)}>
                  <AddIcon />
                </Fab>
              </StyledTableCell>

            </TableRow>
          </TableHead>
          <TableBody>
            {posts.map((item) => (
              <StyledTableRow key={item._id}>
                <StyledTableCell style={{ width: 50 }} className="firstRow" >
                  {item._id}
                </StyledTableCell>

                <StyledTableCell align="left" >

                  <Box component="div" textOverflow="ellipsis">
                    {item.title}
                  </Box>
                </StyledTableCell>

                <StyledTableCell align="left">{''}</StyledTableCell>

                <StyledTableCell className={classes.colCat} align="left">{item.category_id}</StyledTableCell>

                <StyledTableCell className={classes.colDate} align="left">{''}</StyledTableCell>

                <StyledTableCell className={classes.colBtns} align="right">

                  <IconButton aria-label="edit" onClick={() => handleEditPost(item)} >
                    <EditIcon />
                  </IconButton>

                  <IconButton aria-label="delete"  onClick={() => setItemRemove(item._id)}>
                    <DeleteIcon />
                  </IconButton>

                </StyledTableCell>

              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        length={posts.length} 
        totalPages ={totalPages} 
        itemPerPage ={itemPerPage}
        setPage ={setPage} 
        setItemPerPage ={setItemPerPage}
       />

    </>
  );
}


const mapStateToProps = (state) => ({
 
  posts: getPostsFilteredByPage(state),
  currentPage: getCurrentPage(state),
  itemPerPage: getPageItems(state),
  totalPages: state.Post.totalPages,
  loading: state.Post.loading,
})

export default connect(
  mapStateToProps,
  Post
)(PostTable)



const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },

}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

function createData(id, question, category, type, difficult, protein) {
  return { id, question, category, type, difficult, protein };
}

const rows = [
  createData('3422', 'As sete maravilhas de Cabo Verde', 'Desporto', '20 jan de 2020', 'CVQuiz team', 4.0),
  createData('3423', 'A historia da banda Os Tubarões?', 'Arte', '23 dez de 2019', 'CVQuiz team', 4.3),
  createData('3424', 'A praia de Tarrafal santiago', 'Desporto', '12 fev de 2019', 'CVQuiz team', 6.0),
  createData('3425', 'Heróis nacionais de cabo verde', 'História', '24 jan de 2019', 'Pedro Gomes', 4.3),
  createData('3426', 'Carbeirinho – Tarrafal de São Nicolau', 'Geografia', '4 jan de 2019', 'Ana Rita', 3.9),
];

const useStyles = makeStyles({


  header: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 15,
  },


  table: {
    // minWidth: 700,
  },
  tableHeader: {
    display: 'flex',
    alignItems: 'center',
    height: 40,
    backgroundColor: 'red',
    width: '100%',
  },

  inputWrapper: {
    // maxWidth:250,
    width: '100%',
    paddingLeft: 10,
    // marginRight: -120,

  },

  inputQuestionWrapper: {
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
    width: '100%',
  },

  buttonFilter:{
    textAlign:"left",
    color: '#ffffff',
  },

  selectStatus: {
    backgroundColor: '#ffffff',
    marginLeft: 20,
    padding: "5px 10px",
    borderRadius: 5,
  },

  fieldId: {
    fontWeight: 'bold',
    width: 50
  },

  select: {
    color: '#ffffff'
  },

  colCat: {
    width: 100,
  },

  colDate: {
    width: 130,
  },

  author: {
    width: 160,
  },

  colBtns: {
    width: 130,
  },

  input: {
    width: '82%',
  },

  textEllipsis: {
    textOverflow: 'ellipsis'
  },
  autoImput: {
    marginTop: -12,
    color:"#ffffff"
  },

});
