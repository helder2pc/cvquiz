import React, { useState,useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Select from '@material-ui/core/Select';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Delete as DeleteIcon, Edit as EditIcon } from '@material-ui/icons';
import InputBase from '@material-ui/core/InputBase';
import Pagination from '@material-ui/lab/Pagination';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import AddCategory from '../../components/AddCategory/addCategory';
import { Modal } from '../../components/UI';
import { listItemsPerPage, status as categoryStatus } from "../../../utils/constants";
import { connect } from 'react-redux';
import { Category } from '../../redux/actions';
import { getCategoriesFilteredByPage, getCurrentPage, getPageItems } from '../../redux/selectors/category';
import { Loading, ConfirmDialog, TablePagination } from '../../components';


 function CategoryTable(props) {
  const classes = useStyles();
  const [openModal, setOpenModal] = React.useState(false);
  const [status, setStatus] = useState('all');
  const [itemRemove, setItemRemove] = useState(null);


  const { categories, loading, initCategory, editCategory, editCategoryClear,
    searchCategory, totalPages, setPage, currentPage, itemPerPage, setItemPerPage } = props;

    useEffect(() => {
      initCategory();
    }, []);

    const handleEditCategory = (cat) => {
      handleOpenModal();
      editCategory(cat);
      console.log(cat);
    }
  
    const handleOpenModal = () => {
      editCategoryClear();
      setOpenModal(true);
    }

  return (
    <>
      <Loading loading={loading === true} />

      <Modal openModal={openModal} setOpenModal={setOpenModal}>
        <AddCategory setOpenModal={setOpenModal} />
      </Modal>

      <ConfirmDialog
        title="Remover Categoria"
        text="Quer mesmo remover esta categoria?"
        itemRemove={itemRemove}
        setItemRemove={setItemRemove}
        remove={props.removeCategory} />

      <Box className={classes.header} >
        <div className={classes.inputQuestionWrapper}>

          <Paper component="form" className={classes.inputWrapper}>

            <IconButton type="submit" className={classes.iconButton} aria-label="search">
              <SearchIcon />
            </IconButton>

            <InputBase

              className={classes.input}
              placeholder="Pesquisar.."
              inputProps={{ 'aria-label': 'Pesquisar..' }}
              onChange={(event) => searchCategory(event.target.value)}
              className={classes.input}
            // value={searchText}
            />

          </Paper>
        </div>

        <Select
          value={status}
          onChange={(event) => setStatus(event.target.value)}
          displayEmpty
          className={classes.selectStatus}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem value="" disabled>
            Estado
                </MenuItem>
          {categoryStatus.map(({ id, title }) => <MenuItem key={id} value={id}>{title}</MenuItem>)}
        </Select>

      </Box>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead >
            <TableRow>

              <StyledTableCell className={classes.fieldId}>
              <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      onClick={()=> props.setSortCategory('_id')}
                      >
                      #id
                  </Button>
              </StyledTableCell>
              
              <StyledTableCell align="left" className={classes.columnName}>
                <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      onClick={()=> props.setSortCategory('name')}
                      >
                      Nome
                  </Button>
              </StyledTableCell>


              <StyledTableCell align="left" className={classes.columnDescription}>
                <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      // onClick={()=> props.setSortUser('_id')}
                      >
                      Descrição
                  </Button>
              </StyledTableCell>


              <StyledTableCell align="left" className={classes.columnStatus}>
                <Button 
                      size="small" 
                      className={classes.buttonFilter}
                      // onClick={()=> props.setSortUser('_id')}
                      >
                      Estado
                  </Button>
              </StyledTableCell>

              <StyledTableCell className={classes.colBtns} align="right">
                <Fab
                  size="small" color="secondary" aria-label="add" onClick={() => handleOpenModal()}>
                  <AddIcon />
                </Fab>
              </StyledTableCell>

              {/* <StyledTableCell align="right">Protein&nbsp;(g)</StyledTableCell> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {categories.map((cat) => (
              <StyledTableRow key={"category_"+cat._id}>
                <StyledTableCell style={{ width: 50 }} className="firstRow" >
                  {cat._id}
                </StyledTableCell>

                <StyledTableCell align="left" >{cat.name}</StyledTableCell>


                <StyledTableCell align="left" >{''}</StyledTableCell>


                <StyledTableCell align="left" >activo</StyledTableCell>


                <StyledTableCell className={classes.colBtns} align="right">

                  <IconButton 
                      aria-label="edit"
                      onClick={() => handleEditCategory(cat)}
                      >
                    <EditIcon />
                  </IconButton>

                  <IconButton 
                  aria-label="delete"
                  onClick={() => setItemRemove(cat._id)}
                  >
                    <DeleteIcon />
                  </IconButton>




                </StyledTableCell>

                {/* <StyledTableCell align="right">{row.protein}</StyledTableCell> */}
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        length={categories.length} 
        totalPages ={totalPages} 
        itemPerPage ={itemPerPage}
        setPage ={setPage} 
        setItemPerPage ={setItemPerPage}
       />

    </>
  );
}



 
const mapStateToProps = (state) => ({
 
  categories: getCategoriesFilteredByPage(state),
  currentPage: getCurrentPage(state),
  itemPerPage: getPageItems(state),
  totalPages: state.Category.totalPages,
  loading: state.Category.loading,
})

export default connect(
  mapStateToProps,
  Category
)(CategoryTable)



const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },

}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

function createData(id, question, category, type, difficult, protein) {
  return { id, question, category, type, difficult, protein };
}


const useStyles = makeStyles({

  header: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 15,
  },

  table: {
    // minWidth: 700,
  },
  tableHeader: {
    display: 'flex',
    alignItems: 'center',
    height: 40,
    backgroundColor: 'red',
    width: '100%',
  },

  inputWrapper: {
    width: '100%',
    paddingLeft: 10,

  },

  inputQuestionWrapper: {
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
    width: '100%',
  },

  buttonFilter:{
    textAlign:"left",
    color: '#ffffff',
  },

  selectStatus: {
    backgroundColor: '#ffffff',
    width: 220,
    marginLeft: 20,
    padding: "5px 10px",
    borderRadius: 5,
  },
  fieldId: {
    fontWeight: 'bold',
    width: 50
  },

  select: {
    color: '#ffffff'
  },

  columnName: {
    width: 200,
  },

  columnDescription: {
      width: '50%',
  },

  columnStatus: {
    width: 130,
  },
  colBtns:{
    width:350,
  },

  input: {
    width: '82%',
  },
});
