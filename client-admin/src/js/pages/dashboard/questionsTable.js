import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { type, difficult, category as listCategory, status as categoryStatus } from '../../../utils/constants';
import { Modal } from '../../components/UI'
import { Delete as DeleteIcon, Edit as EditIcon } from '@material-ui/icons';
import { AddQuestion } from '../../components';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { connect } from 'react-redux';
import { Question } from '../../redux/actions';
import { getQuestionsFilteredByPage, getCurrentPage, getPageItems } from '../../redux/selectors/question';
import { Loading, ConfirmDialog, TablePagination } from '../../components';



function QuestionsTable(props) {
  const classes = useStyles();
  const [category, setCategory] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const [status, setStatus] = useState('all');
  const [itemRemove, setItemRemove] = useState(null);

  const { questions, loading, initQuestion, editQuestion, editQuestionClear,
    searchQuestion, totalPages, setPage, currentPage, itemPerPage, setItemPerPage,
    difficultyFilter, typeFilter, setFilterQuestion } = props;

  useEffect(() => {
    initQuestion();
  }, []);

  const handleEditQuestion = (q) => {
    handleOpenModal();
    editQuestion(q);
  }

  const handleOpenModal = () => {
    editQuestionClear();
    setOpenModal(true);
  }



  return (

    <>

      <Loading loading={loading === true} />


      <Modal openModal={openModal} setOpenModal={setOpenModal}>
        <AddQuestion setOpenModal={setOpenModal} />
      </Modal>

      <ConfirmDialog
        title="Remover Pergunta"
        text="Quer mesmo remover esta pergunta?"
        itemRemove={itemRemove}
        setItemRemove={setItemRemove}
        remove={props.removeQuestion} />

      <Box className={classes.header} >
        <div className={classes.inputQuestionWrapper}>

          <Paper component="form" className={classes.inputWrapper}>

            <IconButton type="submit"
              className={classes.iconButton}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>

            <InputBase

              className={classes.input}
              placeholder="Pesquisar.."
              inputProps={{ 'aria-label': 'Pesquisar..' }}
              onChange={(event) => searchQuestion(event.target.value)}
              className={classes.input}
            // value={searchText}
            />

          </Paper>
        </div>




        <Select
          value={category}
          onChange={(event) => setCategory(event.target.value)}
          displayEmpty
          className={classes.selectStatus}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem value="" disabled>
            Categorias
                        </MenuItem>
          {listCategory.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

        </Select>


        <Select
          value={difficultyFilter}
          onChange={(event) => setFilterQuestion('difficulty', event.target.value)}
          displayEmpty
          className={classes.selectStatus}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem value="" disabled>Dificuldade</MenuItem>
          <MenuItem value={'all'}>{'Todos'}</MenuItem>
          {difficult.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

        </Select>

        <Select
          value={typeFilter}
          onChange={(event) => setFilterQuestion('type', event.target.value)}
          displayEmpty
          className={classes.selectStatus}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem value="" disabled>Tipo</MenuItem>
          <MenuItem value={'all'}>{'Todos'}</MenuItem>
          {type.map(item => <MenuItem value={item.id}>{item.title}</MenuItem>)}

        </Select>


        <Select
          value={status}
          onChange={(event) => setStatus(event.target.value)}
          displayEmpty
          className={classes.selectStatus}
          inputProps={{ 'aria-label': 'Without label' }}
        >
          <MenuItem value="" disabled>Estado</MenuItem>
          {categoryStatus.map(({ id, title }) => <MenuItem key={id} value={id}>{title}</MenuItem>)}
        </Select>


      </Box>


      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead >
            <TableRow>


              <StyledTableCell className={classes.fieldId}>
                <Button
                  size="small"
                  className={classes.buttonFilter}
                  onClick={()=> props.setSortQuestion('_id')}
                // onClick={()=> props.setSortUser('_id')}
                >
                  #id
                  </Button>
              </StyledTableCell>
              <StyledTableCell align="left">

                <Button
                  size="small"
                  className={classes.buttonFilter}
                  onClick={()=> props.setSortQuestion('question')}
                >
                  Pergunta
                  </Button>
              </StyledTableCell>

              <StyledTableCell align="left">

                <Button
                  size="small"
                  className={classes.buttonFilter}
                // onClick={()=> props.setSortUser('category')}
                >
                  Categorias
                  </Button>



              </StyledTableCell>


              <StyledTableCell align="left">

                <Button
                  size="small"
                  className={classes.buttonFilter}
                onClick={()=> props.setSortQuestion('difficulty')}
                >
                  Dificuldade
                  </Button>



              </StyledTableCell>



              <StyledTableCell
                className={classes.colDifType}
                align="left">

                <Button
                  size="small"
                  className={classes.buttonFilter}
                onClick={()=> props.setSortQuestion('type')}
                >
                  Tipo
                  </Button>


              </StyledTableCell>

              <StyledTableCell className={classes.colBtns} align="right">
                <Fab
                  size="small" color="secondary" aria-label="add" onClick={() => handleOpenModal()}>
                  <AddIcon />
                </Fab>
              </StyledTableCell>

            </TableRow>
          </TableHead>
          <TableBody>
            {questions.map((item) => (
              <StyledTableRow key={item._id}>
                <StyledTableCell style={{ width: 50 }} className="firstRow" >
                  {item._id}
                </StyledTableCell>

                <StyledTableCell align="left" >{item.question}</StyledTableCell>

                <StyledTableCell className={classes.colCat} align="left">{'-'}</StyledTableCell>

                <StyledTableCell className={classes.colDifType} align="left">{item.difficulty}</StyledTableCell>

                <StyledTableCell className={classes.colDifType} align="left">{item.type}</StyledTableCell>

                <StyledTableCell className={classes.colBtns} align="right">

                  <IconButton aria-label="edit" onClick={() => handleEditQuestion(item)}>
                    <EditIcon />
                  </IconButton>

                  <IconButton aria-label="delete" onClick={() => setItemRemove(item._id)}>
                    <DeleteIcon />
                  </IconButton>

                </StyledTableCell>

              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        length={questions.length}
        totalPages={totalPages}
        itemPerPage={itemPerPage}
        setPage={setPage}
        setItemPerPage={setItemPerPage}
      />
    </>
  );
}


const mapStateToProps = (state) => ({

  questions: getQuestionsFilteredByPage(state),
  currentPage: getCurrentPage(state),
  itemPerPage: getPageItems(state),
  totalPages: state.Question.totalPages,
  difficultyFilter: state.Question.difficulty,
  typeFilter: state.Question.type,
  loading: state.Question.loading,
})

export default connect(
  mapStateToProps,
  Question
)(QuestionsTable)


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },

}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

function createData(id, question, category, type, difficult, protein) {
  return { id, question, category, type, difficult, protein };
}

const rows = [
  createData('3422', 'Qual é a maior ilha do arquipélago de Cabo Verde?', 'Desporto', 'mcq_ma', 'Facíl', 4.0),
  createData('3423', 'Qual é o nome do primeiro álbum da banda Os Tubarões?', 'Arte', 'mcq', 'Fácil', 4.3),
  createData('3424', 'Em que ano Cabo Verde foi campeão africano de Andebol sub-21:', 'Desporto', 'mcq', 'Difícil', 6.0),
  createData('3425', 'Quando foi abolida a escravidão em Cabo Verde?', 'História', 'true_false', 'Difícil', 4.3),
  createData('3426', 'A superfície total do arquipélago de Cabo Verde é de:', 'Geografia', 'short_answer', 'Médio', 3.9),
];

const useStyles = makeStyles({

  header: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 15,
  },

  table: {
    // minWidth: 700,
  },
  tableHeader: {
    display: 'flex',
    alignItems: 'center',
    height: 40,
    backgroundColor: 'red',
    width: '100%',
  },

  inputWrapper: {
    width: '100%',
    paddingLeft: 10,

  },

  inputQuestionWrapper: {
    display: 'flex',
    alignItems: 'center',
    fontWeight: 'bold',
    width: '100%',
  },
  selectStatus: {
    // backgroundColor: '#ffffff',
    // width: 220,
    marginLeft: 20,
    padding: "5px 10px",
    borderRadius: 5,
  },

  fieldId: {
    fontWeight: 'bold',
    width: 50
  },

  buttonFilter: {
    textAlign: "left",
    color: '#ffffff',
  },

  select: {
    color: '#ffffff'
  },

  colCat: {
    width: 100,
  },

  colDifType: {
    width: 70,
  },

  colBtns: {
    width: 130,
  },

  input: {
    width: '82%',
  },
});
