import React from 'react';
import {Row,Col} from 'react-bootstrap'
import { Toolbar, ListUser, SidebarInfo, TimelinePublication } from '../../components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const feed = () => {

  const publications =[
    {key:1, name:'Helder Correia', iconPath:require('../../../assets/icon/historia.png')},
    {key:2, name:'Gibs Correia', iconPath:require('../../../assets/icon/geografia.png')},
    {key:3, name:'Pedro Gomes', iconPath:require('../../../assets/icon/desporto.png')}
  ]

 
  return (
    <div className="feed-page">
     
      <div className="container-cvq">
      <Row noGutters>
        <Col md={3} className="container-followed" > 
        {/* <div className="btns-menu">
          <button className="btn">
            <img src={require('../../../assets/icon/play.png')} />
            <span>Jogar</span>
          </button>
          <button className="btn">
            <img src={require('../../../assets/img/ranking.png')} />
            <span>Ranking</span>
          </button>

          <button className="btn">
            <img src={require('../../../assets/img/setting.png')} />
            <span>Definições</span>
          </button>
        </div> */}
              <ListUser></ListUser>
        </Col>
        <Col md={6} >
       
      
        <div className=" timeline-body col-md-12">
           
            <div className="timeline">
               
                <div className="line text-muted"></div>

               
                <article className="panel panel-danger panel-outline">
                  
                     <div className="panel-heading icon">
                        <i className="glyphicon glyphicon-plus-sign"></i>
                        <FontAwesomeIcon icon="plus-circle" /> 
                    </div>
                   
                    <div className="panel-body">
                        <strong>Gibs Correia</strong> começou a seguir-te.
                    </div>
                   

                </article>
               
               {publications.map( (item) => <TimelinePublication key={item.key} name={item.name} iconPath={item.iconPath}></TimelinePublication>)}
               

                <div className="separator text-muted">
                    <time>25. 3. 2015</time>
                </div>

                <div className="col-lg-12 text-center" >
                  
                        <button className="btn btn-success btn-xs"  >
                            Ver mais publicações
                        </button>
                  
                </div>

            </div>
           
           

        </div>


        </Col>
        <Col md={3}  >
          <SidebarInfo></SidebarInfo>
        </Col>
      </Row>
      </div>
    </div>
  );
}

export default feed;