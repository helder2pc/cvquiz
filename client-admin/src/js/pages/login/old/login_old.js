import React from 'react';
import {Row,Col,Tab, Tabs,} from 'react-bootstrap';
import { connect } from 'react-redux';
import {NavLink, withRouter} from 'react-router-dom';

import { LoginForm } from '../../components';
import { RegisterForm } from '../../components';

class login extends React.Component {

 render(){
  return (
    <div className="App-header">


    <div className="login-page">
      <Row>
    <Col md={6} sm={0} > 
    <div className="text-center login-title-div">
      <div className="login-logo-div-img ">
            
            <img src={require('../../../assets/img/logo.png')} />
            
        </div>

       <p>
               Testa bu kunhisimentu sobri nôs téra
       </p>
       </div>
    </Col>
    <Col  md={6} sm={12}>
    <div className="text-right">
     
     
        <div  className="form" > 
      {/* onSubmit={this.handleSubmit} */}
            
          

            <Tabs defaultActiveKey="login" id="uncontrolled-tab-example" className="border-0">
              <Tab eventKey="login" title="Login" className="border-0">
                
              <LoginForm/> 
              
                 {/*  <p className="text-or">or</p> */}
             </Tab>
             <Tab eventKey="register" title="Registar" >
                <RegisterForm/> 
            </Tab>

            <Tab eventKey="forget_password" >
                <RegisterForm/> 
            </Tab>
          </Tabs>

        {/*<button className="btn-facebook btn-login-rsocias"></button>*/}

      </div>
     

      </div>
    </Col>
  </Row>
 
    </div>

    </div>
  );
 }
}


 //export default login;

//  function mapStateToProps({Auth}) {
//   const { login_user } = Auth;
//   return {
//     login_user
//   };
// }

// const connectedLogin = withRouter(connect(mapStateToProps)(login)); 
// export default {connectedLogin};

export default connect(
  store => ({ login_user: store.Auth.login_user }),
  //{ increase, decrease }
)(login)

