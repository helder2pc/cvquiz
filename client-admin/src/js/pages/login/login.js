import React, {useState} from 'react';
import { connect } from 'react-redux';
import {  makeStyles } from '@material-ui/core/styles';
import { Box,Button,TextField, Typography,CircularProgress } from '@material-ui/core';
import ForgotPassword from './forgotPassword';
import {Auth} from '../../redux/actions';
import { Spinner } from '../../components';


  const Login = (props) => {
  const classes = useStyles();

  const [open, setOpen] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  const handleClickOpen = (event) => {
    event.preventDefault(); // Let's stop this event.
    event.stopPropagation(); // Really this time.
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit =(event) =>{
    // alert('An essay was submitted: ' + email + " "+ password);
    event.preventDefault();
    props.auth(email, password, false);
  }

  return (
    <Box className={classes.loginWrapper}>
      {/* crete a component for this */}
     {props.loading && <Box className={classes.loginLoad}>
          {/* <CircularProgress   color="secondary"/> */}
          <Spinner></Spinner>
      </Box>
      }
 
      <ForgotPassword open={open} handleClose={handleClose}/>

      <Box className={classes.container}>

      <form  
          className={classes.form} 
          noValidate 
          autoComplete="off" 
          onSubmit={handleSubmit}
          >

      <Typography variant="h4" >
       Dashboard
      </Typography>
         <TextField  
            className={classes.input} 
            variant="outlined" 
            label="Username"  
            onChange={(event)=>setEmail(event.target.value)}
            />
         <TextField  
            className={classes.input} 
            variant="outlined" 
            label="Password"  
            type="password"
            onChange={(event)=>setPassword(event.target.value)}
            />
         
         <Button 
          className={classes.btnLogin} 
          variant="contained" 
          color="secondary"
          type="submit"
          // component={ Link } 
          // to="/dashboard"
          >
                Login
          </Button>
          
          <a className={classes.btnforgotPassword} href="#text-buttons"  onClick={handleClickOpen} color="primary">
          Esqueceste-te da tua conta?
          </a>
      
    </form>
      
      </Box>

    </Box>
  );
 
}



const useStyles = makeStyles((theme) => ({
  root: {
    // minWidth: 700,
  },

  loginWrapper:{
    width:'100vw',
    height:'100vh',
    backgroundImage: `linear-gradient(${theme.palette.primary.dark}, ${theme.palette.primary.light})`,
    
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
  },

  container:{
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
    // alignItems:'center',
    maxWidth:500,
    minHeight:350,
    width:'100%',
    height:300,
    marginRight:10,
    marginLeft:10,
    backgroundColor:'#fff',
    padding:20,
   
  },

  input:{
    width:'100%',
    marginTop:15,
  },

  form:{
      textAlign:"center",
  },

  btnLogin:{
    padding:10,
    width:150,
    textAlign:'center',
    marginTop:20,
    marginLeft:'auto',
    marginRight:'auto',
  },

  btnforgotPassword:{
    marginTop:20,
    display:"block",
    color:theme.palette.primary.light,
    textDecoration: 'none'
  },
 
  loginLoad:{
    position:"absolute",
    top:0,
    left:0,
    bottom:0,
    right:0,
    backgroundColor:"rgba(255,255,255,0.3)",
    zIndex:2,
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
  },
}));



const mapStateToProps = ({Auth}) => ({
  loading: Auth.loading,
  userId: Auth.userId,
})

export default connect(
  mapStateToProps,
  Auth
)(Login)
