import React from 'react';
import { connect } from 'react-redux';
import {  makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { Button,TextField,  } from '@material-ui/core';


 const ForgotPassword = (props) => {
  const classes = useStyles();

  const {open, handleClose} =props;
  
  return (
  


<Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Recuperar palavra-passe</DialogTitle>
        <DialogContent>
          <DialogContentText>
          Sed cursus turpis vitae tortor. Vestibulum suscipit nulla quis orci. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. 
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Email Address"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleClose} color="primary">
            Enviar
          </Button>
        </DialogActions>
      </Dialog>


  );
 
}



const useStyles = makeStyles((theme) => ({
  root: {
    // minWidth: 700,
  },

  
 
}));



export default connect(
  store => ({ login_user: store.Auth.login_user }),
  //{ increase, decrease }
)(ForgotPassword)

