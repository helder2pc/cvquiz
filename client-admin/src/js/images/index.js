//import user from '../../../assets/img/peter.jpg'


export default {
  user:require('../../assets/img/peter.jpg'),
  logo:require('../../assets/img/logo.png'),
  categories:{
    art:require('../../assets/icon/art.png'),
    random:require('../../assets/icon/aleatorio.png'),
    sport:require('../../assets/icon/desporto.png'),
    geography:require('../../assets/icon/geografia.png'),
    history:require('../../assets/icon/historia.png'),
    tour:require('../../assets/icon/tour.png'),
  }
 
}