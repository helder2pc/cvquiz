import { createStore, applyMiddleware, compose } from 'redux'
//import logger from 'redux-logger'
import {connectRouter, routerMiddleware} from 'connected-react-router'
import thunk from 'redux-thunk';
import createSagaMiddleware from "redux-saga";
import reducer from '../reducers'
import history from '../../../routes/history'
import { watchAuth, watchUser, watchHandleError, watchCategory,
          watchQuestion, watchPost} from "../effects";

// import { routerMiddleware } from 'connected-react-router';

const sagaMiddleware = createSagaMiddleware();

const initialState = {}
const enhancers = []
const middleware = [
  thunk,sagaMiddleware, routerMiddleware(history)
]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)


const store = createStore(
  reducer,
  initialState,
  composedEnhancers,
)

sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchUser);
sagaMiddleware.run(watchCategory);
sagaMiddleware.run(watchQuestion);
sagaMiddleware.run(watchPost);
sagaMiddleware.run(watchHandleError);

export default store