
import { put, call } from "redux-saga/effects";
import axios from '../../../utils/axios-cvq';
import {
    Question as actions,
    HandleError as actionsError,
} from "../actions";
import { toastr } from 'react-redux-toastr';
import { arrayToObject } from "../../../utils/functionAux";

export function* initQuestionSaga(action) {

    try {
        const response = yield axios.get('questions');

        yield put(
            actions.setQuestion(arrayToObject(response.data))
        );

    } catch (error) {
        yield put(actionsError.handleError(error.response));
    }
}

export function* addQuestionSaga(action) {

    try {

        const data = {
            "question": action.questionForm.question,
            "description": "Cras varius. Donec vitae orci sed dolor rutrum auctor. Donec mollis hendrerit risus.",
            "difficulty": action.questionForm.difficulty,
            "type": action.questionForm.type,
        }
        const response = yield axios.post('questions', data);

        yield put(
            actions.addQuestionSuccessful(response.data)
        );

        toastr.success('Adiconado', 'Pergunta adiconado com sucesso!');

    } catch (error) {
        yield put(actionsError.handleError(error.response));
        //toastr.error('Erro','Erro ao adiconar utilizador!',error.response.data.message);
    }
}


export function* editQuestionSaga(action) {

    try {

        const data = {
            "question": action.questionForm.question,
            "description": "Cras varius. Donec vitae orci sed dolor rutrum auctor. Donec mollis hendrerit risus.",
            "difficulty": action.questionForm.difficulty,
            "type": action.questionForm.type,
        }
        const response = yield axios.put('questions/' + action.questionForm._id, data);
        yield put(
            actions.updateQuestion(response.data)
        );
        toastr.success('Editado', 'Pergunta atualizada com sucesso!');

    } catch (error) {

        yield put(actionsError.handleError(error.response));
    }
}



export function* removeQuestionSaga(action) {

    try {

        yield axios.delete('questions/' + action.id);

        yield put(
            actions.removeQuestionSuccessful(action.id)
        );

        toastr.success('Remover', 'Pergunta removido com sucesso!');

    } catch (error) {

        yield put(actionsError.handleError(error.response));
    }
}


