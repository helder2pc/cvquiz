
import { put, call } from "redux-saga/effects";
import axios from '../../../utils/axios-cvq';
import {
    Category as actions, 
    HandleError as actionsError,
  } from "../actions";
import { toastr } from 'react-redux-toastr';
import { arrayToObject } from "../../../utils/functionAux";

export function* initCategorySaga(action) {

  try {
    const response = yield axios.get('categories');
    
    yield put(
      actions.setCategory(arrayToObject(response.data))
    );
   
  } catch (error) {
    yield put(actionsError.handleError(error.response));
  }
}

export function* addCategorySaga(action) {

    try {

      const response =  yield axios.post('categories', action.categoryForm);
      
      yield put(
        actions.addCategorySuccessful(response.data)
  );
      
      toastr.success('Adiconado','Categoria adiconado com sucesso!');
     
    } catch (error) {
      yield put(actionsError.handleError(error.response));
      //toastr.error('Erro','Erro ao adiconar utilizador!',error.response.data.message);
    }
  }


export function* editCategorySaga(action) {

  try {


    const data={
      name : action.categoryForm.name,
      description : action.categoryForm.description
    }

    const response = yield axios.put('categories/'+action.categoryForm._id, data);
    yield put(
          actions.updateCategory(response.data)
    );
    toastr.success('Editado','Categoria atualizada com sucesso!');
   
  } catch (error) {
    
    yield put(actionsError.handleError(error.response));
  }
}
  


  export function* removeCategorySaga(action) {

    try {

      let url = 'categories/'+action.id;
      const response = yield axios.delete(url);

      yield put(
            actions.removeCategorySuccessful(action.id)
      );

      toastr.success('Remover','Categoria removido com sucesso!');
     
    } catch (error) {
     
      yield put(actionsError.handleError(error.response));
    }
  }


