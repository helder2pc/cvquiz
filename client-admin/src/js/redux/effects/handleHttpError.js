import { put, call } from "redux-saga/effects";
import { toastr } from 'react-redux-toastr';
import {Auth as actions} from "../actions";
import {Account as actionsAccount} from "../actions";

export function* handleHttpErrorSaga(error) {
    // alert(error.response)
    // console.log(error.response)
   
    if(error.response && error.response.status === 404)
        toastr.error('Error','Url not found');
    else
        toastr.error(error.response.data.error,error.response.data.message);
    
    if(error.response && error.response.status === 401){
        yield call([localStorage, "removeItem"], "token");
        yield call([localStorage, "removeItem"], "user");
        yield call([localStorage, "removeItem"], "userId");
        yield put(actions.logoutSucceed());
        yield put(actionsAccount.clearAccount());
    }
    
  } 