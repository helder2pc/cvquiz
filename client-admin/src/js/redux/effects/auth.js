import { delay } from "redux-saga";
import { put, call } from "redux-saga/effects";
// import axios from "axios";
 import axios from '../../../utils/axios-cvq';

import {Auth as actions} from "../actions";
import {Account as actionsAccount} from "../actions";

export function* logoutSaga(action) {
  yield call([localStorage, "removeItem"], "token");
  yield call([localStorage, "removeItem"], "user");
  yield call([localStorage, "removeItem"], "userId");
  yield put(actions.logoutSucceed());
  yield put(actionsAccount.clearAccount());
} 

export function* checkAuthTimeoutSaga(action) {
//   yield delay(action.expirationTime * 1000);
  yield put(actions.logout());
}

export function* authUserSaga(action) {
  yield put(actions.authStart());

  const authData = {
    username: action.email,
    password: action.password,
    strategy: "local",
    // returnSecureToken: true
  };
  let url =
    "users";
  if (!action.isSignup) {
    url =
      "auth/login";
  }
  try {
    const response = yield axios.post(url, authData);

    yield localStorage.setItem("token", response.data.token);
    yield localStorage.setItem("user", JSON.stringify(response.data.user));
    yield localStorage.setItem("userId", response.data.user._id);
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
   
    yield put(
      actions.authSuccess(response.data.token, response.data.user._id)
    );
    yield put(
      actionsAccount.setAccount(response.data.user)
    );

  } catch (error) {
   
    yield put(actions.authFail(error.response.data.error));
  }
}

export function* authCheckStateSaga(action) {
  // alert("auth check")
  yield put(actions.authStart());
  const token = yield localStorage.getItem("token");
  if (!token) {
    yield put(actions.logout());
    yield put(actionsAccount.clearAccount());
  } else {
      const userId = yield localStorage.getItem("userId");
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
      yield put(actions.authSuccess(token, userId));
      

      const user= yield localStorage.getItem("user");
      if(user){
        yield put(
          actionsAccount.setAccount(JSON.parse(user))
        );

      }
     
  }
}
