
import { put, call } from "redux-saga/effects";
import axios from '../../../utils/axios-cvq';
import {
    Post as actions, 
    HandleError as actionsError,
  } from "../actions";
import { toastr } from 'react-redux-toastr';
import { arrayToObject } from "../../../utils/functionAux";

export function* initPostSaga(action) {

  try {
    const response = yield axios.get('posts');
    
    yield put(
      actions.setPost(arrayToObject(response.data))
    );
   
  } catch (error) {
    yield put(actionsError.handleError(error.response));
  }
}

export function* addPostSaga(action) {

    try {

      const dataForm={
        
        title: action.postForm.title,
        summary: action.postForm.summary,
        content: action.postForm.content,
        category_id: "5eada16ee57e9b002e071e26",
        publisher_id: "5ea431a0336ed6002ed8184d",
        // tags:[],
        // language:'pt'
    }
    console.log("dataForm 4545")
    console.log(dataForm)
      const response =  yield axios.post('posts', dataForm);
      
      yield put(
        actions.addPostSuccessful(response.data)
  );
      
      toastr.success('Adiconado','Post adiconado com sucesso!');
     
    } catch (error) {
      yield put(actionsError.handleError(error.response));
      //toastr.error('Erro','Erro ao adiconar utilizador!',error.response.data.message);
    }
  }


export function* editPostSaga(action) {

  try {


    const data={
        
      title: action.postForm.title,
      summary: action.postForm.summary,
      content: action.postForm.content,
      category_id: "5eada16ee57e9b002e071e26",
      publisher_id: "5ea431a0336ed6002ed8184d",
      // tags:[],
      // language:'pt'
  }

    const response = yield axios.put('posts/'+action.postForm._id, data);
    yield put(
          actions.updatePost(response.data)
    );
    toastr.success('Editado','Categoria atualizada com sucesso!');
   
  } catch (error) {
    
    yield put(actionsError.handleError(error.response));
  }
}
  


  export function* removePostSaga(action) {

    try {

      let url = 'posts/'+action.id;
      const response = yield axios.delete(url);

      yield put(
            actions.removePostSuccessful(action.id)
      );

      toastr.success('Remover','Categoria removido com sucesso!');
     
    } catch (error) {
     
      yield put(actionsError.handleError(error.response));
    }
  }


