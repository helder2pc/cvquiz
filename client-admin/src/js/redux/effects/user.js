
import { put, call } from "redux-saga/effects";
import axios from '../../../utils/axios-cvq';
import {
    User as actions, 
    Auth as actionsAuth, 
    Account as actionsAccount,
    HandleError as actionsError,
  } from "../actions";
import { toastr } from 'react-redux-toastr';
import { arrayToObject } from "../../../utils/functionAux";

export function* initUserSaga(action) {

  try {
    const response = yield axios.get('users?relations=photo');
    
    yield put(
      actions.setUser(arrayToObject(response.data))
    );
   
  } catch (error) {
    yield put(actionsError.handleError(error.response));
  }
}

export function* addUserSaga(action) {

    try {
      const response = yield axios.post('users', action.userForm);
      yield put(
            actions.addUserSuccessful(response.data)
      );
      toastr.success('Adiconado','Utilizador adiconado com sucesso!');
     
    } catch (error) {
      yield put(actionsError.handleError(error.response));
      //toastr.error('Erro','Erro ao adiconar utilizador!',error.response.data.message);
    }
  }


export function* editUserSaga(action) {

  try {
  
    if(action.userForm.photo){
  
      const dataMedia={
        "name": "nome",
        "type": "image",
        "file": action.userForm.photo
      }
      const responseMedia = yield axios.post('medias', dataMedia);
      action.userForm.photo = responseMedia.data._id;

      console.log(responseMedia.data._id);
    }
    
    const response = yield axios.put('users/'+action.userForm._id, action.userForm);
    yield put(
          actions.updateUser(response.data)
    );
    toastr.success('Editado','Utilizador editado com sucesso!');
   
  } catch (error) {
    
    yield put(actionsError.handleError(error.response));
    // toastr.error('Erro','Erro ao editar o utilizador!',error.response.data.message);
  }
}
  


  export function* removeUserSaga(action) {

    try {

      let url = 'users/'+action.id;
      const response = yield axios.delete(url);

      yield put(
            actions.removeUserSuccessful(action.id)
      );

      toastr.success('Removido','Utilizador removido com sucesso!');
     
    } catch (error) {
     
      yield put(actionsError.handleError(error.response));
      //toastr.error('Erro','Erro ao remover utilizador!',error.response.data.message);
    }
  }


