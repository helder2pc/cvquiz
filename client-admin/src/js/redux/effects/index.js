import { takeEvery, all, takeLatest } from "redux-saga/effects";

import * as actionTypes from "../constants";
import {
  logoutSaga,
  authUserSaga,
  authCheckStateSaga
} from "./auth";


import {
  initUserSaga,
  addUserSaga,
  removeUserSaga,
  editUserSaga,
} from "./user";

import {
  initCategorySaga,
  addCategorySaga,
  removeCategorySaga,
  editCategorySaga,
} from "./category";


import {
  initQuestionSaga,
  addQuestionSaga,
  removeQuestionSaga,
  editQuestionSaga,
} from "./question";

import {
  initPostSaga,
  addPostSaga,
  removePostSaga,
  editPostSaga,
} from "./post";

import {
  handleHttpErrorSaga
} from "./handleHttpError";

export function* watchAuth() {

  yield all([
    takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),
    takeEvery(actionTypes.AUTH_USER, authUserSaga),
    takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga),
  ]);
}


export function* watchUser() {

  yield all([
    takeEvery(actionTypes.USER_INIT, initUserSaga),
    takeEvery(actionTypes.USER_ADD_START, addUserSaga),
    takeEvery(actionTypes.USER_REMOVE_START, removeUserSaga),
    takeEvery(actionTypes.USER_UPDATE_START, editUserSaga),
  ]);

}


export function* watchCategory() {

  yield all([
    takeEvery(actionTypes.CATEGORY_INIT, initCategorySaga),
    takeEvery(actionTypes.CATEGORY_ADD_START, addCategorySaga),
    takeEvery(actionTypes.CATEGORY_REMOVE_START, removeCategorySaga),
    takeEvery(actionTypes.CATEGORY_UPDATE_START, editCategorySaga),
  ]);

}

export function* watchQuestion() {

  yield all([
    takeEvery(actionTypes.QUESTION_INIT, initQuestionSaga),
    takeEvery(actionTypes.QUESTION_ADD_START, addQuestionSaga),
    takeEvery(actionTypes.QUESTION_REMOVE_START, removeQuestionSaga),
    takeEvery(actionTypes.QUESTION_UPDATE_START, editQuestionSaga),
  ]);

}

export function* watchPost() {

  yield all([
    takeEvery(actionTypes.POST_INIT, initPostSaga),
    takeEvery(actionTypes.POST_ADD_START, addPostSaga),
    takeEvery(actionTypes.POST_REMOVE_START, removePostSaga),
    takeEvery(actionTypes.POST_UPDATE_START, editPostSaga),
  ]);

}

export function* watchHandleError() {

  yield all([
    takeEvery(actionTypes.SET_ERROR, handleHttpErrorSaga),
  ]);
}