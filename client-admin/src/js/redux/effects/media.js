
import { put, call } from "redux-saga/effects";
import axios from '../../../utils/axios-cvq';
import {
    User as actions, 
    Auth as actionsAuth, 
    Account as actionsAccount,
    HandleError as actionsError,
  } from "../actions";
import { toastr } from 'react-redux-toastr';
import { arrayToObject } from "../../../utils/functionAux";

//not used
export function* uploadMediaSaga(action) {

  try {
    const response = yield axios.get('users');
   
    yield put(
      actions.setUser(arrayToObject(response.data))
    );
   
  } catch (error) {
    yield put(actionsError.handleError(error.response));
  }
}
