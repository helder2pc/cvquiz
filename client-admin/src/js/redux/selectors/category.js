import { createSelector } from 'reselect'
import { compareValues } from '../../../utils/functionAux';

export const getCategories = (state) => Object.values(state.Category.categories)

export const searchText = state => state.Category.searchText;

export const getTotalItems = state => state.Category.totalItems;

export const getPageItems = state => state.Category.pageItems;

export const getTotalPages = state => state.Category.totalPages;

export const getCurrentPage = state => state.Category.currentPage;

export const getCategorySort = state => state.Category.sort;


export const getCategoriesFilteredByPage =  state => { 
  const page_number = getCurrentPage(state);
  const page_size = getPageItems(state);
  const categories = getCategoriesFilteredByKeyword2(state)
  return categories.slice((page_number - 1) * page_size, page_number * page_size)
}

export const getCategoriesSort = createSelector(
  [getCategories, getCategorySort],
  (listCategories, sort) =>{
    if(!sort.column){
      return listCategories;
    }

    return listCategories.sort(compareValues(sort.column, sort.order ))
  }
    
)



export const getCategoriesFilteredByKeyword2 = createSelector(

  [getCategoriesSort, searchText],
  (listCategories, keyword) =>
    listCategories.filter(category => 
        category.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1 
        || category._id.indexOf(keyword) > -1
        // || category.last_name.indexOf(keyword) > -1
        
        )
)



