import { createSelector } from 'reselect'
import { compareValues } from '../../../utils/functionAux';

export const getQuestions = (state) => Object.values(state.Question.questions)

export const searchText = state => state.Question.searchText;

export const getFilterDifficulty = state => state.Question.difficulty;

export const getFilterType = state => state.Question.type;

export const getTotalItems = state => state.Question.totalItems;

export const getPageItems = state => state.Question.pageItems;

export const getTotalPages = state => state.Question.totalPages;

export const getCurrentPage = state => state.Question.currentPage;

export const getQuestionSort = state => state.Question.sort;


export const getQuestionsFilteredByPage = state => {
  const page_number = getCurrentPage(state);
  const page_size = getPageItems(state);
  const questions = getQuestionsFilteredByKeyword2(state)
  return questions.slice((page_number - 1) * page_size, page_number * page_size)
}

export const getQuestionsSort = createSelector(
  [getQuestions, getQuestionSort],
  (listQuestions, sort) => {
    if (!sort.column) {
      return listQuestions;
    }
    return listQuestions.sort(compareValues(sort.column, sort.order))
  }

)

export const getQuestionsFilteredByDifficulty = createSelector(

  [getQuestionsSort, getFilterDifficulty],
  (listQuestions, difficulty) => {
    if (difficulty === '' || difficulty === 'all')
      return listQuestions

    return listQuestions.filter(question =>
      question.difficulty === difficulty
    )
  }
)


export const getQuestionsFilteredByType = createSelector(

  [getQuestionsSort, getFilterType],
  (listQuestions, type) => {
    if (type === '' || type === 'all')
      return listQuestions

    return listQuestions.filter(question =>
      question.difficulty === type
    )
  }
)

export const getQuestionsFilteredByKeyword2 = createSelector(

  [getQuestionsFilteredByDifficulty, searchText],
  (listQuestions, keyword) =>
    listQuestions.filter(question =>
      question.question.toLowerCase().indexOf(keyword.toLowerCase()) > -1
      || question._id.indexOf(keyword) > -1
      // || question.last_name.indexOf(keyword) > -1

    )
)



