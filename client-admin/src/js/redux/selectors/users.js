import { createSelector } from 'reselect'
import { compareValues } from '../../../utils/functionAux';

export const getUsers = (state) => Object.values(state.User.users)

export const searchText = state => state.User.searchText;

export const getTotalItems = state => state.User.totalItems;

export const getPageItems = state => state.User.pageItems;

export const getTotalPages = state => state.User.totalPages;

export const getCurrentPage = state => state.User.currentPage;

export const getUserSort = state => state.User.sort;


export const getUsersFilteredByPage =  state => { 
  const page_number = getCurrentPage(state);
  const page_size = getPageItems(state);
  const users = getUsersFilteredByKeyword2(state)
  return users.slice((page_number - 1) * page_size, page_number * page_size)
}

export const getUsersSort = createSelector(
  [getUsers, getUserSort],
  (listUsers, sort) =>{
    if(!sort.column){
      return listUsers;
    }

    return listUsers.sort(compareValues(sort.column, sort.order ))
  }
    
)

// export const getUsers = (state) => Object.values(state.User.users).sort((a, b) => a.first_name - b.first_name);

// export const getUsersFilteredByKeyword = createSelector(
//   [getUsers, getCurrentPage],
//   (listUsers, page_number) =>
//     {
     
//       const page_size = getPageItems(state);
//       const users = getUsersFilteredByKeyword2(state)
//       return users.slice((page_number - 1) * page_size, page_number * page_size)
//     }
// )


export const getUsersFilteredByKeyword2 = createSelector(
  // [getUsers, searchText],
  [getUsersSort, searchText],
  (listUsers, keyword) =>
    listUsers.filter(user => 
      (user.first_name && user.first_name.toLowerCase().indexOf(keyword.toLowerCase()) > -1 )
        || (user.last_name && user.last_name.toLowerCase().indexOf(keyword.toLowerCase()) > -1)
        || (user.username.toLowerCase().indexOf(keyword.toLowerCase()) > -1)
        || user.email.toLowerCase().indexOf(keyword.toLowerCase()) > -1
        || user._id.indexOf(keyword) > -1)
)



