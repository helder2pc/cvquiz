import { createSelector } from 'reselect'
import { compareValues } from '../../../utils/functionAux';

export const getPosts = (state) => Object.values(state.Post.posts)

export const searchText = state => state.Post.searchText;

export const getTotalItems = state => state.Post.totalItems;

export const getPageItems = state => state.Post.pageItems;

export const getTotalPages = state => state.Post.totalPages;

export const getCurrentPage = state => state.Post.currentPage;

export const getPostSort = state => state.Post.sort;


export const getPostsFilteredByPage =  state => { 
  const page_number = getCurrentPage(state);
  const page_size = getPageItems(state);
  const posts = getPostsFilteredByKeyword2(state)
  return posts.slice((page_number - 1) * page_size, page_number * page_size)
}

export const getPostsSort = createSelector(
  [getPosts, getPostSort],
  (listPosts, sort) =>{
    if(!sort.column){
      return listPosts;
    }

    return listPosts.sort(compareValues(sort.column, sort.order ))
  }
    
)


export const getPostsFilteredByKeyword2 = createSelector(

  [getPostsSort, searchText],
  (listPosts, keyword) =>
    listPosts.filter(post => 
        post.title.toLowerCase().indexOf(keyword.toLowerCase()) > -1 ||
         post._id.indexOf(keyword) > -1
        // || post.last_name.indexOf(keyword) > -1
        
        )
)



