import * as actionTypes from '../constants';
const initialState = {
    questions: [],
    questionForm: null,
    error: null,
    loading: false,
    questionToEdit: null,
    searchText: '',
    difficulty: '',
    type: '',

    totalPages: 0,
    currentPage: 1,
    pageItems: 20,
    totalItems: 0,

    sort: {
        column: null,
        order: null,
    },
}


export default (state = initialState, { type, payload }) => {
    switch (type) {

        case actionTypes.QUESTION_START:
            return { ...state, ...payload }

        case actionTypes.QUESTION_INIT:
            return { ...state, loading: true }

        case actionTypes.QUESTION_SET:
            const total = Object.keys(payload.questions).length;
            return { ...state, ...payload, totalItems: total, totalPages: Math.ceil(total / state.pageItems) }

        case actionTypes.QUESTION_INIT_FAIL:
            return { ...state, ...payload }

        case actionTypes.QUESTION_ADD_START:
            return { ...state, ...payload }

        case actionTypes.QUESTION_ADD_SUCCESSFUL:
            return {
                ...state, questions: { [payload._id]: payload, ...state.questions },
                totalItems: state.totalItems + 1, totalPages: Math.ceil((state.totalItems + 1) / state.pageItems),
                loading: false
            }

        case actionTypes.QUESTION_EDIT:

            return { ...state, ...payload }

        case actionTypes.QUESTION_EDIT_CLEAR:
            return { ...state, questionToEdit: null }

        case actionTypes.QUESTION_REMOVE_START:
            return { ...state, loading: false }

        case actionTypes.QUESTION_REMOVE_SUCCESSFUL:
            delete state.questions[payload.id]


            return {
                ...state, loading: false,
                totalItems: state.totalItems - 1, totalPages: Math.ceil((state.totalItems - 1) / state.pageItems),
            }

        case actionTypes.QUESTION_UPDATE_START:
            return { ...state, loading: true }

        case actionTypes.QUESTION_UPDATE:
            return { ...state, questions: { ...state.questions, [payload.question._id]: payload.question }, loading: false }

        case actionTypes.SET_ERROR:
            return { ...state, loading: false, }

        case actionTypes.QUESTION_SEARCH:
            return { ...state, searchText: payload, }

        case actionTypes.QUESTION_SET_PAGE:
            return { ...state, ...payload, }

        case actionTypes.SET_ITEM_PER_PAGE:
            return {
                ...state, ...payload,
                totalPages: Math.ceil((state.totalItems + 1) / payload.pageItems),
            }

        case actionTypes.QUESTION_SORT:

            let order = 'asc';

            if (payload.sort.column === state.sort.column) {
                if (state.sort.order === 'asc')
                    order = 'desc';

            }

            return {
                ...state,
                sort: {
                    column: payload.sort.column,
                    order: order,
                },
            }

        case actionTypes.QUESTION_FILTER:
            // alert(payload.value)
            return { ...state, [payload.filter]: payload.value }

        default:
            return state
    }
}
