import * as actionTypes from '../constants';

const initialState = {
    
    userId: null,
    user:null, //remove later
    name:null,
    username:null,
};

const setAccount = (state, action) => {
  
    return { 
        ...state, 
        user:action.user,
        userId: action.user._id,
        name: action.user.first_name +" "+ action.user.last_name,
        username:action.user.username,
      };
};

const clearAccount = (state, action) => {
    return {
        ...initialState
     };
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_ACCOUNT: return setAccount(state, action);
        case actionTypes.CLEAR_ACCOUNT: return clearAccount(state, action);
        default:
            return state;
    }
};

export default reducer;