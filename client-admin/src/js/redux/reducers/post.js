import * as actionTypes from '../constants';
const initialState = {
    posts: [],
    postForm: null,
    error: null,
    loading: false,
    postToEdit: null,
    searchText: '',

    totalPages: 0,
    currentPage: 1,
    pageItems: 20,
    totalItems: 0,

    sort:{
        column:null,
        order:null,
    },
}


export default (state = initialState, { type, payload }) => {
    switch (type) {

        case actionTypes.POST_START:
            return { ...state, ...payload }

        case actionTypes.POST_INIT:
            return { ...state, loading: true }

        case actionTypes.POST_SET:
            const total = Object.keys(payload.posts).length;
            return { ...state, ...payload, totalItems: total, totalPages: Math.ceil(total / state.pageItems) }

        case actionTypes.POST_INIT_FAIL:
            return { ...state, ...payload }

        case actionTypes.POST_ADD_START:
            return { ...state, ...payload }

        case actionTypes.POST_ADD_SUCCESSFUL:
            return {
                ...state, posts: { [payload._id]: payload, ...state.posts },
                totalItems: state.totalItems + 1, totalPages: Math.ceil((state.totalItems + 1) / state.pageItems),
                loading: false
            }

        case actionTypes.POST_EDIT:

            return { ...state, ...payload }

        case actionTypes.POST_EDIT_CLEAR:
            return { ...state, postToEdit: null }

        case actionTypes.POST_REMOVE_START:
            return { ...state, loading: false }

        case actionTypes.POST_REMOVE_SUCCESSFUL:
            delete state.posts[payload.id]
            

            return {
                ...state,  loading: false,
                totalItems: state.totalItems - 1, totalPages: Math.ceil((state.totalItems - 1) / state.pageItems),
            }

        case actionTypes.POST_UPDATE_START:
            return { ...state, loading: true }

        case actionTypes.POST_UPDATE:
            return { ...state, posts: { ...state.posts, [payload.post._id]: payload.post }, loading: false }

        case actionTypes.SET_ERROR:
            return { ...state, loading: false, }

        case actionTypes.POST_SEARCH:
            return { ...state, searchText: payload, }

        case actionTypes.POST_SET_PAGE:
            return { ...state, ...payload, }

        case actionTypes.SET_ITEM_PER_PAGE:
            return {
                ...state, ...payload,
                totalPages: Math.ceil((state.totalItems + 1) / payload.pageItems),
            }

        case actionTypes.POST_SORT:

        let order = 'asc';

        if(payload.sort.column ===  state.sort.column){
            if(state.sort.order ==='asc')
                order = 'desc';
            
        }
        
            return { ...state, 
                sort:{
                    column:payload.sort.column,
                    order:order,
                },
                 }

        default:
            return state
    }
}
