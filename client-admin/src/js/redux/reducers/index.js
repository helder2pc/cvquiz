import { combineReducers } from "redux"
import {reducer as toastrReducer} from 'react-redux-toastr'
// import Auth from "./Auth_old";
import Game from "./Game";
import history from '../../../routes/history';
import Auth from "./auth";
import Account from "./account";
import User from "./user";
import Category from "./category";
import Question from "./question";
import Post from "./post";


// export default combineReducers({
// 	Auth,
// });


import { connectRouter } from 'connected-react-router'
export default combineReducers({
   Auth,
   Game,
   Account,
   User,
   Category,
   Question,
   Post,
   toastr: toastrReducer,
  router: connectRouter(history),
});