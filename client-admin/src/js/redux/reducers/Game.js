
import {
  START_GAME, NEXT_QUESTION
} from '../constants';

import {geographyData, historyData, artData, sportData} from '../data';
export default function reducer(state={
    questions: [],
    loading: true,
    loaded: false,
    questionId: null,
    currentQA: null,
    score:0,
  }, action) {

    switch (action.type) {

      /*case LOCATION_CHANGE: {
        console.log('location change')
        return {...state, data: [] }
      }*/
      

      case START_GAME: {
        
        let data =geographyData;

        if(action.gameType='history'){
          data =historyData;
        }
        else if(action.gameType='art'){
          data =artData;
        }
        else if(action.gameType='history'){
          data =sportData;
        }
        


          return {
            ...state, 
            //questions: action.listQuestion,
            questions: data,
            questionId: 0,
            currentQA:data[0], 
             }
      }
      case NEXT_QUESTION: {
        
        let value = state.questionId+1;
        if(value>9) value=9
        console.log(value)
          return {
            ...state, 
            questionId: value,
            currentQA:geographyData[value], 
             }
      }

     
      // case "GOT_PAGE": {
        
      //   return {
      //     ...state,
      //     loading: false,
      //     loaded: true,
      //     siteReady: true,
      //     data: action.payload,
      //   }
      // }
      default: {
        return {
          ...state
        };
      }
    }
}