import * as actionTypes from '../constants';
const initialState = {
    users: [],
    userForm: null,
    error: null,
    loading: false,
    userToEdit: null,
    searchText: '',

    totalPages: 0,
    currentPage: 1,
    pageItems: 20,
    totalItems: 0,

    sort:{
        column:null,
        order:null,
    },
}


export default (state = initialState, { type, payload }) => {
    switch (type) {

        case actionTypes.USER_START:
            return { ...state, ...payload }

        case actionTypes.USER_INIT:
            return { ...state, loading: true }

        case actionTypes.USER_SET:
            const total = Object.keys(payload.users).length;
            return { ...state, ...payload, totalItems: total, totalPages: Math.ceil(total / state.pageItems) }

        case actionTypes.USER_INIT_FAIL:
            return { ...state, ...payload }

        case actionTypes.USER_ADD_START:
            return { ...state, ...payload }

        case actionTypes.USER_ADD_SUCCESSFUL:
            return {
                ...state, users: { [payload._id]: payload, ...state.users },
                totalItems: state.totalItems + 1, totalPages: Math.ceil((state.totalItems + 1) / state.pageItems),
                loading: false
            }

        case actionTypes.USER_EDIT:
            return { ...state, ...payload }

        case actionTypes.USER_EDIT_CLEAR:
            return { ...state, userToEdit: null }

        case actionTypes.USER_REMOVE_START:
            return { ...state, loading: false }

        case actionTypes.USER_REMOVE_SUCCESSFUL:
            delete state.users[payload.id]
            return {
                ...state,  loading: false,
                totalItems: state.totalItems - 1, totalPages: Math.ceil((state.totalItems - 1) / state.pageItems),
            }

        case actionTypes.USER_UPDATE_START:
            return { ...state, loading: true }

        case actionTypes.USER_UPDATE:
            return { ...state, users: { ...state.users, [payload.user._id]: payload.user }, loading: false }

        case actionTypes.SET_ERROR:
            return { ...state, loading: false, }

        case actionTypes.USER_SEARCH:
            return { ...state, searchText: payload, }

        case actionTypes.USER_SET_PAGE:
            return { ...state, ...payload, }

        case actionTypes.SET_ITEM_PER_PAGE:
            return {
                ...state, ...payload,
                totalPages: Math.ceil((state.totalItems + 1) / payload.pageItems),
            }

        case actionTypes.USER_SORT:

        let order = 'asc';

        if(payload.sort.column ===  state.sort.column){
            if(state.sort.order ==='asc')
                order = 'desc';
            
        }

            return { ...state, 
                sort:{
                    column:payload.sort.column,
                    order:order,
                },
                 }

        default:
            return state
    }
}
