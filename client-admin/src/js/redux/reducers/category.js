import * as actionTypes from '../constants';
const initialState = {
    categories: [],
    categoryForm: null,
    error: null,
    loading: false,
    categoryToEdit: null,
    searchText: '',

    totalPages: 0,
    currentPage: 1,
    pageItems: 20,
    totalItems: 0,

    sort:{
        column:null,
        order:null,
    },
}


export default (state = initialState, { type, payload }) => {
    switch (type) {

        case actionTypes.CATEGORY_START:
            return { ...state, ...payload }

        case actionTypes.CATEGORY_INIT:
            return { ...state, loading: true }

        case actionTypes.CATEGORY_SET:
            const total = Object.keys(payload.categories).length;
            return { ...state, ...payload, totalItems: total, totalPages: Math.ceil(total / state.pageItems) }

        case actionTypes.CATEGORY_INIT_FAIL:
            return { ...state, ...payload }

        case actionTypes.CATEGORY_ADD_START:
            return { ...state, ...payload }

        case actionTypes.CATEGORY_ADD_SUCCESSFUL:
            return {
                ...state, categories: { [payload._id]: payload, ...state.categories },
                totalItems: state.totalItems + 1, totalPages: Math.ceil((state.totalItems + 1) / state.pageItems),
                loading: false
            }

        case actionTypes.CATEGORY_EDIT:

            return { ...state, ...payload }

        case actionTypes.CATEGORY_EDIT_CLEAR:
            return { ...state, categoryToEdit: null }

        case actionTypes.CATEGORY_REMOVE_START:
            return { ...state, loading: false }

        case actionTypes.CATEGORY_REMOVE_SUCCESSFUL:
            delete state.categories[payload.id]
            

            return {
                ...state,  loading: false,
                totalItems: state.totalItems - 1, totalPages: Math.ceil((state.totalItems - 1) / state.pageItems),
            }

        case actionTypes.CATEGORY_UPDATE_START:
            return { ...state, loading: true }

        case actionTypes.CATEGORY_UPDATE:
            return { ...state, categories: { ...state.categories, [payload.category._id]: payload.category }, loading: false }

        case actionTypes.SET_ERROR:
            return { ...state, loading: false, }

        case actionTypes.CATEGORY_SEARCH:
            return { ...state, searchText: payload, }

        case actionTypes.CATEGORY_SET_PAGE:
            return { ...state, ...payload, }

        case actionTypes.SET_ITEM_PER_PAGE:
            return {
                ...state, ...payload,
                totalPages: Math.ceil((state.totalItems + 1) / payload.pageItems),
            }

        case actionTypes.CATEGORY_SORT:

        let order = 'asc';

        if(payload.sort.column ===  state.sort.column){
            if(state.sort.order ==='asc')
                order = 'desc';
            
        }

            return { ...state, 
                sort:{
                    column:payload.sort.column,
                    order:order,
                },
                 }

        default:
            return state
    }
}
