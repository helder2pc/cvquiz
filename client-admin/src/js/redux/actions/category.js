import * as actionTypes from "../constants";

export const startCategory = () => {
  
 return {
    type: actionTypes.CATEGORY_START,
    payload: {
      loading: true,
    }
  };
};


export const initCategory = () => {

  return {
    type: actionTypes.CATEGORY_INIT,
  };
};


export const setCategory = (data) => {

  return {
    type: actionTypes.CATEGORY_SET,
    payload: {
      categories: data,
      loading: false,
    }
  };
};


export const initCategoryFail = (error) => {

  return {
    type: actionTypes.CATEGORY_INIT_FAIL,
    payload: {
      error: error,
      loading: false
    }
  };
};


export const addCategoryStart = (category) => {

    return {
    type: actionTypes.CATEGORY_ADD_START,
    categoryForm: category,
    loading: true,
  };
};


export const addCategorySuccessful = (category) => {
  return {
    type: actionTypes.CATEGORY_ADD_SUCCESSFUL,
    payload: category,
  };
};



export const editCategory = (data) => {

  const dataForm={
    _id:data._id,
    name : data.name,
    description : data.description??''
  }

  return {
    type: actionTypes.CATEGORY_EDIT,
    payload: { categoryToEdit: dataForm },
  };
};


export const editCategoryClear = (category) => {
  return {
    type: actionTypes.CATEGORY_EDIT_CLEAR,
  };
};


export const updateCategory = (data) => {

  return {
    type: actionTypes.CATEGORY_UPDATE,
    payload: { category: data },
  };
};


export const updateCategoryStart = (category) => {

  return {
    type: actionTypes.CATEGORY_UPDATE_START,
    categoryForm: category,
  };
};


export const searchCategory = (text) => {
  return {
    type: actionTypes.CATEGORY_SEARCH,
    payload: text,
  };
};

export const removeCategory = (id) => {
  return {
    type: actionTypes.CATEGORY_REMOVE_START,
    id: id,
  };
};


export const removeCategorySuccessful = (_id) => {

  return {
    type: actionTypes.CATEGORY_REMOVE_SUCCESSFUL,
    payload: { id: _id }
  };
};


export const setPage = (value) => {

  return {
    type: actionTypes.CATEGORY_SET_PAGE,
    payload: { currentPage: value }
  };
};


export const setItemPerPage = (value) => {

  return {
    type: actionTypes.SET_ITEM_PER_PAGE,
    payload: { pageItems: value }
  };
};



export const setSortCategory = (_column ) => {

  return {
    type: actionTypes.CATEGORY_SORT,
    payload: { 
      sort:{
        column:_column,
      }, 
    }
  };
};


