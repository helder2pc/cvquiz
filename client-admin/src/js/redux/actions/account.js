import * as actionTypes from "../constants";
  
export const clearAccount = () => {

  return {
    type: actionTypes.CLEAR_ACCOUNT
  };
};

export const setAccount = (_user) => {

  return {
    type: actionTypes.SET_ACCOUNT,
    user:_user
  };
};
