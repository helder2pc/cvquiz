import * as actionTypes from "../constants";

export const startUser = () => {
  return {
    type: actionTypes.USER_START,
    payload: {
      loading: true,
    }
  };
};


export const initUser = () => {

  return {
    type: actionTypes.USER_INIT,
  };
};


export const setUser = (users) => {
  return {
    type: actionTypes.USER_SET,
    payload: {
      users: users,
      loading: false,
    }
  };
};


export const initUserFail = (error) => {

  return {
    type: actionTypes.USER_INIT_FAIL,
    payload: {
      error: error,
      loading: false
    }
  };
};


export const addUserStart = (user) => {
  return {
    type: actionTypes.USER_ADD_START,
    userForm: user,
    loading: true,
  };
};


export const addUserSuccessful = (user) => {
  return {
    type: actionTypes.USER_ADD_SUCCESSFUL,
    payload: user,
  };
};


export const editUser = (user) => {
  // user.role="user"; // remove when fixed in backend 
  user.status = "active"; // remove when fixed in backend
  user.password = ""; // remove when fixed in backend
  return {
    type: actionTypes.USER_EDIT,
    payload: { userToEdit: user },
  };
};


export const editUserClear = (user) => {
  return {
    type: actionTypes.USER_EDIT_CLEAR,
  };
};


export const updateUser = (user) => {

  return {
    type: actionTypes.USER_UPDATE,
    payload: { user: user },
  };
};


export const updateUserStart = (user) => {

  return {
    type: actionTypes.USER_UPDATE_START,
    userForm: user,
  };
};


export const searchUser = (text) => {
  return {
    type: actionTypes.USER_SEARCH,
    payload: text,
  };
};

export const removeUser = (id) => {
  return {
    type: actionTypes.USER_REMOVE_START,
    id: id,
  };
};


export const removeUserSuccessful = (id) => {

  return {
    type: actionTypes.USER_REMOVE_SUCCESSFUL,
    payload: { id: id }
  };
};


export const setPage = (value) => {

  return {
    type: actionTypes.USER_SET_PAGE,
    payload: { currentPage: value }
  };
};


export const setItemPerPage = (value) => {

  return {
    type: actionTypes.SET_ITEM_PER_PAGE,
    payload: { pageItems: value }
  };
};



export const setSortUser = (_column ) => {

  return {
    type: actionTypes.USER_SORT,
    payload: { 
      sort:{
        column:_column,
      }, 
    }
  };
};





