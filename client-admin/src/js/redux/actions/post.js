import * as actionTypes from "../constants";

export const startPost = () => {
  
 return {
    type: actionTypes.POST_START,
    payload: {
      loading: true,
    }
  };
};


export const initPost = () => {

  return {
    type: actionTypes.POST_INIT,
  };
};


export const setPost = (data) => {

  return {
    type: actionTypes.POST_SET,
    payload: {
      posts: data,
      loading: false,
    }
  };
};


export const initPostFail = (error) => {

  return {
    type: actionTypes.POST_INIT_FAIL,
    payload: {
      error: error,
      loading: false
    }
  };
};


export const addPostStart = (post) => {

    return {
    type: actionTypes.POST_ADD_START,
    postForm: post,
    loading: true,
  };
};


export const addPostSuccessful = (post) => {
  return {
    type: actionTypes.POST_ADD_SUCCESSFUL,
    payload: post,
  };
};



export const editPost = (data) => {

//   const dataForm={
        
//     title: data.title,
//     title: data.title,
//     summary: data.summary,
//     content: data.content,
//     category_id: "5eada16ee57e9b002e071e26",
//     publisher_id: "5ea431a0336ed6002ed8184d",
//     // tags:[],
//     // language:'pt'
// }
  return {
    type: actionTypes.POST_EDIT,
    payload: { postToEdit: data },
  };
};


export const editPostClear = (post) => {
  return {
    type: actionTypes.POST_EDIT_CLEAR,
  };
};


export const updatePost = (data) => {

  return {
    type: actionTypes.POST_UPDATE,
    payload: { post: data },
  };
};


export const updatePostStart = (post) => {

  return {
    type: actionTypes.POST_UPDATE_START,
    postForm: post,
  };
};


export const searchPost = (text) => {
  return {
    type: actionTypes.POST_SEARCH,
    payload: text,
  };
};

export const removePost = (id) => {
  return {
    type: actionTypes.POST_REMOVE_START,
    id: id,
  };
};


export const removePostSuccessful = (_id) => {

  return {
    type: actionTypes.POST_REMOVE_SUCCESSFUL,
    payload: { id: _id }
  };
};


export const setPage = (value) => {

  return {
    type: actionTypes.POST_SET_PAGE,
    payload: { currentPage: value }
  };
};


export const setItemPerPage = (value) => {

  return {
    type: actionTypes.SET_ITEM_PER_PAGE,
    payload: { pageItems: value }
  };
};



export const setSortPost = (_column ) => {

  return {
    type: actionTypes.POST_SORT,
    payload: { 
      sort:{
        column:_column,
      }, 
    }
  };
};


