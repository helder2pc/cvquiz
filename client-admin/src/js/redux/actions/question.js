import * as actionTypes from "../constants";

export const startQuestion = () => {

    return {
        type: actionTypes.QUESTION_START,
        payload: {
            loading: true,
        }
    };
};


export const initQuestion = () => {

    return {
        type: actionTypes.QUESTION_INIT,
    };
};


export const setQuestion = (data) => {

    return {
        type: actionTypes.QUESTION_SET,
        payload: {
            questions: data,
            loading: false,
        }
    };
};


export const initQuestionFail = (error) => {

    return {
        type: actionTypes.QUESTION_INIT_FAIL,
        payload: {
            error: error,
            loading: false
        }
    };
};


export const addQuestionStart = (question) => {

    return {
        type: actionTypes.QUESTION_ADD_START,
        questionForm: question,
        loading: true,
    };
};


export const addQuestionSuccessful = (question) => {
    return {
        type: actionTypes.QUESTION_ADD_SUCCESSFUL,
        payload: question,
    };
};



export const editQuestion = (data) => {

    const dataForm = {
        _id: data._id,
        question: data.question ?? '',
        description: data.description ?? '',
        difficulty: data.difficulty ?? '',
        type: data.type ?? '',
        language: 'pt'
    }

    return {
        type: actionTypes.QUESTION_EDIT,
        payload: { questionToEdit: dataForm },
    };
};


export const editQuestionClear = (question) => {
    return {
        type: actionTypes.QUESTION_EDIT_CLEAR,
    };
};


export const updateQuestion = (data) => {

    return {
        type: actionTypes.QUESTION_UPDATE,
        payload: { question: data },
    };
};


export const updateQuestionStart = (question) => {

    return {
        type: actionTypes.QUESTION_UPDATE_START,
        questionForm: question,
    };
};


export const searchQuestion = (text) => {
    return {
        type: actionTypes.QUESTION_SEARCH,
        payload: text,
    };
};

export const removeQuestion = (id) => {
    return {
        type: actionTypes.QUESTION_REMOVE_START,
        id: id,
    };
};


export const removeQuestionSuccessful = (_id) => {

    return {
        type: actionTypes.QUESTION_REMOVE_SUCCESSFUL,
        payload: { id: _id }
    };
};


export const setPage = (value) => {

    return {
        type: actionTypes.QUESTION_SET_PAGE,
        payload: { currentPage: value }
    };
};


export const setItemPerPage = (value) => {

    return {
        type: actionTypes.SET_ITEM_PER_PAGE,
        payload: { pageItems: value }
    };
};



export const setSortQuestion = (_column) => {

    return {
        type: actionTypes.QUESTION_SORT,
        payload: {
            sort: {
                column: _column,
            },
        }
    };
};



export const setFilterQuestion = (filter, value) => {

    return {
        type: actionTypes.QUESTION_FILTER,
        payload: {
            filter: filter,
            value: value,
        }
    };
};


