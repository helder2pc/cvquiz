import * as actionTypes from "../constants";

export const handleError = (response) => {
  return {
    type: actionTypes.SET_ERROR,
    response:response,
  };
};
