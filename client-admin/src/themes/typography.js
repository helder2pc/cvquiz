export default {
    useNextVariants: true,
    fontSize: 11,
    fontFamily: ['Arial',  'sans-serif', 'Helvetica Neue'].join(',')
  };