import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import { Login, Feed } from '../js/pages';
import { Toolbar, AddQuestion, CircularDeterminate } from '../js/components';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import { Menu, GameArea, Result } from '../js/components/Game';
import Dashboard from '../js/pages/dashboard/dashboard';
import history from './history'
import {ConnectedRouter} from 'connected-react-router';
import {connect} from "react-redux";
import AddCategory from '../js/components/AddCategory/addCategory';
import ReduxToastr from 'react-redux-toastr'
function CVQRouter(props) {
  const {isAuthenticated} = props;
  
  return (
    <ConnectedRouter history={history}> 
      <div className="App"> 
      {/* { isAuthenticated && <Toolbar></Toolbar>} */}
      {/* <Toolbar></Toolbar>
        */}

        <ReduxToastr
              timeOut={3000}
              newestOnTop={false}
              preventDuplicates
              position="top-right"
              getState={(state) => state.toastr} // This is the default
              transitionIn="fadeIn"
              transitionOut="fadeOut"
              progressBar= {false}
              closeOnToastrClick= {false}
              />

        <Switch>
          {isAuthenticated && <Route exact path="/" component={Dashboard} />} 
          {isAuthenticated===null && <Route exact path="/" component={CircularDeterminate} />}
          {isAuthenticated===false && <Route exact path="/" component={Login} />}
          {isAuthenticated && <Route path="/login" component={Login} />}
          <Redirect from='*' to='/' />

          {/* <Route exact path="/" component={Login} /> */}
          {/* <Route path="/dashboard" component={Dashboard} />  */}
          {/* <Route path="/addquestion" component={AddQuestion} /> 
          <Route path="/addcategory" component={AddCategory} /> */}
           {/* <Route path="/login" component={Login} /> */}
          {/* <Route path="/feed" component={Feed} /> */}
          {/* <Route path="/menu" component={Menu} />
          <Route path="/game-area" component={GameArea} />
          <Route path="/topics" component={Toolbar} />
          <Route path="/result" component={Result} />  */}
          
        </Switch>

     

      </div>
  </ConnectedRouter>
  );
}

// export default CVQRouter;
const mapStateToProps = state => ({
  isAuthenticated: state.Auth.isAuthenticated,
  loading: state.Auth.loading
})

export default connect(
  mapStateToProps,
  null
)(CVQRouter);
