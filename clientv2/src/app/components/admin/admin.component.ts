import { AdminService } from './admin.service';


import { UserService } from './../../services/user.service';
import { Router } from '@angular/router';
import { Pergunta } from './../../shared/pergunta';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { Question } from '../../models/question';

@Component({
  selector: 'admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [AdminService]
})
export class AdminComponent implements OnInit {
  public questions: Question[]; 
  public category: string;
  public text;

  constructor(
    private _adminService: AdminService,
    private _userService: UserService,
    private router: Router,

  ) {
    this.category = 'Historia';
    this.text="3434";
  /*  this._sharedService.changeEmitted$.subscribe(
      text => {
        console.log("AAAAAAAAAAAA")
        console.log(text);
        this.text=text;
        
      });*/
      AdminService.get('sentquestion').subscribe(
        data => {
          this.text = data
          this.getAllQuestions();

        });
  }

  ngOnInit() {

    this.getAllQuestions();
    //this.questions = this._adminService.getAllQuestion(this.category);


  }

  /* this._userService.getUsers(page).subscribe(
     response => {
         if(!response.users){
           this.status ="error";
         }
 */

  getAllQuestions() {
    //this._adminService.getAllQuestion.("history",1).subscribe()
    this._adminService.getAllQuestion(this.category, 1).subscribe(
      response => {
        console.log(response);
        this.questions = response.qa;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        if (errorMessage != null) {
          //this.status ="error";
        }
      }
    );

  }


  changeSelect(select) {
    this.category = select.value;
    this.ngOnInit();
  }

  createQuestion() {
    this.router.navigate(['/admin/question/new', this.category]);
  }

}

/*export class ParentComponent { 
  dummyText: string; 
  constructor(private _sharedService: SharedService) { 
    _sharedService.changeEmitted$.subscribe(
      text => { 
        this.dummyText = text;
       }); 
      } 
}*/