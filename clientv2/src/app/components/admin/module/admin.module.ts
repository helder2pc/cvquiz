import { AdminService } from './../admin.service';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './../admin.component';
import { NgModule } from '@angular/core';
import { SharedModule } from './../../../shared/shared.module';
import { QuestionFormComponent } from './../question-form/question-form.component';
import { QuestionDetailsComponent } from './../question-details/question-details.component';

@NgModule({
  imports: [
    SharedModule,
    AdminRoutingModule
  ],
  declarations: [AdminComponent, QuestionFormComponent, QuestionDetailsComponent], 
  exports: [AdminComponent, QuestionFormComponent, QuestionDetailsComponent], 

  providers: [AdminService]
})
export class AdminModule { }


