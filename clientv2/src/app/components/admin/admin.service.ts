import { Question } from './../../models/question';
import { GLOBAL } from './../../services/global';
import { Pergunta } from './../../shared/pergunta';
import { Database } from './../../shared/database';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http'; //Response, ResponseOptions,Headers
import { EventEmitter } from '@angular/core';

@Injectable()
export class AdminService {
  private database: Database;
  public url: string;

   constructor(public _http:HttpClient,){
    this.database = new Database();
    this.url = GLOBAL.url;
}

   getQuestion(category: string, id: number) :Observable<any>{
    //return this.database.getQuestion(category, id);

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this._http.get(this.url + 'qa-history/'+id, {'headers':headers});
  }


  getAllQuestion(category: string,page=1):Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    return  this._http.get(this.url + 'qas-history/'+page, {'headers':headers});
   // return this.database.getAllQuestion(category);
  }

  

  sendQuestion(question: any, category: string) :Observable<any>{
    //this.database.addQuestion(pergunta, category);
    let params = JSON.stringify(question);
    console.log("ADMN SERVCE")
    console.log(category)

        console.log(params)
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    switch (category) {
        case 'history':
            return this._http.post(this.url+'qa-history', params,{'headers':headers});
        case 'tour':
            return this._http.post(this.url+'qa-image', params,{'headers':headers});
        case 'geography':
            return this._http.post(this.url+'qa-geography', params,{'headers':headers});  
        case 'sport':
        return this._http.post(this.url+'qa-sport', params,{'headers':headers});  
        case 'art':
        return this._http.post(this.url+'qa-art', params,{'headers':headers});        
         
    
        default:
            break;
    }
   

  }

  deleteQuestion( id: string, category: string) :Observable<any>{
   // this.database.removeQuestion(id, category);
   let headers = new HttpHeaders().set('Content-Type', 'application/json');
   return this._http.delete(this.url+'qa-history/'+id, {'headers':headers});
  }

  updateQuestion( id: string,  question: Question) :Observable<any>{
   let params = JSON.stringify(question);
   console.log(params)
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.put(this.url+'qa-history/'+id, params,{'headers':headers});
  }



  private static emitters: {
    [nomeEvento: string]: EventEmitter<any>
    } = {}

static get (nomeEvento:string): EventEmitter<any> {
    if (!this.emitters[nomeEvento])
        this.emitters[nomeEvento] = new EventEmitter<any>();
    return this.emitters[nomeEvento];
}
}
