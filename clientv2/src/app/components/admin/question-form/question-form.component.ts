import { UserService } from './../../../services/user.service';
import { GLOBAL } from './../../../services/global';
import { UploadService } from './../../../services/upload.service';
import { Question } from './../../../models/question';

import { AdminService } from './../admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Rx';

import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.css'],
  providers:[AdminService,UserService,UploadService]
})
export class QuestionFormComponent implements OnInit, OnDestroy {

  /*@ViewChild('questionInput') questionValueInput: ElementRef; 
  @ViewChild('option1Input') option1ValueInput: ElementRef;
  @ViewChild('option2Input') option2ValueInput: ElementRef;
  @ViewChild('option3Input') option3ValueInput: ElementRef;
  @ViewChild('option4Input') option4ValueInput: ElementRef;
  @ViewChild('select') selectValueInput: ElementRef;*/


  private inscription: Subscription;
  public question: Question;
  private formChange = false;
  public category: string;
  public idQuestion: string;
  public form: FormGroup;
  public mode: string;
  private token;
  private url;
 


  constructor(
    private route: ActivatedRoute,
    private _adminService: AdminService,
    private _uploadService: UploadService,
    private _userService: UserService,
    public router: Router,
    public formBuilder: FormBuilder,
   
  ) {
    this.url = GLOBAL.url;
   // this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();

   }

  ngOnInit() {
  /*
   this.form =  new FormGroup({
    question: new FormControl('') ,
    answer1: new FormControl('') ,
    answer2: new FormControl('') ,
    answer3: new FormControl('') ,
    answer4: new FormControl('') ,
    select: new FormControl('') ,
  });*/

  this.form = this.formBuilder.group({
    question: [''],
    answer1: [''],
    answer2: [''],
    answer3: [''],
    answer4: [''],
    image: [''],
    select: [''],
  });
    

    this.form.controls['select'].valueChanges.subscribe((value) => {
      /*if (this.registerForm.controls['yourControlName'].value === 'someValue') {
         // 
      }*/
      this.category=value;
     // alert(value)
    });

    
    this.form.controls['image'].valueChanges.subscribe((value) => {
    //  console.log(value.target)
     // alert( this.form.value)
    //  this.filesToUpload = <Array <File>> value.target.files;
    
    });

    this.initQuestion();

    //(change)="fileChangeEvent($event)"
    
  }

  fillQuestionForm(){
    this.form = this.formBuilder.group({
      question: [this.question.question, Validators.required],
      answer1: [this.question.answers[0], [Validators.required]],
      answer2: [this.question.answers[1], [Validators.required]],
      answer3: [this.question.answers[2], [Validators.required]],
      answer4: [this.question.answers[3], [Validators.required]],
      select: [this.category],
    });
  }

  initQuestion() {
    this.inscription = this.route.params.subscribe(
      (params: any) => {
        let id = params['id'];
        let cat = params['category'];
        this.idQuestion = id;
        this.category = cat;
        //this.selectValueInput.nativeElement.value = cat;
        // criar nova pergunta verifcar depois e melhorar 
        if (cat != undefined && id === undefined) {
          this.question = new Question('', 'question', ['option', 'option', 'option', 'option'],'','','');
          this.mode="new";
          return;
        }
       /* this.question = null;//this.adminService.getQuestion(cat, id);

        if (this.question == null) {
          this.question = new Question ('', 'question', ['option', 'option', 'option', 'option'],'','')
        }*/
        this.inscription = this._adminService.getQuestion(this.category, id).subscribe(
          response =>{
            console.log(response);
            this.question=response.qa;
            this.fillQuestionForm();
            },
            error => {
              var errorMessage = <any>error;
              console.log(errorMessage);
              if(errorMessage!=null){
                //this.status ="error";
              }
            }
         );
         this.mode="edit";
      }
    );
  }

  ngOnDestroy() {
    this.inscription.unsubscribe();
  }

  onInput() {
    this.formChange = true;
    console.log('mudou');
  }

  canChangeRoute() {
    if (this.formChange) {
      confirm('Tem certeza que deseja sair dessa página?');
    }
    return true;
  }

 

  podeDesativar() {
    return this.canChangeRoute();
  }

  changeSelect(select) {
    this.category = select.value;

  }

  sendQuestion(value) {

    if (this.form.valid) {
    let quest = new Question(
      this.idQuestion,
      this.form.value.question,
      [
        this.form.value.answer1,
        this.form.value.answer2,
        this.form.value.answer3,
        this.form.value.answer4,
      ],
      '','',''
    );
  
  console.log(JSON.parse(JSON.stringify(quest)));
   // this.adminService.sendQuestion(quest, this.form.value.select);
  
 

    //console.log(this.form.value);

    if(this.mode=="new"){
      quest._id=""; 
      this.inscription = this._adminService.sendQuestion(quest, this.category).subscribe(
        response =>{
           alert("pergunta inserido com successo");
           console.log(response.qa._id);
           //this.router.navigate(['/admin']);

           if(this._adminService && this.filesToUpload.length){
            //enviar imagem
              this._uploadService.makeFileRequest(this.url+'upload-image-qa/'+response.qa._id,
                                                  [],
                                                  this.filesToUpload,
                                                  this.token,
                                                  'file')
                                  .then((result:any)=>{
                                    //this.imagem =result.image;
                                    console.log(result.image)
                                  });
         
          }



           this.form.reset();
           AdminService.get('sentquestion').emit(true);
        },
        
        error => {
          var errorMessage = <any>error;
          if(errorMessage!=null){
            console.log(errorMessage);
          }
        }
      )
    }
    else if(this.mode=="edit"){
      this.inscription = this._adminService.updateQuestion(this.idQuestion,quest).subscribe(
        response =>{
          alert("pergunta atualizado com successo");
          console.log(response);
          AdminService.get('sentquestion').emit(true);
          //this.router.navigate(['/admin']);
          //this.form.reset();
        },
        error => {
          var errorMessage = <any>error;
          if(errorMessage!=null){
            console.log(errorMessage);
          }
        }
      )
    }
  }
  else {
    alert("pergunta invalida")
  }
console.log(this.mode)







    //this.router.navigate(['/admin/question', this.category, this.idQuestion]);
    //question/{{category}}/{{question_.id}}"
  }

  reset() {
    this.form.reset();
  }

  aplicaCssErro(campo: string) {
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
    };
  }


  verificaValidTouched(campo: string) {
    return (
      !this.form.get(campo).valid &&
      (this.form.get(campo).touched || this.form.get(campo).dirty)
    );
  }

  /*onClick(){
    this._sharedService.emitChange('Data from child');
   }*/

public filesToUpload: Array <File>;

onFileChange($event) {
  this.filesToUpload = <Array <File>> $event.target.files;
  console.log(this.filesToUpload)
  
  //let file = $event.target.files[0]; // <--- File Object for future use.
  //this.form.controls['imageInput'].setValue(file ? file.name : ''); // <-- Set Value for Validation
}

}
