
import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { MessageService } from './../../../../services/message.service';
import { UserService } from './../../../../services/user.service';
import { Message } from './../../../../models/message';
import {Follow} from '../../../../models/follow';
import {FollowService} from '../../../../services/follow.service';
import { GLOBAL} from '../../../../services/global';

@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers:[FollowService, MessageService,UserService],
})
export class AddComponent implements OnInit {
  public  title;
  public message:Message;
  public identity;
  public token;
  public url;
  public page;
  public status;
  public follows;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _messageService: MessageService,
    private _followService: FollowService,
    private _userService: UserService
  ) {
    this.title ="Messagens privados,";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.message = new Message('','','','',this.identity._id,'');
    this.url = GLOBAL.url;
    
   }

  ngOnInit() {
    this.getMyFollows();
  }

  getMyFollows(){
    this._followService.getMyFollows(this.token).subscribe(
      response =>{
       // console.log("FOLLOWSSSSSSSSSSSSSSS")
        //console.log(response);
        this.follows = response.follows;
      },
      error => {
        console.log(<any> error);
      }
    )
  }

  onSubmit(form){
    this._messageService.addMessage(this.token, this.message).subscribe(
      response =>{
          if(response.message){
            this.status = 'success';
            form.reset();
          }
      },
      error =>{
        this.status ='error',
        console.log(<any>error);
      }
    )
    console.log(this.message)
  }

}
