import { UserGuard } from './../../services/user.guard';
//import { ModuleWithProviders }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


//components
import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { ReceivedComponent } from './components/received/received.component';
import { SendedComponent } from './components/sended/sended.component';

const messagesRoutes: Routes=[
    {
        path: 'mensagem', component: MainComponent,
        children:[
            {path:'', redirectTo: 'recebidos', pathMatch: 'full'},
            {path:'enviar', component: AddComponent,canActivate:[UserGuard]},
            {path:'recebidos', component:ReceivedComponent ,canActivate:[UserGuard]},
            {path:'recebidos/:page', component:ReceivedComponent ,canActivate:[UserGuard]},
            {path:'enviados', component: SendedComponent ,canActivate:[UserGuard]},
            {path:'enviados/:page', component: SendedComponent ,canActivate:[UserGuard]},
        ]
    }
];

@NgModule({
    imports:[
        RouterModule.forChild(messagesRoutes)
    ],

    exports:[
        RouterModule
    ]
})

export class MessageRountingModule{}