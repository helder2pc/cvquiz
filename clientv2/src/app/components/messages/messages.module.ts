import { UserGuard } from './../../services/user.guard';
import { UserService } from './../../services/user.service';


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'angular2-moment';

//rounting 
import { MessageRountingModule } from './messages.routing';

//components
import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { ReceivedComponent } from './components/received/received.component';
import { SendedComponent } from './components/sended/sended.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MessageRountingModule,
    MomentModule,
  ],
  declarations: [
    MainComponent, 
    AddComponent, 
    ReceivedComponent, 
    SendedComponent
  ],
  exports: [
    MainComponent, 
    AddComponent, 
    ReceivedComponent, 
    SendedComponent
  ],
  providers:[
    UserService,
    UserGuard
  ],
})
export class MessagesModule {
  
 }
