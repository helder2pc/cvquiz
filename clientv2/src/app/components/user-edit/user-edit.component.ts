import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';
import {trigger, state, style, transition, animate} from '@angular/animations'

import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {UploadService} from '../../services/upload.service';
import { GLOBAL} from '../../services/global';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService,UploadService],
  animations:[
    trigger('animation-register',[
      state('create',style({
        opacity: 1,
        transform: 'scale(1)'
      })),
  
      transition('void => create',[
        style({
          opacity:0,
           /*transform: 'translate(50px,0)'*/
           transform: 'scale(0.5)'
        }),
        animate(
          //duracao, delay e acelaracao
          '1000ms 0s ease-in-out' 
        )
      ])
    ])
  ]
})
export class UserEditComponent implements OnInit {
  public title:string;
  public user: User;
  public status: string;
  public identity;
  public token;
  public url;
  public stateUserEdit: string = 'create';

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _uploadService: UploadService
  ) { 
    this.title ="Atualizar meus dados";
    this.user = this._userService.getIdentity();
    console.log("121212121212WRWRWRRW");
    console.log(this._userService.getIdentity());
    this.identity = this.user;
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
  }

  ngOnInit() {
    console.log(this.user);
    console.log("user-edit.component foi carregado" )
  }

  onSubmit(){
    console.log(this.user);
    this._userService.updateUsers(this.user).subscribe(
      response =>{
          if(!response.user){
            this.status = 'error';
          }
          else {
            this.status ='success';
            localStorage.setItem('identity',JSON.stringify(this.user));
            this.identity=this.user;

            // Esnviar imagem do utilizador
            if(this.filesToUpload){ 
            this._uploadService.makeFileRequest(this.url+'upload-image-user/'+this.user._id,[],this.filesToUpload,this.token,'image')
                                .then((result:any)=>{
                               
                                  this.user.photo = result.user.photo;
                                  localStorage.setItem('identity',JSON.stringify(this.user));
                                })
          }
          else{
            console.log("file empty")
          }
        }
        
      },

      error =>{
          var errorMessage =<any>error;
          console.log(error);
          if(errorMessage != null){
            this.status = 'error';
          }
      }
    );
  }

  public filesToUpload: Array<File>;
  fileChangeEvent(fileInput:any){
    this.filesToUpload = <Array <File>> fileInput.target.files;
    console.log(this.filesToUpload);
   
  }

}
