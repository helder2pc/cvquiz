import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';
import {trigger, state, style, transition, animate} from '@angular/animations'

import {User} from '../../models/user';
import {UserService} from '../../services/user.service';


@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService],
  animations:[
    trigger('animation-register',[
      state('create',style({
        opacity: 1
      })),
  
      transition('void => create',[
        style({
          opacity:0,
           transform: 'translate(50px,0)'
        }),
        animate(
          //duracao, delay e acelaracao
          '500ms 0s ease-in-out' 
        )
      ])
    ])
  ]
  
   
})

export class RegisterComponent implements OnInit {
    public user: User;
    public title:string;
    public status:string;
    public stateRegister: string = 'create';
    public height: string;
    public captchaResponse;
  
    constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) { 
    this.title = 'Registar';
    this.user = new User("","","","","","","ROLE_USER","",null,null);
    this.height="600";
    this.captchaResponse=null;
  }

  ngOnInit() {
  }

  onSubmit(form){
    //console.log(this.user);
    if(!this.captchaResponse){
      this.status = "error_captcha";
      this.height  = "680";
      return;
    }
    
    this._userService.register(this.user).subscribe(
      response =>{
        console.log("AAAAA "+ JSON.stringify( response));
        if(response!=null && response._id){
          this.status='success';
          form.reset();
          //console.log(response.user);
        }
       else{
        this.status='error';
       }
       this.height  = "680";
      },
      error =>{
        console.log(<any> error);
      }
     
    );
  }
  resolved(_captchaResponse: string){
    console.log('Resolved '+ _captchaResponse )
    this.captchaResponse=_captchaResponse;
  }

}
