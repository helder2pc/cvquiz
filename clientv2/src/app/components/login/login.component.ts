import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute, Params} from '@angular/router';
import {trigger, state, style, transition, animate} from '@angular/animations'


import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService],
  animations:[
  trigger('animation-login',[
    state('create',style({
      opacity: 1
    })),

    transition('void => create',[
      style({
        opacity:0,
         transform: 'translate(50px,0)'
      }),
      animate(
        //duracao, delay e acelaracao
        '500ms 0s ease-in-out' 
      )
    ])
  ]),

  trigger('animation-logo',[
    state('create',style({
      opacity: 1
    })),

    transition('void => create',[
      style({
        opacity:0,
         transform: 'translate(-50px,0)'
      }),
      animate(
        //duracao, delay e acelaracao
        '500ms 0s ease-in-out' 
      )
    ])
  ])
]
})
export class LoginComponent implements OnInit {

    public stateLogin: string = 'create';
    public stateLogo: string = 'create';
     
    public title:string;
    public user: User;   
    public status: string;
    public identity;
    public token;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {
    this.title ="Login";
    this.user = new User("","","","","","","ROLE_USER","",null, null);
   }

  ngOnInit() {
  }

  onSubmit(){
   this._userService.signin(this.user).subscribe(

        response =>{
         
          console.log(response)
          console.log(response.user)
          console.log(response.token)
          this.identity = response.user;
          if(!this.identity || !this.identity._id){
            this.status ="error";
          }
          else{
          
            //Guardar dados do utlizador
            localStorage.setItem('identity',JSON.stringify(this.identity));
            this._router.navigate(['/category']);
           
           //this.getToken();
           this.setToken(response.token);
          }     
       
        },

        error => {
          var errorMessage = <any> error;
        
          if(errorMessage != null){
            this.status = 'error';
          }
        }
   );
  }

  getToken(){

    this._userService.signin(this.user,'true').subscribe(

      response =>{
        this.token = response.token;
        console.log( this.token);
        if(this.token.length <= 0){
          this.status ="error";
        }
        else{
          
          //Guardar token do utlizador
          localStorage.setItem('token',this.token);
          this.getCounters();
         
          // conseguir contadores e estatistica
        }
       
       
       // console.log(response.user);
        
      },

      error => {
        var errorMessage = <any> error;
        console.log(error);
        if(errorMessage != null){
          this.status = 'error';
        }
      }
 );
  }


  setToken(token){

        this.token = token;
        if(this.token.length <= 0){
          this.status ="error";
        }
        else{ 
          //Guardar token do utlizador
          localStorage.setItem('token',this.token);
        }
      }

    


  getCounters(){
    this._userService.getCounters().subscribe(
      response => {
        //if(response.follwing.length)
        localStorage.setItem('stats',JSON.stringify(response));
        this.status = 'success';
        this._router.navigate(['/category']);
      },

      error =>{
        console.log(<any> error);
      }
    );
  }

}
