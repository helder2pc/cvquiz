import { Subscription } from 'rxjs/Rx';

import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import{Router, ActivatedRoute} from '@angular/router';
import { Publication } from './../../models/publication';
import {UserService} from '../../services/user.service';
import { PublicationsService } from './../../services/publication.service.';
import { GLOBAL} from '../../services/global';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
  providers: [UserService,PublicationsService],
})
export class TimelineComponent implements OnInit {

  public title:string;
  public status: string;
  public identity;
  public token;
  public url;
  public page;
  public pages;
  public total;
  public itemsPerPage;
  public showImage;
  public publications: Publication[];
  public isCreate:boolean;
  public interval;
  private inscription: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _publicationsService: PublicationsService,
  ) { 
    this.title ="Timeline";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.page=1;
    this.isCreate = false;
    this.getPublications(this.page);

  }

  ngOnInit() {
    this.getPublications(this.page);
    this.refresh();
  }

  getPublications(page, adding=false){
    //alert(this.isCreate);
    this.inscription = this._publicationsService.getPublication(this.token,page).subscribe(
        response => {
             
              if(response.publication){
               
                this.total = response.total_items;
                this.pages = response.pages;
                this.itemsPerPage = response.itemsPerPage;
                this.isCreate = true;
                //alert(this.isCreate)

                if(!adding){
                  this.publications = response.publication;
                }
                else{
                  var arrayA = this.publications;
                  var arrayB = response.publication;
                  this.publications = arrayA.concat(arrayB);
                  $("html, body").animate({ scrollTop: $('html').prop("scrollHeight")},500);

                }

                if(page > this.pages){
                  //this._router.navigate(['/home']);
                }                
              }
              else{
                this.status = 'error';
              }
        },

        error =>{
          var errorMessage =<any> error;
          if(errorMessage!=null){
            this.status = 'error';
          }
        }
      );
  }

  public noMore = false;
  viewMore(){
    this.page += 1;

    if(this.page == this.pages ){
        this.noMore =true;
    }
  
    this.getPublications(this.page,true);
  }

  refresh(event=null){
    this.getPublications(1);

    this.interval = setInterval(() => {
      this.getPublications(1);
    }, 30000);
  }

  /*refresh(event=null){
    this.getPublications(1);
  }*/

  showThisImage(id){
    this.showImage=id;
  }

  hideThisImage(){
    this.showImage = 0;
  }

  deletePublication(id){
    this._publicationsService.deletePublication(this.token,id).subscribe(
      response =>{
          this.refresh();
      },
      error =>{
        console.log(<any>error);
      }
    );
  }
  chanlenge(id){
    this._router.navigate(['/game-area']);
  }


  OnDestroy() {
    window.clearInterval(this.interval);
    this.inscription.unsubscribe();
  }

  @HostListener("window:scroll", [])
    onScroll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            // you're at the bottom of the page
          //  alert(34)
            //this.viewMore();
        }
    }
}
