import { UserService } from './../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService],
})
export class HomeComponent implements OnInit {
public title:string;
public identity;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _userService : UserService
  ) { 
    this.title ='Bem vindo ao CVQuiz';
    this.identity = this._userService.getIdentity();
  }

  ngOnInit() {
    console.log(' home carregado')
  }

}
