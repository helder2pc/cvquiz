import { GLOBAL } from './../../services/global';
import { UserService } from './../../services/user.service';
import { FeedInfo } from './../../models/feedinfo';
import { FeedInfoService } from './../../services/feedinfo';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'feedinfo',
  templateUrl: './feedinfo.component.html',
  styleUrls: ['./feedinfo.component.css'],
  providers: [FeedInfoService, UserService]
})
export class FeedinfoComponent implements OnInit {

  public identity;
  public token;
  public url;
  public page;
  public pages;
  public total;
  public itemsPerPage;
  public feedInfos: FeedInfo[];
  public feedInfoShow: FeedInfo[];
  public status: string;
  public n_feedInfo: number;

  constructor(
    private _feedInfoService: FeedInfoService,
    private _userService: UserService,
    
  ) { 
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.n_feedInfo = 3;
    this.feedInfoShow =[];
    
    
    this.page=1;
  }

  ngOnInit() {
    this.getFeedInfo( this.page);
   
  }

  getFeedInfo(page, adding=false){
    //alert(this.isCreate);
      this._feedInfoService.getFeedInfo(this.token,page).subscribe(
        response => {
           
          
              if(response.feedInfo){
               
                this.total = response.total_items;
                this.pages = response.pages;
                this.itemsPerPage = response.itemsPerPage;
                

                if(!adding){
                  this.feedInfos = response.feedInfo;
                  this.getRandomFeed();
                  console.log(this.feedInfos);
                

                }
                else{
                  var arrayA = this.feedInfos;
                  var arrayB = response.feedInfo;
                  this.feedInfos = arrayA.concat(arrayB);
                  $("html, body").animate({ scrollTop: $('html').prop("scrollHeight")},500);

                }

                if(page > this.pages){
                  //this._router.navigate(['/home']);
                }                
              }
              else{
                this.status = 'error';
              }
             
        },

        error =>{
          var errorMessage =<any> error;
          if(errorMessage!=null){
            this.status = 'error';
          }
        }
      );
  }

  getRandomFeed(){
    //let len= this.feedInfos.length;
    let array = this.shuffleArray(this.feedInfos);
    for(var i =0 ; i<this.n_feedInfo;i++){
      this.feedInfoShow.push(this.feedInfos[i]);
    }
    
  }
 

  shuffleArray(array){
  
      let m = array.length, t, i;
    
      // While there remain elements to shuffle
      while (m) {
        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);
    
        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
      }
    
      return array;
    } 

    

}
