import { Subscription } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { QAService } from './../../services/qa.service';
import { Question } from './../../models/question';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-question',
  templateUrl: './user-question.component.html',
  styleUrls: ['./user-question.component.css']
})
export class UserQuestionComponent implements OnInit {
  public questions: Question[];
  public category: string;
  public create: boolean;
  private inscription: Subscription;

  constructor(
    private _qAService:  QAService,
    private router: Router,
  ) { 
   
    this.category = 'Historia';
    this.create = true;
  }
 
  ngOnInit() {
    this.getAllQuestions();
  }

  getAllQuestions() {
    //this._adminService.getAllQuestion.("history",1).subscribe()
    this.inscription = this._qAService.getAllQuestion(this.category, 1).subscribe(
      response => { 
        this.questions = response.qa;
       
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        alert(567)
        if (errorMessage != null) {
          //this.status ="error";
        }
      }
    );

    

}

  changeSelect(select) {
    this.category = select.value;
    this.ngOnInit();
  }

  createQuestion() {
    this.router.navigate(['/admin/question/new', this.category]);
  }

  changeStatusTrue(){
   // (this.create)?this.create=false:this.create=true;
   this.create=true;
  }
  changeStatusFalse(){
    this.create=false;
  }

  ngOnDestroy() {
    this.inscription.unsubscribe();
  }

}
