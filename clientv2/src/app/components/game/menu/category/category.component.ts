import { Config } from './../../../../shared/config';
import { MenuService } from './../menu.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {trigger, state, style, transition, animate} from '@angular/animations'

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [MenuService],
  animations:[
    trigger('animation-register',[
      state('create',style({
        opacity: 1,
        transform: 'scale(1)'
      })),
  
      transition('void => create',[
        style({
          opacity:0,
           /*transform: 'translate(50px,0)'*/
           transform: 'scale(0.5)'
        }),
        animate(
          //duracao, delay e acelaracao
          '1000ms 0s ease-in-out' 
        )
      ])
    ])
  ]
})
export class CategoryComponent implements OnInit {
  categories: any;
  public stateCategory: string = 'create';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private menuService: MenuService
    
  ) { }

  ngOnInit() {
    this.categories = this.menuService.getCategory();
    //alert(JSON.stringify(this.categories));
  }

  playGame(btn: HTMLButtonElement) {
    let cate: string = btn.getAttribute('catName');
   // alert(cate);
    this.router.navigate(['/game-area']);

    Config.categorySelected = cate;

  }

}
