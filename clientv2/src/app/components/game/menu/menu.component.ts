import { Config } from './../../../shared/config';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from './menu.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [MenuService]
})
export class MenuComponent implements OnInit {

  constructor(
    private menuService: MenuService,
    private route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
  }

  playSingleMode () {
    this.router.navigate(['/menu/category']);
    Config.gameType = 'single';
  }

  playMultiplayerMode () {
    this.router.navigate(['/menu/category']);
    Config.gameType = 'multiplayer';
  }

  play1vs1Mode () {
    this.router.navigate(['/menu/category']);
    Config.gameType = '1vs1';
  }

  ranking () {
    this.router.navigate(['/menu/ranking']);
  }

  setting () {
    this.router.navigate(['/menu/setting']);
  }

  help () {
    this.router.navigate(['/menu/help']);
  }

}
