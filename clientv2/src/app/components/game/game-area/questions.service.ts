import { GLOBAL } from './../../../services/global';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class QuestionsService {
  public url: string;
  constructor(public _http:HttpClient,){
    this.url = GLOBAL.url;
}

   getQuestion_(category: string, id: number) {
    /* console.log(this.database.getQuestion(category, id))
     return this.database.getQuestion(category, id);*/
   }

   getQuestion(category: string, id: number) :Observable<any>{

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this._http.get(this.url + 'qa-history/'+id, {'headers':headers});
  }


  getAllQuestion(category: string,page=1):Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    switch (category) {
      case 'history':
        return  this._http.get(this.url + 'qas-history/'+page, {'headers':headers});
      case 'geography':
        return  this._http.get(this.url + 'qas-geography/'+page, {'headers':headers});
      case 'sport':
        return  this._http.get(this.url + 'qas-sport/'+page, {'headers':headers});
      case 'art':
        return  this._http.get(this.url + 'qas-art/'+page, {'headers':headers});  
      case 'tour':
        return  this._http.get(this.url + 'qas-image/'+page, {'headers':headers});
    
      default:
        break;
    }
    
    
  }
}
