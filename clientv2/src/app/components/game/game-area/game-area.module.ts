import { GameAreaDesactivateGuard } from './game-area-desactivate.guard';
import { HttpModule } from '@angular/http';
import { SharedModule } from './../../../shared/shared.module'; 
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameAreaComponent } from './game-area.component';
import { QuestionsService } from './questions.service';



@NgModule({
  imports: [
    SharedModule,
 
  ],
  declarations: [GameAreaComponent],

  providers: [
    QuestionsService,
    GameAreaDesactivateGuard,    
  ],
})
export class GameAreaModule { }

