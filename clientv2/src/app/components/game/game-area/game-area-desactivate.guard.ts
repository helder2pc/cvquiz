import { GameAreaComponent } from './game-area.component';
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class GameAreaDesactivateGuard implements CanDeactivate<GameAreaComponent> {

    canDeactivate(
      component: GameAreaComponent,
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean>|Promise<boolean>|boolean {
      //alert(67);
      component.OnDestroy();
      return  true;
      //this.permissions.canDeactivate(this.currentUser, route.params.id);
  }
}

