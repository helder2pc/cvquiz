import { Subscription } from 'rxjs/Rx';
import { GLOBAL } from './../../../services/global';
import { GameSingle } from './../../../models/gameSingle';
import { User } from './../../../models/user';
import { GameSingleService } from './../../../services/gameSingle.service';
import { UserService } from './../../../services/user.service';
import { Component, OnInit,OnDestroy } from '@angular/core';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css'],
  providers:[UserService,GameSingleService]
})
export class RankingComponent implements OnInit {

  public identity:User;
  public token;
  public url;
  public page;
  public pages;
  public total;
  public itemsPerPage;
  public gameSingles : GameSingle [];
  private inscription: Subscription;
  public isActive: String;
  
  constructor(
    private _gameSingleService: GameSingleService,
    private _userService: UserService,
  ) { 
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.page=1;
    this.isActive = "total";
  }

  ngOnInit() {
    this.getGame(this.identity._id, this.page,'total');
  }


 getGame(user, page, category){
  this.inscription = this._gameSingleService.getGameSingleUser(this.token,user,category, page).subscribe(
        response => {
              console.log(response);
              if(response.gameS){
               
                this.total = response.total_items;
                this.pages = response.pages;
                this.itemsPerPage = response.itemsPerPage;

              
                  this.gameSingles = response.gameS;
               
                if(page > this.pages){
                  //this._router.navigate(['/home']);
                }                
              }
              else{
                console.log( 'error');
              }
        },

        error =>{
          var errorMessage =<any> error;
          if(errorMessage!=null){
            console.log( 'error44');
            
          }
        }
      );
  }

  getButtonValue(event){
    console.log(event.target.value);
    this.isActive=event.target.value;
    this.getGame(this.identity._id, this.page,event.target.value);
  }
  ngOnDestroy() {
    this.inscription.unsubscribe();
  }
  
}

 
