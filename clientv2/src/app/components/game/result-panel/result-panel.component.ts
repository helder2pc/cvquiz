import { PublicationsService } from './../../../services/publication.service.';
import { Publication } from './../../../models/publication';
import { GameSingleService } from './../../../services/gameSingle.service';
import { GameSingle } from './../../../models/gameSingle';
import { UserService } from './../../../services/user.service';
import { GLOBAL } from './../../../services/global';
import { Config } from './../../../shared/config';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import {trigger, state, style, transition, animate} from '@angular/animations'

@Component({
  selector: 'app-result-panel',
  templateUrl: './result-panel.component.html',
  styleUrls: ['./result-panel.component.css'],
  providers:[UserService,GameSingleService, PublicationsService],
  animations:[
    trigger('animation-category',[
      state('create',style({
        opacity: 1,
        transform: 'scale(1)'
      })),
  
      transition('void => create',[
        style({
          opacity:0,
           /*transform: 'translate(50px,0)'*/
           transform: 'scale(0.5)'
        }),
        animate(
          //duracao, delay e acelaracao
          '500ms 0s ease-in-out'  
        )
      ])
    ])
  ]

})
export class ResultPanelComponent implements OnInit {

    public score: number;
    public wrongAnswer: number;
    public currectAnswer: number;
    public identity:User;
    public token;
    public url;
    public gameSingle: GameSingle;
    public stateResult: string = 'create';
    public publication: Publication;
    
    
  constructor( 
    public router: Router,
    private _gameSingleService: GameSingleService,
    private _userService: UserService,
    private _publicationService: PublicationsService
  ) {
    this.wrongAnswer =  Config.wrongAnswer;
    this.score = Config.score ;
    this.currectAnswer = Config.currectAnswer;
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.gameSingle = new GameSingle("","","",0,0,0,"",this.identity._id);
    this.publication = new Publication("","","","","",);
     
  }

  ngOnInit() {
    //this.sendGameSingle();
   //--- this.sendPublication();
  }


    sendGameSingle(){
      console.log(this.gameSingle);
     // this.publication.text ='Num jogo modo single player, categoria ' +Config.categorySelected +', \n '+ this.identity.username + ' '+this.identity.surusername +' obteve ' +this.score + 'pontos.!';//+ this.currectAnswer +'perguntas  correctas e ' + this.wrongAnswer +'erradas!'
     this.gameSingle.category = Config.categorySelected;
     this.gameSingle.mode = Config.gameType;
     this.gameSingle.score = this.score;
     this.gameSingle.currectAnswer = this.currectAnswer;
     this.gameSingle.wrongAnswer = this.wrongAnswer; 
    


      
      this._gameSingleService.addGameSingle(this.token,this.gameSingle).subscribe(
        response =>{
            if(response.gameS){
              //alert(' jogo guardada com suscesso')
              console.log(response.gameS)
          }
            else{
              console.log('error 111');
            }
        },
        error =>{
          var errorMessage =<any> error;
          console.log(errorMessage);
          if(errorMessage!=null){
            console.log('errorMessage');
          }
        }
      );
  }

  sendPublication(){
    console.log(this.gameSingle);
   this.publication.text ='Num jogo modo single player, categoria ' +Config.categorySelected +', \n '+ this.identity.first_name + ' '+this.identity.last_name +' obteve ' +this.score + 'pontos.!';//+ this.currectAnswer +'perguntas  correctas e ' + this.wrongAnswer +'erradas!'
  /* this.gameSingle.category = Config.categorySelected;
   this.gameSingle.mode = Config.gameType;
   this.gameSingle.score = this.score;
   this.gameSingle.currectAnswer = this.currectAnswer;
   this.gameSingle.wrongAnswer = this.wrongAnswer; */
  


    
    this._publicationService.addPublication(this.token,this.publication).subscribe(
      response =>{
          if(response.gameS){
           // alert(' jogo guardada com suscesso')
            console.log(response.gameS)
            
        }
          else{
            console.log('error 111');
           
          }
      },
      error =>{
        var errorMessage =<any> error;
        console.log(errorMessage);
        if(errorMessage!=null){
          console.log('errorMessage');
        }
      }
    );
}




  playAgain(option0) {
  this.router.navigate(['/gamearea']);
  }
}





