
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultPanelComponent } from './result-panel.component';
// Import ng-circle-progress
import { NgCircleProgressModule } from 'ng-circle-progress';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
   
    // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
     // set defaults here
     radius: 100,
     outerStrokeWidth: 16,
     innerStrokeWidth: 8,
     outerStrokeColor: "#78C000",
     innerStrokeColor: "#C7E596",
     animationDuration: 300,
     
   }),  
  ],
  declarations: [
    ResultPanelComponent
  ]
 
})
export class ResultPanelModule { }
