import { Observable } from 'rxjs/Observable';

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { GLOBAL } from './../../services/global';
import { FollowService } from './../../services/follow.service';
import { UserService } from './../../services/user.service';
import { Follow } from './../../models/follow';
import { User } from './../../models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService,FollowService],
})
export class ProfileComponent implements OnInit {
  public identity;
  public token;
  public url;
  public stats;
  public title:string;
  public status:string;
  public user:User;
  public followed;
  public following;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService,
  ) {
    this.title = "Perfil";
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.followed=false;
    this.following=false;
   // this.stats = this._userService.getStats();

   }

  ngOnInit() {
    console.log("componente profile criado com sucesso");
    this.loadPage();
  }

  loadPage(){
    this._route.params.subscribe( params =>{
      let id = params['id'];
      this.getUser (id);
      this.getConters(id);
    })
  }
  getUser(id){
    this._userService.getUser(id).subscribe(
      response =>{
        if(response){
          console.log(response);
            this.user =response.user;

            if(response.following && response.following._id){
              this.following=true;
            }
            else {
              this.following = false;
            }

            if(response.followed && response.followed._id){
              this.followed=true;
            }
            else {
              this.followed = false;
            }
        }
        else{
          console.log(response);
        }
      },
      error=>{
        console.log(<any> error);
        this._router.navigate(['/perfil',this.identity._id]);
      }
    );
  }

  getConters(id){
    this._userService.getCounters(id).subscribe(
      response =>{
        this.stats = response;
        console.log(response)
      },
      error =>{
        console.log(<any> error);
      }
    );
  }

  followUser(followed){
    var follow = new Follow('',this.identity._id,followed);
    this._followService.addFollow(this.token, follow).subscribe(
      response =>{
        this.following=true;
      },

      error =>{
        console.log(<any> error);
      }
    );
  }

  unFollowUser(followed){
    this._followService.deleteFollow(this.token,followed).subscribe(
      response=>{
        this.following=false;
      },
      error =>{
        console.log(<any> error);
      }
    );
  }

  public followUserOver;

  mouseEnter(user_id){
    this.followUserOver = user_id;
  }

  mouseLeave(){
    this.followUserOver = 0;
  }

}
