
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit,EventEmitter,Input,Output } from '@angular/core';

import { Publication } from './../../models/publication';
import {UserService} from '../../services/user.service';
import { PublicationsService } from './../../services/publication.service.';
import { UploadService } from './../../services/upload.service';
import { Observable } from 'rxjs/Observable';
import { GLOBAL} from '../../services/global';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [UserService,PublicationsService,UploadService],
})
export class SidebarComponent implements OnInit {

  public status: string;
  public identity;
  public token;
  public url;
  public stats;
  public publication: Publication;

 
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _uploadService: UploadService,
    private _publicationsService: PublicationsService,
  ) { 
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.stats = this._userService.getStats();
    this.publication = new Publication("","","","",this.identity._id);
  }

  ngOnInit() {
    console.log("o componete foi carregado")
  }

  onSubimit(form, event){
    console.log(this.publication);
    this._publicationsService.addPublication(this.token,this.publication).subscribe(
      response =>{
          if(response.publication){
           // this.publication = response.publication;
           
           //this.status = 'success';
            //form.reset();
           // this._router.navigate(['/timeline']) 
            if(this.filesToUpload && this.filesToUpload.length){
            //enviar imagem
              this._uploadService.makeFileRequest(this.url+'upload-image-pub/'+response.publication._id,
                                                  [],
                                                  this.filesToUpload,
                                                  this.token,
                                                  'file')
                                  .then((result:any)=>{
                                    this.publication.file =result.image;
                                    
                                  });
         
          }
          this.status = 'success';
          form.reset();
          this._router.navigate(['/timeline']);
          this.sended.emit({send:'true'})
        }
          else{
            this.status = 'error';
          }
      },
      error =>{
        var errorMessage =<any> error;
        console.log(errorMessage);
        if(errorMessage!=null){
          this.status = 'error';
        }
      }
    );
  }

public filesToUpload: Array <File>;

fileChangeEvent(fileInput: any){
  this.filesToUpload = <Array <File>> fileInput.target.files;
}

  @Output() sended = new EventEmitter();
  sendPublication(event){
    console.log(event);
    this.sended.emit({send:'true'})
  }
}
