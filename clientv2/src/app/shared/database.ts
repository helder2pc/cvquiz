import { Pergunta } from './pergunta';

export class Database {

    constructor() { }

          getQuestions() {
            return this.history;
          }
          getAllQuestion(category: string) {
            switch (category) {
                case 'Historia':
                     return this.history;
                case 'Geografia':
                     return this.geography;
                case 'Desporto':
                     return this.sport;
                case 'Arte':
                     return this.art;
                default:
                     return this.geography;
                  //  break;
            }
          }

          selectQuestion(category: string,  id: number) {
          /*  switch (category) {
                case 'Historia':
                     return this.getQuestionList(this.history, id);
                case 'Geografia':
                     return this.getQuestionList(this.geography, id);
                case 'Desporto':
                     return this.getQuestionList(this.sport, id);
                case 'Arte':
                     return this.getQuestionList(this.art, id);
                default:
                     return this.getQuestionList(this.geography, id);
                  //  break;
            }*/
            return this.getQuestionList(this.getAllQuestion(category), id);
          }
          getQuestion(category: string,  id: number) {

          if (category == 'Aleatorio') {
              return this.getQuestionRandom();
          }
            return this.selectQuestion(category, id);

          }

          getQuestionList(list: Pergunta[], id: number) { 
            for (let i = 0; i < list.length; i++) {
                if (list[i].id == id ) {
                return list[i];
                }
            }
            return list[list.length-1 ];
            }


            getQuestionRandom() {
                let categories = ['Música', 'Geografia', 'Historia', 'Desporto'];
                let cat = Math.floor(Math.random() * (5));
                let id = Math.floor(Math.random() * (10));


                return this.selectQuestion( categories[cat], id);
                }

            addQuestion(pergunta: Pergunta, category: string) {
                let list = this.getAllQuestion(category);
                if (pergunta.id==0 || this.checkId(list, pergunta.id) == 0 ) {
                    pergunta.id = list.length + 1;
                    list.push(pergunta);
                } else {
                    //pergunta.id = ;
                    list[ this.checkId(list, pergunta.id ) ] = pergunta;
                }



            }

            checkId(list: Pergunta[], id: number ) {
                for (let i = 0; i < list.length; i++) {
                    if (list[i].id == id ) {
                       return  i;
                    }
                }
                return 0;
            }

            removeQuestion(id: number, category: string){
                let list = this.getAllQuestion(category);
                let index = 0;
                for (let i = 0; i < list.length; i++) {
                    if (list[i].id == id ) {
                       index = i;
                    }
                }
                if (index !=0 ) {
                    list.splice(index, 1);
                }
            }

    private geography: Pergunta[] = [
        {
            id: 1,
            question:'Qual é a maior ilha do arquipélago de Cabo Verde',
            answer:[
                'Santiago',
                'São Vicente',
                'Fogo',
                'Santo Antão'
            ]

        },
        {
            id: 2,
            question:'Em Cabo Verde a temperatura da água do mar varia entre:',
            answer:[
                '21 °C em Fevereiro e 25 °C em Setembro',
                '15 °C em Fevereiro e 25 °C em Setembro',
                '21 °C em Fevereiro e 22 °C em Setembro',
                '18 °C em Fevereiro e 30 °C em Setembro'
            ]
        },
        {
            id: 3,
            question:'O número da população de cidade da Praia é:',
            answer:[
                '113 364',
                '98 200',
                '50 364',
                '504 889'
            ]
        },
        {
            id: 4,
            question:'A superfície total do arquipélago de Cabo Verde é de: ',
            answer:[
                '4 033 km² ',
                '99 864 km²',
                '10 976 km²',
                '2 987 km²'
            ]
        },
        {
            id: 5,
            question:'As ilhas que fazem parte do grupo de Sotavento são:',
            answer:[
                'Maio, Santiago, Fogo e Brava',
                'Sal,Maio, Santiago, Fogo e Brava',
                'Santo Antão, São Vicente, São Nicolau',
                'Sal, Boavista, Maio,Brava'
            ]
        },
        {
            id: 6,
            question:'A Cidade Capital de Cabo Verde é: ',
            answer:[
                'Praia',
                'Mindelo',
                'Porto Novo',
                'Ribeira Grande'
            ]
        },
        {
            id: 7,
            question:'Por quantas ilhas é constituido o arquipélago de Cabo Verde',
            answer:[
                '10',
                '9',
                '12',
                '11'
            ]
        },
        {
            id: 8,
            question: 'A superfície total da ilha de Santiago é de',
            answer:[
                '930 km²',
                '2 000 km²',
                '1 000 km²',
                '500 km²'
            ]
        },
        {
            id: 9,
            question:'Qual é a maior ilha no grupo Barlavento',
            answer:[
                'Santo Antão',
                'Santiago',
                'São Vicente',
                'São Nicolau'
            ]
        },
        
        {
            id: 10,
            question:'Qual é a menor da ilha habitada',
            answer:[
                'Brava' ,
                'Sal',
                'Maio',
                'Santa Luzia'
            ]
        }
      ];



      private history: Pergunta[] = [
        {
            id: 1,
            question:'Quando é que Cabo Verde tornou-se independente.',
            answer:[
                '5 July 1975',
                '5 July 1970',
                '16 November 1969',
                '31 May 1962'
            ]
            
        },
        {
            id: 2,
            question:'Em que ano é que cabo verde começou a ser pouvado.',
            answer:[
                '1462',
                '1460',
                '1450',
                '1562'
            ]
        },
        {
            id: 3,
            question:'Quando Francis Drake atacou Ribeira Grande',
            answer:[
                '1585',
                '1885',
                '1685',
                '1785'
            ]
        },
        {
            id: 4,
            question:'Quem foi o presidente da república de cabo verde em 2003',
            answer:[
                'Pedro Pires',
                'Carlos Veiga',
                'Jorge Fonseca',
                'José Maria Neves'
            ]
        },
        {
            id: 5,
            question:'Quando foi abolida a escravidão em Cabo Verde',
            answer:[
                '1876',
                '1862',
                '1775',
                '1900'
            ]
        },
        {
            id: 6,
            question:'Em que ilha se dança a dança di boi',
            answer:[
                'Santo Antão',
                'São Vicente',
                'São Nicolau',
                'Santiago'
            ]
        },
        {
            id: 7,
            question:'Em que ano o Papa João Paulo II visitou Cabo Verde',
            answer:[
                '1990',
                '1995',
                '2002',
                '1993'
            ]
        },
        {
            id: 8,
            question:'Quais são os tipos de músicas que originaram de Cabo Verde.',
            answer:[
                'Tabanka, Funáná e Batuku',
                'Batuku, Funáná e Zouk',
                'Kuduro, Batuku e Funáná',
                'Batuku e Funáná,AfroHouse'
            ]
        },
        
        {
            id: 9,
            question:'Qual foi a primeira ilha a ser pouvada',
            answer:[
                'Santiago',
                'São Vicente',
                'Fogo',
                'Sal'
            ]
        },
        {
            id: 10,
            question:'Em que ano nasceu Amílcar Lopes Cabral ',
            answer:[
                '1924',
                '1900',
                '1940',
                '1820'
            ]
        }
        
    ]

    private sport: Pergunta[] = [
        {
            'id':1,
            'question':'Em que ano Cabo Verde participou pela primeira vez no CAN:',
            'answer':[
                '2013',
                '2015',
                '2010',
                '2014'
            ]
            
        },
        {
            'id':2,
            'question':'No CAN 2013 que jogador vestiu a camisola número 10:',
            'answer':[
                'Heldon',
                'Platini',
                'Djaniny',
                'Ryan Mendes'
            ]
        },
        {
            'id':3,
            'question':'Que equipa venceu campeonata nacional de futebol em 2014/2015:',
            'answer':[
                'Mindelense',
                'Derby',
                'Boavista',
                'Sporting Praia'
            ]
        },
        {
            'id':4,
            'question':'Em que posição ficou a seleção de basquetebol de Cabo Verde no Afrobasket 2015:',
            'answer':[
                '6',
                '3',
                '5',
                '12'
            ]
        },
        {
            'id':5,
            'question':'Em que ano Cabo Verde foi campeão africano de Andebol sub-21:',
            'answer':[
                '2015',
                '2014',
                '2013',
                '2010'
            ]
        },
        {
            'id':6,
            'question':'Qual equipa venceu mais vezes o torneio Campeonato Cabo-verdiano de Futebol:',
            'answer':[
                'Mindelense',
                'Sporting Praia',
                'Boavista',
                'FC Derby'
            ]
        },
        {
            'id':7,
            'question':'Em que clube joga o cabo-verdiano Babanco: ',
            'answer':[
                'Estoril',
                'Sporting',
                'Benfica',
                'Rio Ave'
            ]
        },
        {
            'id':8,
            'question':'Quem foi o autor do primeiro golo da selecção de Cabo Verde no CAN 2013: ',
            'answer':[
                'Patini',
                'Babanco',
                'Heldon',
                'Ryan Mendes'
            ]
        },
        {
            'id':9,
            'question':'Que atleta paralímpico cabo-verdiano conquistou a medalha de ouro no lançamento de dardo nos XI Jogos Africanos 2015:',
            'answer':[
                'Márcio Fernandes',
                'Gracelino Barbosa',
                'Adilson Spencer',
                'Nelson Cruz'
            ]
        },
        {
            'id':10,
            'question':'Quantos jogo perdeu a selecção de Cabo Verde no CAN 2015:',
            'answer':[
                '0',
                '1',
                '2',
                '3'
            ]
        }
    ]

    private art: Pergunta[] = [
        {
            'id': 1,
            'question':'Que música venceu o premio Música do Ano no CVMA de 2014',
            'answer':[
                '“Bo tem Mel” – Nelson Freitas',
                '“Ka bu tchora” – Dino d’Santiago',
                '“Djar Fogo” – Neuza',
                '“Encomenda de terra” - Mirri lobo '
            ]
            
        },
        {
            'id':2,
            'question':'Qual é o nome do primeiro álbum da banda Os Tubarões',
            'answer':[
                'Pepe Lopi',
                'Tchon di Morgado',
                'Porton d’ nôs ilha',
                'Os Tubarões'
            ]
            
        },
        
        {
            'id': 3,
            'question':'Quem era o vocalista da banda Os Tubarões',
            'answer':[
                'Ildo Lobo',
                'Mário Russo',
                'Albertino Évora',
                'Zeca Couto'
            ]
            
        },
        
        {
            'id': 4,
            'question':'De quem é a musica “Dança Ma Mi Criola”',
            'answer':[
                'Tito Paris',
                'Bana',
                'Ildo Lobo',
                'Zeca Nha Renalda'
            ]
            
        },
        
        {
            'id': 5,
            'question':'Qual é o nome do primeiro disco da cantora Elida Almeida',
            'answer':[
                '“Nta Konsigui”',
                '“Lebam Ku Bo”',
                'Joana',
                '“Tomal el”'
            ]
            
        },
        
        {
            'id': 6,
            'question':'Cantora cabo-verdiana conhicada como “diva dos pés descalços”:',
            'answer':[
                'Cesária Évora',
                'Lura',
                'Sara Tavares',
                'Elida Albertino'
            ]
            
        },
        
        {
            'id': 7,
            'question':'O nome verdadeiro do “Codé di Dona” é: ',
            'answer':[
                'Gregório Vaz',
                'Agusto Tavares',
                'Pedro Semedo',
                'José Varela'
            ]
            
        },
        
        {
            'id': 8,
            'question':'A que banda cabo-verdiana pertece a música “Djam Brancu Dja”: ',
            'answer':[
                'Bulimundo',
                'Os Tubarões',
                'Tulipa Negra',
                'Corda do Sol'
            ]
            
        },
        
        {
            'id': 9,
            'question':'Quem é o compositor da música “Lapidu na bô””: ',
            'answer':[
                'Orlando Pantera',
                'Ildo Lobo',
                'Manuel de Novas',
                'Lura'
            ]
            
        },
        
        
        {
            'id':10,
            'question':'Qual foi a música vencedora da caregoria Música Popular do Ano no CVMA 2015: ',
            'answer':[
                'Djam Bira Fino-Lejemea  ',
                'Toni-Elji feat Zeca di Nha Reinalda',
                'Terra Sabe-Loony Johnson',
                'Poderosa-Djodje feat. Dynamo'
            ]
            
        },
        
        {
            'id':11,
            'question':'Em que ano foi a primeira edição de Cabo Verde Music Awards: ',
            'answer':[
                '2011',
                '2012',
                '2013',
                '2014'
            ]
            
        }
    ]
}
