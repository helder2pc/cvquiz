import { GameSingleService } from './services/gameSingle.service';
import { SafePipe } from './pipe/safe.pipe';
import { FeedInfoService } from './services/feedinfo';
import { QAService } from './services/qa.service';
import { SharedModule } from './shared/shared.module';
import { ResultPanelModule } from './components/game/result-panel/result-panel.module';
import { HelpModule } from './components/game/help/help.module';
import { SettingModule } from './components/game/setting/setting.module';
import { RankingModule } from './components/game/ranking/ranking.module';
import { MenuModule } from './components/game/menu/menu.module';
import { GameAreaModule } from './components/game/game-area/game-area.module';
import { AdminGuard } from './components/admin/admin.guard';
import { AdminModule } from './components/admin/module/admin.module';



import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'angular2-moment';
import { MessagesModule } from './components/messages/messages.module';
import { routing, appRoutingProviders } from './app.routing';
//import { appRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { PublicationsComponent } from './components/publications/publications.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';
import { UserGuard } from './services/user.guard';
import { UserService } from './services/user.service';
import { PageNotFoundComponent } from './components/game/page-not-found/page-not-found.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { CreateQuestionsComponent } from './components/create-questions/create-questions.component';
import { AwardsComponent } from './components/awards/awards.component';
import { UserQuestionComponent } from './components/user-question/user-question.component';
import { EvaluateQuestionComponent } from './components/evaluate-question/evaluate-question.component';
import { FeedinfoComponent } from './components/feedinfo/feedinfo.component'
import { RecaptchaModule } from 'ng-recaptcha';
import { InfoComponent } from './components/info/info.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserEditComponent,
    UsersComponent,
    SidebarComponent,
    TimelineComponent,
    PublicationsComponent,
    ProfileComponent,
    FollowingComponent,
    FollowedComponent,
    PageNotFoundComponent,
    CreateQuestionsComponent,
    AwardsComponent,
    UserQuestionComponent,
    EvaluateQuestionComponent,
    FeedinfoComponent,
    SafePipe,
    InfoComponent,

    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    MomentModule,
    MessagesModule,
    BrowserAnimationsModule, 
    AdminModule,
    GameAreaModule,
    routing,
    GameAreaModule,
    MenuModule,
    RankingModule,
    SettingModule,
    HelpModule,
    ResultPanelModule,
    SharedModule,
    RecaptchaModule.forRoot(), // Keep in mind the "forRoot"-magic nuances!

  
    
    
   
    //appRoutingModule,
  ],
  providers: [
    UserService,
    UserGuard,
    AdminGuard,
    QAService,
    FeedInfoService,
    appRoutingProviders,
    GameSingleService,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
