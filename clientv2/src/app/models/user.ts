export class User{
    constructor(
        public _id:string,
        public first_name: string,
        public last_name:string,
        public username: string,
        public email: string,
        public hash: string,
        public role: string,
        public photo:string,
        public created_at: Date,
        public updated_at: Date,
        
    ){} 
}