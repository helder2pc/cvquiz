export class FeedInfo{
    constructor(
        public _id:string,
        public text:string,
        public file:string,
        public url: String,
        public position: String,
        public created_at:string,
        public user: string
    ){}
}