
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
//import { User } from '../models/user';
import { Publication } from './../models/publication';
import { GLOBAL} from './global';
//import { Http } from '@angular/http'; //Response, ResponseOptions,Headers

@Injectable()
export class GameSingleService{

    public url: string;
   
    constructor(public _http:HttpClient){
        this.url = GLOBAL.url;
    }

    addGameSingle(token, game):Observable<any>{
        let params = JSON.stringify(game);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                        .set('Authorization',token); 
        return  this._http.post(this.url + 'game-single/', params, {'headers':headers});
    }

    getGameSingle(token, category, page=1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization',token); 

        return  this._http.get(this.url + 'game-single-list/'+category+'/'+page, {'headers':headers});
    }

    getGameSingleUser(token, user_id, category,  page=1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization',token); 

        return  this._http.get(this.url + 'game-single-user/'+user_id+'/'+category+'/'+page, {'headers':headers});
    }

    deleteGameSingle(token, id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization',token); 
        return  this._http.delete(this.url + 'game-single/'+id, {'headers':headers});
    }
   


    
}