import { GLOBAL } from './global';


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http'; //Response, ResponseOptions,Headers

@Injectable()
export class GameService {
  public url: string;

   constructor(public _http:HttpClient,){
    this.url = GLOBAL.url;
}

   getQuestion(category: string, id: number) :Observable<any>{

    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this._http.get(this.url + 'qa-history/'+id, {'headers':headers});
  }


  getAllQuestion(category: string,page=1):Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    return  this._http.get(this.url + 'qas-history/'+page, {'headers':headers});
  }

  

}
