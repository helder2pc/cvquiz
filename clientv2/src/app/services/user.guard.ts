import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {UserService} from './user.service';


@Injectable()
export class UserGuard implements CanActivate {
  constructor (
    private _router: Router,
    private _userService: UserService,
  ){

  }
  canActivate(){
    let identity = this._userService.getIdentity();
    if(identity && (identity.role == 'user' || identity.role == 'admin') ){
      return true;
    }
    this._router.navigate(['/login']);
    return false;
  }
}
